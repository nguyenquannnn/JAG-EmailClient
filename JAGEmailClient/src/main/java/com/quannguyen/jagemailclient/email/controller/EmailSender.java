/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.controller;

import jodd.mail.*;
import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import com.quannguyen.jagemailclient.email.data.EmailBean;
import java.time.LocalDateTime;

/**
 * Email Sender module that uses SMTP mail server that handles 
 * converting and sending emails.
 * @author Anh Quan Nguyen
 */
class EmailSender {
    private final String SMTP_SEVER_HOST = "smtp.gmail.com";
    private SmtpServer smtpServer;
    
    /**
     * 2 params constructor that takes in authentication info and
     * create new SmtpSever
     * 
     * @param address
     * @param password 
     */
    public EmailSender(EmailAddressBean address, String password) {
        this.createSmtpServer(address.getEmailAddress(), password);
    }
    
    /**
     * Create new SMTPSever from authentication info
     * 
     * @param address
     * @param password 
     */
    private void createSmtpServer(String address, String password) {
        this.smtpServer = MailServer.create()
                .host(SMTP_SEVER_HOST)
                .ssl(true)
                .auth(address, password)
                .buildSmtpMailServer();
    }
    
    /**
     * Convert and Send email through a SMTP session
     * 
     * @param emailBean Email to be send
     */    
    public void sendEmail(EmailBean emailBean) {
        try(SendMailSession session = this.smtpServer.createSession()) {
            session.open();
            Email email = emailBean.toJoddEmail();
            session.sendMail(email);
            emailBean.setSendDateTime(LocalDateTime.now());
            session.close();
        }  
    } 
}

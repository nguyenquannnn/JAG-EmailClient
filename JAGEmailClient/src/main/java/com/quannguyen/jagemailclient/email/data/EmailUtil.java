/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.data;

import java.util.List;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;
import javax.activation.DataSource;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceivedEmail;
import jodd.net.MimeTypes;

/**
 * Utility class that has multiple helper methods that is used by the Email 
 * Client 
 * 
 * @author Anh Quan Nguyen
 */
public class EmailUtil {
    /*
    *    Title: convertToLocalDateViaInstant
    *    Author: baeldung
    *    Date: August 25, 2018
    *    Availability: https://www.baeldung.com/java-date-to-localdate-and-localdatetime
    */
    public static LocalDateTime convertToLocalDateTime(Date date) {
        return date.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
    
    /**
     * Check to see if 2 lists are equals.
     * 
     * @param list1
     * @param list2
     * @return true if both lists are null/empty or contains all of the same elements
     *         false if one list is null and the other is not and if they do not all contains
     *         all of the same elements
     */
    public static <T> boolean isEqualsList(List<T> list1, List<T> list2) {
        if(list1 == null && list2 == null) {
            return true;
        } else if((list1 == null && list2 != null) ||
                (list1 != null && list2 == null)) {
            return false;
        } else if(list1.isEmpty() && list2.isEmpty()) {
            return true;
        } else {
            //Iterate through list1 and search for the equals email address in array 2
            return (list1.stream().map(a -> {
                return list2.stream().anyMatch(b -> b.equals(a));
            }).filter(b -> b == true).count() == list1.size()) 
                   && 
            list1.size() == list2.size();
        }
    }
    /**
     * Compare to see if 2 String field are equals
     * 
     * @param s1
     * @param s2
     * @return True if 2 string are both null or are equals
     *         False if one of 2 string is null while the other is not or if they
     *         are not equals
     */
    public static boolean isSameStringField(String s1, String s2) {
        if(s1 != null) {
            return s1.equals(s2); 
        } else if(s1 != s2) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Parse a ReceivedEmail and return the equivalent EmailBean
     * 
     * @param email
     * @return 
     */
    public static EmailBean parseJoddMailToEmailBean(ReceivedEmail email) {
        EmailBean temp = new EmailBean();
        
        List<EmailMessage> messages = email.messages();
        List<EmailAttachment<? extends DataSource>> attachments = email.attachments();
        
        parseMessages(temp, messages);
        parseAttachmentFix(temp, attachments);
        parseMetadata(temp, email);
        return temp;
    }
    
    /**
     * Check the type of EmailMessage from ReceivedMessage and set the appropriate 
     * field of an EmailBean
     * 
     * @param email 
     * @param messages List of Received EmailMessage 
     */
    private static void parseMessages(EmailBean email, List<EmailMessage> messages) {
        if(email != null && messages != null && messages.size() > 0) {
            messages.stream().forEach(m -> {
                if(m.getMimeType().equalsIgnoreCase(MimeTypes.MIME_TEXT_PLAIN)) {
                    email.setTextMessage(m.getContent());
                } else if(m.getMimeType().equalsIgnoreCase(MimeTypes.MIME_TEXT_HTML)) {
                    email.setHtmlMessage(m.getContent());
                } 
            });    
        }
    }
    
    /**
     * Parse a List of EmailAttachment from a ReceivedEmail
     * Set them in the equivalent EmailBean
     * 
     * @param email
     * @param attachments 
     */
    private static void parseAttachment(EmailBean email, List<EmailAttachment<? extends DataSource>> attachments) {
        if(email != null && attachments != null && attachments.size() > 0) {
            attachments.stream().forEach(a -> {
                if(a.getContentId() != null ) {
                    email.addEmbeddedAttachments(new EmailAttachmentBean(a));
                } else {
                    email.addAttachments(new EmailAttachmentBean(a));
                }
            });
        }
    }
    
    private static void parseAttachmentFix(EmailBean email, List<EmailAttachment<? extends DataSource>> attachments) {
        if(email != null && attachments != null && attachments.size() > 0) {
            attachments.stream().forEach(a -> {
                if(a.getContentId() != null || a.isInline()) {
                    email.addEmbeddedAttachments(new EmailAttachmentBean(a));
                } else {
                    email.addAttachments(new EmailAttachmentBean(a));
                }
            });
        }
    }
    
    /**
     * Parse the metadata of an ReceivedEmail (EmailAddress for From, To, Cc, Bcc
     * Subject, Priority, SentDate, ReceiveDate)
     * Set them in the equivalent EmailBean
     * @param email
     * @param received 
     */
    private static void parseMetadata(EmailBean email, ReceivedEmail received) {
        if(email != null && received != null) {
            if(received.from() != null) {
                email.setFrom(new EmailAddressBean(received.from().getEmail(),
                        received.from().getPersonalName()));
            }
            
            if(received.to() != null) {
                email.setTo(parseListEmailAddress(Arrays.asList(received.to())));
            } 
            
            if(received.cc() != null) {
                email.setCc(parseListEmailAddress(Arrays.asList(received.cc())));
            } 
            
            if(received.subject() != null) {
                email.setSubject(received.subject());
            } else {
                email.setSubject("");
            }
            
            email.setPriority(email.getPriority());
            email.setSendDateTime(EmailUtil.convertToLocalDateTime(received.sentDate()));
            email.setReceiveDateTime(EmailUtil.convertToLocalDateTime(received.receivedDate()));
            email.setFolderId(1);
        } else {
            throw new IllegalArgumentException("EmailReceiver: Null email or received email");
        }
    }
    
    /**
     * Convert a list of EmailAddress to the equivalent List of EmailAddressBean
     * 
     * @param emailAddress List of EmailAddress
     * @return an equivalent list of EmailAddressBean 
     */
    private static List<EmailAddressBean> parseListEmailAddress(List<EmailAddress> emailAddress) {
        if(emailAddress != null && emailAddress.size() > 0) {
            List<EmailAddressBean> res = 
                        emailAddress.stream()
                        .map(ea -> { 
                            return new EmailAddressBean(ea.getEmail(), ea.getPersonalName());
                        })
                        .collect(Collectors.toList());
            return res;
        } else {
            return new ArrayList<EmailAddressBean>();
        }
    }
    

    /**
     * Check to see if specified string is a valid emailAddress
     * 
     * @param emailAddress
     * @return True if valid emailAddress
     *         False if not
     */
    public static boolean checkEmailAddress(String emailAddress) throws IllegalArgumentException {
        if(RFC2822AddressParser.STRICT.parseToEmailAddress(emailAddress) != null) {
            return true;
        } else {
            throw new IllegalArgumentException("EmailUtil: Invalid email address.");
        }
    }
}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.email.persistence;

import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface that abstracts the CRUD operation regarding the email address table.
 * @author Quan-Nguyen
 */
public interface EmailAddressDAO {
    /**
     * Retrieve an email address with a specified email address String
     * 
     * @param emailAddress
     * @return emailAddressBean
     * @throws SQLException 
     */
    EmailAddressBean getEmailAddress(String emailAddress) throws SQLException;
    /**
     * Get all email address from the database
     * @return List of EmailAddressBean
     * @throws SQLException 
     */
    List<EmailAddressBean> getAllEmailAddress() throws SQLException;
    /**
     * Get all email address that are cc to from an email with specified emailId
     * 
     * @param emailId
     * @return List of Cc EmailAddressBean
     * @throws SQLException 
     */
    List<EmailAddressBean> getEmailAddressCcFromEmail(int emailId) throws SQLException;
    /**
     * Get all email address that are bcc to from an email with specified emailId
     * 
     * @param emailId
     * @return List of Bcc EmailAddressBean
     * @throws SQLException 
     */
    List<EmailAddressBean> getEmailAddressBccFromEmail(int emailId) throws SQLException;
    /**
     * Get all email address that are to to from an email with specified emailId
     * 
     * @param emailId
     * @return List of To EmailAddressBean
     * @throws SQLException 
     */
    List<EmailAddressBean> getEmailAddressToFromEmail(int emailId) throws SQLException;
    /**
     * Get email address from of an email with specified emailId
     * 
     * @param emailId
     * @return EmailAddressBean
     * @throws SQLException 
     */
    EmailAddressBean getEmailAddressFrom(int emailId) throws SQLException;
    /**
     * Add additional email address to the Cc of an email within the database
     * 
     * @param emailId
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    int addEmailAddressCcForEmail(int emailId, EmailAddressBean emailAddressBean) throws SQLException;
    /**
     * Delete  specified email address from the Cc of an email within the database
     * 
     * @param emailId
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    int deleteEmailAddressCcFromEmail(int emailId, EmailAddressBean emailAddressBean) throws SQLException;
    /**
     * Add additional email address to the Bcc of an email within the database
     * 
     * @param emailId
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    int addEmailAddressBccForEmail(int emailId, EmailAddressBean emailAddressBean) throws SQLException;
    /**
     * Delete  specified email address from the Bcc of an email within the database
     * 
     * @param emailId
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    int deleteEmailAddressBccFromEmail(int emailId, EmailAddressBean emailAddressBean) throws SQLException;
    /**
     * Add additional email address to the To of an email within the database
     * 
     * @param emailId
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    int addEmailAddressToForEmail(int emailId, EmailAddressBean emailAddressBean) throws SQLException;
    /**
     * Delete  specified email address from the To of an email within the database
     * 
     * @param emailId
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    int deleteEmailAddressToFromEmail(int emailId, EmailAddressBean emailAddressBean) throws SQLException;
    /**
     * Create an email address record from a specified EmailAddressBean
     * 
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    int createEmailAddress(EmailAddressBean emailAddressBean) throws SQLException;
    /**
     * Delete an email address record from a specified EmailAddressBean
     * 
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    int deleteEmailAddress(EmailAddressBean emailAddressBean) throws SQLException;
}

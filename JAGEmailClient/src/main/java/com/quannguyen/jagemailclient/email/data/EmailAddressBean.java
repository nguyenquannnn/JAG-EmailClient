/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.data;

import java.io.Serializable;
import javafx.beans.property.*;
import jodd.mail.EmailAddress;
import jodd.mail.RFC2822AddressParser;

/**
 * JavaBean representing an EmailAddress, associated with a personal name
 *
 * @author Anh Quan Nguyen
 */
public class EmailAddressBean implements Serializable {

    private IntegerProperty id;
    private StringProperty emailAddress;
    private StringProperty personalName;

    public EmailAddressBean() {
        super();
    }

    /**
     * Constructor that initialize emailAddress and personalName
     *
     * @param emailAddress
     * @param personalName
     * @throws IllegalArgumentException
     */
    public EmailAddressBean(String emailAddress, String personalName) throws IllegalArgumentException {
        this(-1, emailAddress, personalName);
    }

    public EmailAddressBean(int id, String emailAddress, String personalName) {
        this.id = new SimpleIntegerProperty();
        this.emailAddress = new SimpleStringProperty();
        this.personalName = new SimpleStringProperty();
        this.id.set(id);
        setEmailAddress(emailAddress);
        this.personalName.set(personalName);
    }

    public String getEmailAddress() {
        return this.emailAddress.get();
    }

    /**
     * Set the field to the specified value if the argument is not null and is a
     * valid email. Throw IllegalArgumentException if it does not satisfy the
     * condition
     *
     * @param emailAddress
     * @throws IllegalArgumentException
     */
    public void setEmailAddress(String emailAddress) throws IllegalArgumentException {
        if (emailAddress != null && checkEmailAddress(emailAddress)) {
            this.emailAddress.set(emailAddress);
        } else {
            throw new IllegalArgumentException("EmailAddressBean: Illegal email address.");
        }
    }

    public String getPersonnalName() {
        return this.personalName.get();
    }

    public void setPersonnalName(String personnalName) {
        this.personalName.set(personnalName);
    }

    public int getId() {
        return this.id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public IntegerProperty id() {
        return this.id;
    }

    public StringProperty personnalName() {
        return this.personalName;
    }

    public StringProperty emailAddress() {
        return this.emailAddress;
    }

    /**
     * Create a string that represent EmailAddressBean
     *
     * @return String
     */
    @Override
    public String toString() {
        return this.personalName.get() + "<" + this.emailAddress.get() + ">";
    }

    /**
     * Check to see if specified string is a valid emailAddress
     *
     * @param emailAddress
     * @return True if valid emailAddress False if not
     */
    public boolean checkEmailAddress(String emailAddress) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(emailAddress) != null;
    }

    /**
     * Create an equivalent Jodd EmailAddress
     *
     * @return EmailAddress
     */
    public EmailAddress toJoddEmailAddress() {
        return new EmailAddress(personalName.get(), emailAddress.get());
    }

    /**
     * Method checking if 2 objects are equals
     *
     * @param obj
     * @return True if both object share the same references and if both object
     * share the same personnalName and emailAddress
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj instanceof EmailAddressBean) {
            EmailAddressBean temp = (EmailAddressBean) obj;
                return temp.emailAddress.get().equals(this.emailAddress.get());
        } else {
            return false;
        }
    }
}

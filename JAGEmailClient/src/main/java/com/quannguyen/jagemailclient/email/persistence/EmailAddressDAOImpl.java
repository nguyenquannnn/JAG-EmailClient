/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.email.persistence;

import com.quannguyen.jagemailclient.db.controller.DbConnectionHandler;
import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that implements the EmailAddressDAO interface, implementing the CRUD operation 
 * regarding the email_address, email_to_address, email_cc_address, email_bcc_address tables.  
 * 
 * @author Quan-Nguyen
 */
public class EmailAddressDAOImpl implements EmailAddressDAO {
    private static final Logger LOG = LoggerFactory.getLogger(EmailAddressDAOImpl.class);
    private final DbConnectionHandler handler;
    
    /**
     * Get a handler to the connection of Database
     * 
     * @param handler 
     */
    public EmailAddressDAOImpl(DbConnectionHandler handler) {
        super();
        this.handler = handler;
    }
    /**
     * Retrieve an email address with a specified email address String
     * 
     * @param emailAddress
     * @return emailAddressBean
     * @throws SQLException 
     */    
    @Override
    public EmailAddressBean getEmailAddress(String emailAddress) throws SQLException {
        LOG.info("Fetching email address.");
        String query = "SELECT * FROM address WHERE email_address = ?";
        EmailAddressBean row = null;
        
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            ps.setString(1, emailAddress);
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    row = createEmailAddressBean(rs);
                }
            }
        }
        return row;   
    }
    /**
     * Get all email address from the database
     * @return List of EmailAddressBean
     * @throws SQLException 
     */    
    @Override
    public List<EmailAddressBean> getAllEmailAddress() throws SQLException {
        LOG.info("Fetching all Email addresses.");
        String query = "SELECT * FROM address";
        List<EmailAddressBean> rows = new ArrayList<>();
        
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    rows.add(createEmailAddressBean(rs));
                }
            }
        }
        return rows;    
    }
    /**
     * Create an EmailAddressBean from the ResultSel of the SQL query
     * @param rs  ResultSel of the SQL query
     * @return EmailAddressBean
     * @throws SQLException 
     */
    private EmailAddressBean createEmailAddressBean(ResultSet rs) throws SQLException{
        return new EmailAddressBean(rs.getInt("ADDRESS_ID"),
                rs.getString("EMAIL_ADDRESS"), 
                rs.getString("CONTACT_NAME"));
    }
    /**
     * Get all email address that are cc to from an email with specified emailId
     * 
     * @param emailId
     * @return List of Cc EmailAddressBean
     * @throws SQLException 
     */
    @Override
    public List<EmailAddressBean> getEmailAddressCcFromEmail(int emailId) throws SQLException {
        LOG.info("Fetching Cc.");
        String query = "SELECT * FROM address a "
                + "JOIN email_cc_address eca ON a.address_id = eca.cc_email_id "
                + "WHERE eca.email_id = ?";
        List<EmailAddressBean> rows = new ArrayList<>();
        
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1, emailId);
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    rows.add(createEmailAddressBean(rs));
                }
            }
        }
        return rows;    
    }
    /**
     * Get all email address that are bcc to from an email with specified emailId
     * 
     * @param emailId
     * @return List of Bcc EmailAddressBean
     * @throws SQLException 
     */
    @Override
    public List<EmailAddressBean> getEmailAddressBccFromEmail(int emailId) throws SQLException {
        LOG.info("Fetching Bcc.");
        String query = "SELECT * FROM address a "
            + "JOIN email_bcc_address eca ON a.address_id = eca.bcc_email_id "
            + "WHERE eca.email_id = ?";
        List<EmailAddressBean> rows = new ArrayList<>();
        
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1, emailId);
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    rows.add(createEmailAddressBean(rs));
                }
            }
        }
        return rows;    
    }
    /**
     * Get all email address that are to to from an email with specified emailId
     * 
     * @param emailId
     * @return List of To EmailAddressBean
     * @throws SQLException 
     */
    @Override
    public List<EmailAddressBean> getEmailAddressToFromEmail(int emailId) throws SQLException {
        LOG.info("Fetching To.");
        String query = "SELECT * FROM address a "
            + "JOIN email_to_address eca ON a.address_id = eca.to_email_id "
            + "WHERE eca.email_id = ?";
        List<EmailAddressBean> rows = new ArrayList<>();
        
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1, emailId);
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    rows.add(createEmailAddressBean(rs));
                }
            }
        }
        return rows;
    }
    /**
     * Create an email address record from a specified EmailAddressBean
     * 
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int createEmailAddress(EmailAddressBean emailAddressBean) throws SQLException {
        LOG.info("Creating email address");
        String insertQuery = "INSERT INTO address(email_address, contact_name) VALUES (?, ?)";
        int result = 0;
        
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
            
            EmailAddressBean preQuery = getEmailAddress(emailAddressBean.getEmailAddress());
            if(preQuery != null) {
                emailAddressBean.setId(preQuery.getId());
                return 0;
            } else {
                ps.setString(1, emailAddressBean.getEmailAddress());
                ps.setString(2, emailAddressBean.getPersonnalName());
                result = ps.executeUpdate();

                try (ResultSet rs = ps.getGeneratedKeys();) {
                    int recordNum = -1;
                    if (rs.next()) {
                        recordNum = rs.getInt(1);
                    }
                    emailAddressBean.setId(recordNum);
                } 
            }
        }
        return result; 
    }
    /**
     * Delete an email address record from a specified EmailAddressBean
     * 
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int deleteEmailAddress(EmailAddressBean emailAddressBean) throws SQLException {
        LOG.info("Deleting email address");
        String deleteQuery = "DELETE FROM address WHERE address_id = ?";
        int result = 0;
        
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(deleteQuery);
            
            ps.setInt(1, emailAddressBean.getId());
            result = ps.executeUpdate();
        }
        return result; 
    }
    /**
     * Get email address from of an email with specified emailId
     * 
     * @param emailId
     * @return EmailAddressBean
     * @throws SQLException 
     */
    @Override
    public EmailAddressBean getEmailAddressFrom(int emailId) throws SQLException {
        String query = "SELECT * FROM address a "
            + "JOIN email e ON a.address_id = e.from_address "
            + "WHERE e.email_id = ?";
        LOG.info("Fetching from email address.");
        EmailAddressBean row = null;
        
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1, emailId);
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    row = createEmailAddressBean(rs);
                }
            }
        }
        return row;
    }
    /**
     * Add additional email address to the Cc of an email within the database
     * 
     * @param emailId
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int addEmailAddressCcForEmail(int emailId, EmailAddressBean emailAddressBean) throws SQLException {
        LOG.info("Adding cc email address for email");
        String insertQuery = "INSERT INTO email_cc_address(email_id, cc_email_id) VALUES (?, ?);";
        int result = 0;
        
        try (Connection c = handler.getConnection()) {
            createEmailAddress(emailAddressBean);
            PreparedStatement ps = c.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);

            ps.setInt(1, emailId);
            ps.setInt(2, emailAddressBean.getId());
            result = ps.executeUpdate();

            try (ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                    emailAddressBean.setId(recordNum);
            } 
        }
        return result; 
    }
    /**
     * Delete  specified email address from the Cc of an email within the database
     * 
     * @param emailId
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int deleteEmailAddressCcFromEmail(int emailId, EmailAddressBean emailAddressBean) throws SQLException {
        LOG.info("Deleting cc email address from email");
        String deleteQuery = "DELETE FROM email_cc_address WHERE email_id = ?"
                + " AND cc_email_id = ?";
        int result = 0;
        
        try (Connection c = handler.getConnection()) {
            emailAddressBean = getEmailAddress(emailAddressBean.getEmailAddress());
            PreparedStatement ps = c.prepareStatement(deleteQuery);
            
            ps.setInt(1, emailId);
            ps.setInt(2, emailAddressBean.getId());
            result = ps.executeUpdate();
        }
        return result; 
    }
    /**
     * Add additional email address to the Bcc of an email within the database
     * 
     * @param emailId
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int addEmailAddressBccForEmail(int emailId, EmailAddressBean emailAddressBean) throws SQLException {
        LOG.info("Adding bcc email address for email");
        String insertQuery = "INSERT INTO email_bcc_address(email_id, bcc_email_id) VALUES (?, ?);";
        int result = 0;
        
        try (Connection c = handler.getConnection()) {
            createEmailAddress(emailAddressBean);
            PreparedStatement ps = c.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);

            ps.setInt(1, emailId);
            ps.setInt(2, emailAddressBean.getId());
            result = ps.executeUpdate();

            try (ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                    emailAddressBean.setId(recordNum);
            } 
        }
        return result;         
    }
    /**
     * Delete  specified email address from the Bcc of an email within the database
     * 
     * @param emailId
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int deleteEmailAddressBccFromEmail(int emailId, EmailAddressBean emailAddressBean) throws SQLException {
        LOG.info("Deleting bcc email address from email.");
        String deleteQuery = "DELETE FROM email_bcc_address WHERE email_id = ?"
                + " AND bcc_email_id = ?";
        int result = 0;
        
        try (Connection c = handler.getConnection()) {
            emailAddressBean = getEmailAddress(emailAddressBean.getEmailAddress());
            PreparedStatement ps = c.prepareStatement(deleteQuery);
            
            ps.setInt(1, emailId);
            ps.setInt(2, emailAddressBean.getId());
            result = ps.executeUpdate();
        }
        return result; 
    }
    /**
     * Add additional email address to the To of an email within the database
     * 
     * @param emailId
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int addEmailAddressToForEmail(int emailId, EmailAddressBean emailAddressBean) throws SQLException {
        LOG.info("Adding to email address for email.");
        String insertQuery = "INSERT INTO email_to_address(email_id, to_email_id) VALUES (?, ?);";
        int result = 0;
        
        try (Connection c = handler.getConnection()) {
            createEmailAddress(emailAddressBean);
            PreparedStatement ps = c.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);

            ps.setInt(1, emailId);
            ps.setInt(2, emailAddressBean.getId());
            result = ps.executeUpdate();

            try (ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                    emailAddressBean.setId(recordNum);
            } 
        }
        return result;     
    }
    /**
     * Delete  specified email address from the To of an email within the database
     * 
     * @param emailId
     * @param emailAddressBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int deleteEmailAddressToFromEmail(int emailId, EmailAddressBean emailAddressBean) throws SQLException {
        LOG.info("Deleting to email address from email.");
        String deleteQuery = "DELETE FROM email_to_address WHERE email_id = ?"
                + " AND to_email_id = ?";
        int result = 0;
        
        try (Connection c = handler.getConnection()) {
            emailAddressBean = getEmailAddress(emailAddressBean.getEmailAddress());
            PreparedStatement ps = c.prepareStatement(deleteQuery);
            
            ps.setInt(1, emailId);
            ps.setInt(2, emailAddressBean.getId());
            result = ps.executeUpdate();
        }
        return result;         
    }
}

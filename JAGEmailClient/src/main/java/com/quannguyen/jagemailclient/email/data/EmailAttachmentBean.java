package com.quannguyen.jagemailclient.email.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import javafx.beans.property.*;
import jodd.mail.EmailAttachment;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * JavaBean representing an EmailAttachment that can either be embedded or don't
 *
 * @author Anh Quan Nguyen
 */
public class EmailAttachmentBean implements Serializable {

    private IntegerProperty id;
    private byte[] data;
    private StringProperty attachmentName;
    private BooleanProperty isEmbedded;

    public EmailAttachmentBean() {
        super();
        this.id = new SimpleIntegerProperty();
        this.attachmentName = new SimpleStringProperty();
        this.isEmbedded = new SimpleBooleanProperty();
        this.data = data;
    }

    public EmailAttachmentBean(byte[] data, String attachmentName, boolean isEmbedded) {
        super();
        this.id = new SimpleIntegerProperty();
        this.attachmentName = new SimpleStringProperty();
        this.isEmbedded = new SimpleBooleanProperty();
        this.data = data;
        this.attachmentName.set(attachmentName);
        this.isEmbedded.set(isEmbedded);
    }

    /**
     * Default constructor for non-embedded attachments.
     *
     * @param file
     * @throws IOException
     */
    public EmailAttachmentBean(File file) throws IOException {
        this(file, false);
    }

    /**
     * 2 Params constructor that load in the file data from the specified File
     * and set it to be embedded if true
     *
     * @param file
     * @param isEmbedded
     * @throws IOException
     */
    public EmailAttachmentBean(File file, boolean isEmbedded) throws IOException {
        super();
        this.id = new SimpleIntegerProperty();
        this.attachmentName = new SimpleStringProperty();
        this.isEmbedded = new SimpleBooleanProperty();
        this.data = readDataFromFile(file);
        this.attachmentName.set(file.getName());
        this.isEmbedded.set(isEmbedded);
    }

    /**
     * Constructor that initialize an equivalent EmailAttachmentBean from the
     * same EmailAttachment object
     *
     * @param attachment
     */
    public EmailAttachmentBean(EmailAttachment attachment) {
        super();
        this.id = new SimpleIntegerProperty();
        this.attachmentName = new SimpleStringProperty();
        this.isEmbedded = new SimpleBooleanProperty();
        this.data = attachment.toByteArray();
        this.attachmentName.set(attachment.getName());
        this.isEmbedded.set(attachment.isInline());
    }

    public int getId() {
        return this.id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getAttachmentName() {
        return attachmentName.get();
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName.set(attachmentName);
    }

    public boolean isIsEmbedded() {
        return isEmbedded.get();
    }

    public void setIsEmbedded(boolean isEmbedded) {
        this.isEmbedded.set(isEmbedded);
    }

    public StringProperty attachmentName() {
        return this.attachmentName;
    }

    public IntegerProperty id() {
        return this.id;
    }

    public BooleanProperty isEmbedded() {
        return this.isEmbedded;
    }

    /**
     * Convert to this EmailAttachmentBean to the equivalent EmailAttachment
     *
     * @return EmailAttachment
     */
    public EmailAttachment toJoddEmailAttachment() {
        return EmailAttachment.with()
                .contentId(this.attachmentName.get())
                .content(this.data)
                .name(this.attachmentName.get())
                .inline(this.isEmbedded.get())
                .buildByteArrayDataSource();
    }

    /**
     * Read-in data of a File as byte[]
     *
     * @param file
     * @return byte[] of the file
     * @throws IOException
     */
    private byte[] readDataFromFile(File file) throws IOException {
        byte[] fileContent = new byte[(int) file.length()];
        try (FileInputStream fis = new FileInputStream(file)) {
            fis.read(fileContent);
            return fileContent;
        }
    }

    /**
     * Method checking if objects are equal
     *
     * @param obj
     * @return True if both objects share same reference, or have same
     * attachmentName and equal byte[] of data
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj instanceof EmailAttachmentBean) {
            EmailAttachmentBean temp = (EmailAttachmentBean) obj;
            return (this.attachmentName.get().equals(temp.attachmentName.get())
                    && Arrays.equals(this.data, temp.data));
        } else {
            return false;
        }
    }
}

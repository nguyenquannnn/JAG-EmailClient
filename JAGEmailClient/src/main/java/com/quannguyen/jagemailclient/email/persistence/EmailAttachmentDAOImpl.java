/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.email.persistence;

import com.quannguyen.jagemailclient.db.controller.DbConnectionHandler;
import com.quannguyen.jagemailclient.email.data.EmailAttachmentBean;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that implements the EmailAddressDAO interface, implementing the CRUD operation 
 * regarding the attachment, email_attachment tables.  
 * 
 * @author Quan-Nguyen
 */
public class EmailAttachmentDAOImpl implements EmailAttachmentDAO {
    private final DbConnectionHandler handler;
    private static final Logger LOG = LoggerFactory.getLogger(EmailAttachmentDAOImpl.class);
    
    /**
     * Get a handler to the connection of Database
     * 
     * @param handler 
     */
    public EmailAttachmentDAOImpl(DbConnectionHandler handler) {
        super();
        this.handler = handler;
    }
    /**
     * Retrieve an attachment with a specified a id
     * 
     * @param id emailId
     * @return
     * @throws SQLException 
     */    
    @Override
    public EmailAttachmentBean getEmailAttachmentBean(int id) throws SQLException {
        LOG.info("Fetching email attachment.");
        String query = "SELECT * FROM attachment a "
                + " WHERE a.attachment_id = ?";
        EmailAttachmentBean row = null;
        
        try (Connection c = this.handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1, id);
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    row = createEmailAttachmentBean(rs);
                }
            }
        }
        return row; 
    }
    /**
     * Retrieve list of embedded attachments from a specified email with specified id.
     * 
     * @param emailId emailId
     * @return List of embedded email attachments
     * @throws SQLException 
     */
    @Override
    public List<EmailAttachmentBean> getEmbeddedEmailAttachmentsOfEmail(int emailId) throws SQLException {
        String query = "SELECT * FROM attachment a "
                + "JOIN email_attachment ea ON a.attachment_id = ea.attachment_id "
                + "WHERE ea.email_id = ? AND a.is_embedded = TRUE";
        List<EmailAttachmentBean> rows = new ArrayList<>();
        
        LOG.info("Fetchhing embedded email attachments.");
        try (Connection c = this.handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1, emailId);
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    rows.add(createEmailAttachmentBean(rs));
                }
            }
        }
        return rows;
    }
    /**
     * Retrieve list of attachments from a specified email with specified id.
     * 
     * @param emailId emailId
     * @return List of email attachments
     * @throws SQLException 
     */   
    @Override
    public List<EmailAttachmentBean> getEmailAttachmentsOfEmail(int emailId) throws SQLException {
        String query = "SELECT * FROM attachment a "
                + "JOIN email_attachment ea ON a.attachment_id = ea.attachment_id "
                + "WHERE ea.email_id = ? AND a.is_embedded = FALSE";
        List<EmailAttachmentBean> rows = new ArrayList<>();
        
        LOG.info("Fetching email attachments.");
        try (Connection c = this.handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1, emailId);
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    rows.add(createEmailAttachmentBean(rs));
                }
            }
        }
        return rows;
    }
    /**
     * Create an EmailAttachmentBean from the ResultSet of the SQL query
     * 
     * @param rs ResultSet of the SQL query
     * @return EmailAttachmentBean
     * @throws SQLException 
     */
    private EmailAttachmentBean createEmailAttachmentBean(ResultSet rs) throws SQLException {
        return new EmailAttachmentBean(rs.getBytes("DATA"), rs.getString("ATTACHMENT_NAME"), rs.getBoolean("is_embedded"));
    }
    /**
     * Create an email attachment record in the database from a bean
     * 
     * @param emailAttachmentBean
     * @returnresult of persistence operation
     * @throws SQLException 
     */
    @Override
    public int createEmailAttachment(EmailAttachmentBean emailAttachmentBean) throws SQLException {
        String insertQuery = "INSERT INTO attachment(data, attachment_name, is_embedded) VALUES (?, ?, ?)";
        int result = 0;
        
        LOG.info("Creating email attachment.");
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
            
            ps.setBytes(1, emailAttachmentBean.getData());
            ps.setString(2, emailAttachmentBean.getAttachmentName());
            ps.setBoolean(3, emailAttachmentBean.isIsEmbedded());
            result = ps.executeUpdate();
            
            try (ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                emailAttachmentBean.setId(recordNum);
            } 
        }
        return result; 
    }
    /**
     * Delete an email attachment with a specified id.
     * 
     * @param id
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int deleteEmailAttachment(int id) throws SQLException {
        String deleteQuery = "DELETE FROM attachment WHERE attachment_id = ?";
        int result = 0;
        
        LOG.info("Deleting email attachment.");
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(deleteQuery);
            
            ps.setInt(1, id);
            result = ps.executeUpdate();
        }
        return result; 
    }    
    /**
     * Add an attachment 
     * 
     * @param emailId
     * @param attachment
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int addEmailAttachmentForEmail(int emailId, EmailAttachmentBean attachment) throws SQLException {
        String insertQuery = "INSERT INTO email_attachment(email_id, attachment_id) VALUES (?, ?)";
        int result = 0;
        
        LOG.info("Adding email attachment to Email.");
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
            createEmailAttachment(attachment);
            
            ps.setInt(1, emailId);
            ps.setInt(2, attachment.getId());
            result = ps.executeUpdate();
            
            try (ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                attachment.setId(recordNum);
            } 
        }
        return result; 
    }
    /**
     * Delete an email attachment record in the database from a bean
     * 
     * @param emailId
     * @param id attachmentId
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int deleteEmailAttachmentFromEmail(int email, int id) throws SQLException {
        String deleteQuery = "DELETE FROM attachment WHERE attachment_id = ?";
        int result = 0;
        
        LOG.info("Deleting email attachment.");
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(deleteQuery);
            
            ps.setInt(1, id);
            result = ps.executeUpdate();
        }
        return result; 
    }
}

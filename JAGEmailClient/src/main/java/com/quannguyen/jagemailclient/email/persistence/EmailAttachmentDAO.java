/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.email.persistence;

import com.quannguyen.jagemailclient.email.data.EmailAttachmentBean;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface that abstracts the CRUD operation regarding the attachment table.
 * @author Quan-Nguyen
 */
public interface EmailAttachmentDAO {
    /**
     * Retrieve an attachment with a specified a id
     * 
     * @param id emailId
     * @return
     * @throws SQLException 
     */
    EmailAttachmentBean getEmailAttachmentBean(int id) throws SQLException;
    /**
     * Retrieve list of attachments from a specified email with specified id.
     * 
     * @param emailId emailId
     * @return List of email attachments
     * @throws SQLException 
     */
    List<EmailAttachmentBean> getEmailAttachmentsOfEmail(int emailId) throws SQLException;
    /**
     * Retrieve list of embedded attachments from a specified email with specified id.
     * 
     * @param emailId emailId
     * @return List of embedded email attachments
     * @throws SQLException 
     */
    List<EmailAttachmentBean> getEmbeddedEmailAttachmentsOfEmail(int emailId) throws SQLException;
    /**
     * Add an attachment 
     * 
     * @param emailId
     * @param attachment
     * @return result of persistence operation
     * @throws SQLException 
     */
    int addEmailAttachmentForEmail(int emailId, EmailAttachmentBean attachment) throws SQLException;
    /**
     * Create an email attachment record in the database from a bean
     * 
     * @param emailAttachmentBean
     * @returnresult of persistence operation
     * @throws SQLException 
     */
    int createEmailAttachment(EmailAttachmentBean emailAttachmentBean) throws SQLException;
    /**
     * Delete an email attachment record in the database from a bean
     * 
     * @param emailId
     * @param id attachmentId
     * @return result of persistence operation
     * @throws SQLException 
     */
    int deleteEmailAttachmentFromEmail(int email, int id) throws SQLException;
    /**
     * Delete an email attachment with a specified id.
     * 
     * @param id
     * @return result of persistence operation
     * @throws SQLException 
     */
    int deleteEmailAttachment(int id) throws SQLException;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.data;

/**
 * Enum representing different type of Email
 * @author Anh Quan Nguyen
 */
public enum EmailType {
    NEW("New: "), REPLY("Re: "), FORWARD("Fwd: ");
    
    private String code;

    private EmailType(String code) {
        this.code = code;
    }
    
   public String getCode() {
       return this.code;
   } 
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.data;

import java.io.Serializable;
import java.util.List;
import jodd.mail.Email;
import java.time.*;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;

/**
 * JavaBean class that is modified to work with the Joddd.mail library, and
 * represent an Email object.
 *
 * @author 1632647
 */
public class EmailBean implements Serializable {

    private EmailType type;

    private IntegerProperty id;

    private ObjectProperty<EmailAddressBean> from;
    private ListProperty<EmailAddressBean> to;
    private ListProperty<EmailAddressBean> cc;
    private ListProperty<EmailAddressBean> bcc;

    private StringProperty subject;
    private StringProperty textMessage;
    private StringProperty htmlMessage;
    private ListProperty<EmailAttachmentBean> attachments;
    private ListProperty<EmailAttachmentBean> embeddedAttachments;

    private IntegerProperty priority;
    private ObjectProperty<LocalDateTime> sendDateTime;
    private ObjectProperty<LocalDateTime> receiveDateTime;

    private IntegerProperty folderId;

    /**
     * Default constructor initializing fields.
     */
    public EmailBean() {
        super();
        this.id = new SimpleIntegerProperty();
        id.set(0);
        this.from = new SimpleObjectProperty<>();
        this.to = new SimpleListProperty<>();
        setTo(new ArrayList<EmailAddressBean>());
        this.cc = new SimpleListProperty<>();
        setCc(new ArrayList<EmailAddressBean>());
        this.bcc = new SimpleListProperty<>();
        setBcc(new ArrayList<EmailAddressBean>());

        this.attachments = new SimpleListProperty<>();
        setAttachments(new ArrayList<>());
        this.embeddedAttachments = new SimpleListProperty<>();
        setEmbeddedAttachments(new ArrayList<>());
        
        this.htmlMessage = new SimpleStringProperty();
        this.htmlMessage.set("");
        this.textMessage = new SimpleStringProperty();
        this.textMessage.set("");
        this.subject = new SimpleStringProperty();
        this.subject.set("");
        
        this.priority = new SimpleIntegerProperty();
        this.sendDateTime = new SimpleObjectProperty<>();
        this.receiveDateTime = new SimpleObjectProperty<>();
        
        this.folderId = new SimpleIntegerProperty();
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public EmailType getType() {
        return type;
    }

    public void setType(EmailType type) {
        this.type = type;
    }

    public EmailAddressBean getFrom() {
        return from.get();
    }

    public void setFrom(EmailAddressBean from) {
        this.from.set(from);
    }

    public List<EmailAddressBean> getTo() {
        return to.get();
    }

    public void setTo(List<EmailAddressBean> to) {
        this.to.set(FXCollections.observableArrayList(to));
    }

    public List<EmailAddressBean> getCc() {
        return cc.get();
    }

    public void setCc(List<EmailAddressBean> cc) {
        ObservableList<EmailAddressBean> observableList = FXCollections.observableArrayList(cc);
        this.cc.set(observableList);
    }

    public List<EmailAddressBean> getBcc() {
        return bcc;
    }

    public void setBcc(List<EmailAddressBean> bcc) {
        ObservableList<EmailAddressBean> observableList = FXCollections.observableArrayList(bcc);
        this.bcc.set(observableList);
    }

    public String getSubject() {
        return subject.get();
    }

    public void setSubject(String subject) {
        this.subject.set(subject);
    }

    public String getTextMessage() {
        return textMessage.get();
    }

    public void setTextMessage(String textMessage) {
        this.textMessage.set(textMessage);
    }

    public String getHtmlMessage() {
        return htmlMessage.get();
    }

    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage.set(htmlMessage);
    }

    public List<EmailAttachmentBean> getAttachments() {
        return attachments.get();
    }

    public void setAttachments(List<EmailAttachmentBean> attachments) {
        ObservableList<EmailAttachmentBean> observableList = FXCollections.observableArrayList(attachments);
        this.attachments.set(observableList);
    }

    public List<EmailAttachmentBean> getEmbeddedAttachments() {
        return embeddedAttachments.get();
    }

    public void setEmbeddedAttachments(List<EmailAttachmentBean> embeddedAttachments) {
        embeddedAttachments.stream().forEach(ea -> ea.setIsEmbedded(true));
        ObservableList<EmailAttachmentBean> observableList = FXCollections.observableArrayList(attachments);
        this.embeddedAttachments.set(observableList);
    }

    public int getPriority() {
        return priority.get();
    }

    public void setPriority(int priority) {
        this.priority.set(priority);
    }

    public LocalDateTime getSendDateTime() {
        return sendDateTime.get();
    }

    public void setSendDateTime(LocalDateTime sendDateTime) {
        this.sendDateTime.set(sendDateTime);
    }

    public LocalDateTime getReceiveDateTime() {
        return receiveDateTime.get();
    }

    public void setReceiveDateTime(LocalDateTime receiveDateTime) {
        this.receiveDateTime.set(receiveDateTime);
    }

    public int getFolderId() {
        return folderId.get();
    }

    public void setFolderId(int folder) {
        this.folderId.set(folder);
    }

    /**
     * Add embeddedAttachment to the List of embeddedAttachments
     *
     * @param embeddedAttachments
     */
    public void addEmbeddedAttachments(EmailAttachmentBean embeddedAttachments) {
        this.embeddedAttachments.add(embeddedAttachments);
    }

    /**
     * Add attachments to the list of attachments
     *
     * @param attachments
     */
    public void addAttachments(EmailAttachmentBean attachments) {
        this.attachments.add(attachments);
    }

    /**
     * Attach a list of EmailAddress to an Email object from a List of
     * EmailAddressBean
     *
     * @param list List of EmailAddressBean
     * @param email
     * @param action Callback function
     */
    private void convertEmailAddress(List<EmailAddressBean> list, Email email, Consumer<EmailAddress[]> action) {
        if (list != null && list.size() > 0) {
            action.accept(list.stream()
                    .map(l -> l.toJoddEmailAddress())
                    .collect(Collectors.toList())
                    .toArray(new EmailAddress[0]));
        } else {
            action.accept(new EmailAddress[0]);
        }
    }

    /**
     * Attach metadata to Email object
     *
     * @param metadata
     * @param action
     */
    private void converMetaData(String metadata, Consumer<String> action) {
        if (metadata != null && !metadata.isEmpty()) {
            action.accept(metadata);
        } else {
            action.accept("");
        }
    }

    /**
     * Attach a list of EmailAttachment to Email object from a List of
     * EmailAttachmentBean
     *
     * @param list
     * @param action
     */
    private void converAttachments(List<EmailAttachmentBean> list, Consumer<EmailAttachment> action) {
        if (list != null && list.size() > 0) {
            list.stream()
                    .forEach(l -> action.accept(l.toJoddEmailAttachment()));
        }
    }

    public IntegerProperty id() {
        return this.id;
    }

    public ObjectProperty<EmailAddressBean> from() {
        return this.from;
    }

    public ListProperty<EmailAddressBean> to() {
        return this.to;
    }

    public ListProperty<EmailAddressBean> cc() {
        return this.cc;
    }

    public ListProperty<EmailAddressBean> bcc() {
        return this.bcc;
    }

    public StringProperty subject() {
        return this.subject;
    }

    public StringProperty textMessage() {
        return this.textMessage;
    }

    public StringProperty htmlMessage() {
        return this.htmlMessage;
    }

    public ListProperty<EmailAttachmentBean> attachments() {
        return this.attachments;
    }

    public ListProperty<EmailAttachmentBean> embeddedAttachments() {
        return this.embeddedAttachments;
    }

    public IntegerProperty priority() {
        return this.priority;
    }

    public ObjectProperty<LocalDateTime> sendDateTime() {
        return this.sendDateTime;
    }

    public ObjectProperty<LocalDateTime> receiveDateTime() {
        return this.receiveDateTime;
    }

    public IntegerProperty folderId() {
        return this.folderId;
    }

    /**
     * Convert an EmailBean to an equivalent Jodd Email
     *
     * @return Email
     */
    public Email toJoddEmail() {
        Email email = Email.create();
        email.from(this.from.get().toJoddEmailAddress());

        convertEmailAddress(this.to.get(), email, email::to);
        convertEmailAddress(this.cc.get(), email, email::cc);
        convertEmailAddress(this.bcc.get(), email, email::bcc);

        converMetaData(subject.get(), email::subject);
        converMetaData(textMessage.get(), email::textMessage);
        converMetaData(htmlMessage.get(), email::htmlMessage);

        converAttachments(attachments.get(), email::attachment);
        converAttachments(embeddedAttachments.get(), email::embeddedAttachment);
        return email;
    }

    /**
     * String that represent the EmailBean
     *
     * @return String
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Email{'").append(from.get()).append("\', subject='").append(subject.get()).append("\'\n");
        this.to.get().stream().forEach(t -> sb.append("To: ").append(t.toString()).append("\n"));
        if (this.cc != null && this.cc.size() > 0) {
            this.cc.stream().forEach(t -> sb.append("Cc: ").append(t.toString()).append("\n"));
        }
        if (this.bcc != null && this.bcc.size() > 0) {
            this.bcc.stream().forEach(t -> sb.append("Bcc: ").append(t.toString()).append("\n"));
        }
        sb.append("Text message: ").append(textMessage.get()).append("\n");
        sb.append("Html Messagge: ").append(htmlMessage.get()).append("\n");
        sb.append("Embedded attachments: ").append(embeddedAttachments.get().size()).append("\n");
        sb.append("Attachments: ").append(attachments.get().size()).append("\n");
        sb.append("Sent: ").append(sendDateTime.get() != null ? sendDateTime.get().toString() : "").append("\n");
        sb.append("Receive: ").append(receiveDateTime.get() != null ? receiveDateTime.get().toString() : "").append(" }\n");
        return sb.toString();
    }

    /**
     * Method checking to see if 2 objects are equals
     *
     * @param obj
     * @return True if both have the same references, and share the same
     * following field: from, to ,cc, attachment, embeddedAttachment, subject,
     * textMessage, htmlMessage
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj instanceof EmailBean) {
            EmailBean temp = (EmailBean) obj;
            boolean equals = true;
            if (!EmailUtil.isSameStringField(temp.subject.get(), this.subject.get())
                    || !EmailUtil.isSameStringField(temp.textMessage.get(), this.textMessage.get())
                    || !EmailUtil.isSameStringField(temp.htmlMessage.get(), this.htmlMessage.get())
                    || !temp.from.get().equals(this.from.get())) {
                equals = false;
            }

            if (!EmailUtil.isEqualsList(this.to.get(), temp.to.get())
                    || !EmailUtil.isEqualsList(this.cc.get(), temp.cc.get())) {
                equals = false;
            }

            if (!EmailUtil.isEqualsList(this.attachments.get(), temp.attachments.get())
                    || !EmailUtil.isEqualsList(this.embeddedAttachments.get(), temp.embeddedAttachments.get())) {
                equals = false;
            }
            return equals;
        } else {
            return false;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.controller;

import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import com.quannguyen.jagemailclient.email.data.EmailBean;
import com.quannguyen.jagemailclient.email.data.EmailUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.mail.Flags;
import jodd.mail.*;
/**
 * Email Receiver module that uses IMAP mail server that handles 
 * receiving and parsing of new received emails of an email address
 * @author Anh Quan Nguyen
 */
class EmailReceiver {
    private ImapServer imapServer;
    private final String IMAP_SEVER_HOST = "imap.gmail.com";
    
    /**
     * Take in email address and password to authenticate and create 
     * ImapServer
     * 
     * @param emailAddress 
     * @param password 
     */
    public EmailReceiver(EmailAddressBean emailAddress, String password) {
        createImapServer(emailAddress.getEmailAddress(), password);
    }
    /**
     * Create a new ImapSever with authentication info
     * 
     * @param address
     * @param password 
     */
    private void createImapServer(String address, String password) {
        this.imapServer = MailServer.create()
                .ssl(true)
                .host(IMAP_SEVER_HOST)
                .auth(address, password)
                .buildImapMailServer();
    }
    
    /**
     * Methods that retrieve new and unread emails through an ImapSever
     * 
     * @return List of new unread mail 
     */
    public List<EmailBean> receiveEmail() {
        try(ReceiveMailSession  session = this.imapServer.createSession()) {
            session.open();
            List<ReceivedEmail> emails = Arrays.asList(getNewUnreadMail(session));
            
            if(emails != null && emails.size() > 0) {
                return emails.stream().map(e -> EmailUtil.parseJoddMailToEmailBean(e))
                        .collect(Collectors.toList());
            } else {
                return new ArrayList<>();
            }
        } 
    }
    
    /**
     * Get new unread mails from a mail session
     * 
     * @param session
     * @return Arrays of new unread mails
     */
    private ReceivedEmail[] getNewUnreadMail(ReceiveMailSession session) {
        return session.receiveEmailAndMarkSeen(EmailFilter
                       .filter().flag(Flags.Flag.SEEN, false));
    }
    
    
}

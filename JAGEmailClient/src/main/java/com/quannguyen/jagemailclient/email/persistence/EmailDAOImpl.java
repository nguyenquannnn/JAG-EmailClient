/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.email.persistence;

import com.quannguyen.jagemailclient.db.controller.DbConnectionHandler;
import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import com.quannguyen.jagemailclient.email.data.EmailAttachmentBean;
import com.quannguyen.jagemailclient.email.data.EmailBean;
import com.quannguyen.jagemailclient.folder.data.FolderBean;
import com.quannguyen.jagemailclient.folder.persistence.FolderDAO;
import com.quannguyen.jagemailclient.folder.persistence.FolderDAOImpl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that implements the EmailDAO interface, implementing the CRUD operation 
 * regarding the email table.  
 * @author Quan-Nguyen
 */
public class EmailDAOImpl implements EmailDAO {
    private final DbConnectionHandler handler;
    private static final Logger LOG = LoggerFactory.getLogger(EmailDAOImpl.class);
    /**
     * Get a handler to the connection of Database
     * 
     * @param handler 
     */
    public EmailDAOImpl(DbConnectionHandler handler) {
        super();
        this.handler = handler;
    }
    /**
     * Retrieve all emails from a specified folder
     * 
     * @param folderBean Specified folder
     * @return List of emails
     * @throws SQLException 
     */
    @Override
    public List<EmailBean> getEmailsFromFolder(FolderBean folderBean) throws SQLException,IllegalArgumentException {
        if (folderBean.getId() <= 0 || folderBean.getFolderName() == null || folderBean.getFolderName().isEmpty()) {
            throw new IllegalArgumentException("EmailDAOImpl: Invalid folder_id.");
        }
        
        String query = "SELECT * FROM email e "
                + "JOIN folder f USING (folder_id) "
                + "JOIN address a ON e.from_address = a.address_id "
                + "WHERE folder_id = ? OR f.folder_name = ?";
        List<EmailBean> res = new ArrayList<>();
        LOG.info("Fetching Email From Folder: " +  folderBean.getFolderName());
        
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1, folderBean.getId());
            ps.setString(2, folderBean.getFolderName());
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    res.add(createEmailBean(rs));
                }
            }
        }
        return res;
    }
    /**
     * Retrieve an email with a specified  id
     * 
     * @param id
     * @return An EmailBean
     * @throws SQLException 
     */
    @Override
    public EmailBean getEmail(int id) throws SQLException {
        String query = "SELECT * FROM email e WHERE email_id = ?";
        EmailBean row = null;
        
        LOG.info("Fetching email.");
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1, id);
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    row = createEmailBean(rs);
                }
            }
        }
        return row;
    }
    /**
     * Create an EmailBean from the ResultSet of an SQL query
     * @param rs ResultSet of an SQL query
     * @return EmailBean
     * @throws SQLException 
     */
    private EmailBean createEmailBean(ResultSet rs) throws SQLException {
        EmailAddressDAO addressDAO = new EmailAddressDAOImpl(handler);
        EmailAttachmentDAO attachmentDAO = new EmailAttachmentDAOImpl(handler);
        FolderDAO folderDAO = new FolderDAOImpl(handler);
        EmailBean temp = new EmailBean();
        
        temp.setId(rs.getInt("EMAIL_ID"));
        temp.setFrom(addressDAO.getEmailAddressFrom(temp.getId()));
        temp.setTo(addressDAO.getEmailAddressToFromEmail(temp.getId()));
        temp.setCc(addressDAO.getEmailAddressCcFromEmail(temp.getId()));
        temp.setBcc(addressDAO.getEmailAddressBccFromEmail(temp.getId()));
        temp.setSubject(rs.getString("SUBJECT"));
        temp.setTextMessage(rs.getString("TEXT_MESSAGE"));
        temp.setHtmlMessage(rs.getString("HTML_MESSAGE"));

        
        temp.setAttachments(attachmentDAO.getEmailAttachmentsOfEmail(temp.getId()));
        temp.setEmbeddedAttachments(attachmentDAO.getEmbeddedEmailAttachmentsOfEmail(temp.getId()));
        temp.setFolderId(folderDAO.getFolder(rs.getInt("FOLDER_ID")).getId());
        
        temp.setPriority(rs.getInt("PRIORITY"));
        if(rs.getTimestamp("RECEIVE_TIME") != null) {
            temp.setReceiveDateTime(rs.getTimestamp("RECEIVE_TIME").toLocalDateTime());
        }
        temp.setSendDateTime(rs.getTimestamp("SEND_TIME").toLocalDateTime());
        return temp;
    }
    /**
     * Add List<EmailAddress> to email_to_address bridging table for specified email
     * @param list List of To EmailAddress
     * @param emailId
     * @param c
     * @return result of persistence operation
     * @throws SQLException 
     */
    private int addEmailToAddressBridging(List<EmailAddressBean> list, int emailId, Connection c) throws SQLException {
        EmailAddressDAO addressDAO = new EmailAddressDAOImpl(handler);
        int result = 0;
        String insertQuery = "INSERT INTO email_to_address(email_id, to_email_id) "
                + "VALUES (?, ?)";
        
        PreparedStatement ps = c.prepareStatement(insertQuery);
        for(EmailAddressBean b: list) {
            addressDAO.createEmailAddress(b);
            ps.setInt(1, emailId);
            ps.setInt(2, b.getId());
            result +=  ps.executeUpdate();
        }
        return result;
    }
    /**
     * Add List<EmailAddress> to email_cc_address bridging table for specified email
     * @param list List of cc EmailAddress
     * @param emailId
     * @param c
     * @return result of persistence operation
     * @throws SQLException 
     */    
    private int addEmailCcAddressBridging(List<EmailAddressBean> list, int emailId, Connection c) throws SQLException {
        EmailAddressDAO addressDAO = new EmailAddressDAOImpl(handler);
        int result = 0;
        
        if(list == null) {
           throw new IllegalArgumentException("EmailDAOImpl: Null input");
        }
        
        String insertQuery = "INSERT INTO email_cc_address(email_id, cc_email_id) "
                + "VALUES (?, ?)";
        
        PreparedStatement ps = c.prepareStatement(insertQuery);
        for(EmailAddressBean b: list) {
            addressDAO.createEmailAddress(b);
            ps.setInt(1, emailId);
            ps.setInt(2, b.getId());
            result +=  ps.executeUpdate();
        }
        return result;
    }
    /**
     * Add List<EmailAddress> to email_bcc_address bridging table for specified email
     * @param list List of Bcc EmailAddress
     * @param emailId
     * @param c
     * @return result of persistence operation
     * @throws SQLException 
     */
    private int addEmailBccAddressBridging(List<EmailAddressBean> list, int emailId, Connection c) throws SQLException {
        EmailAddressDAO addressDAO = new EmailAddressDAOImpl(handler);
        int result = 0;
        String insertQuery = "INSERT INTO email_bcc_address(email_id, bcc_email_id) "
                + "VALUES (?, ?)";
        
        if(list == null) {
           throw new IllegalArgumentException("EmailDAOImpl: Null input");
       }
        
        PreparedStatement ps = c.prepareStatement(insertQuery);
        for(EmailAddressBean b: list) {
            addressDAO.createEmailAddress(b);
            ps.setInt(1, emailId);
            ps.setInt(2, b.getId());
            result +=  ps.executeUpdate();
        }
        return result;
    }
    /**
     * Add List<EmailAddress> to email_attachment bridging table for specified email
     * @param list List of EmailAttachment
     * @param emailId
     * @param c
     * @return result of persistence operation
     * @throws SQLException 
     */    
    private int addEmailAttachmentBridging(List<EmailAttachmentBean> list, int emailId, Connection c) throws SQLException {
        EmailAttachmentDAO attachmentDAO = new EmailAttachmentDAOImpl(handler);
        int result = 0;
        String insertQuery = "INSERT INTO email_attachment(email_id, attachment_id) "
                + "VALUES (?, ?)";
        if(list == null) {
           throw new IllegalArgumentException("EmailDAOImpl: Null input");
       }
        PreparedStatement ps = c.prepareStatement(insertQuery);
        for(EmailAttachmentBean ea: list) {
            attachmentDAO.createEmailAttachment(ea);
            ps.setInt(1, emailId);
            ps.setInt(2, ea.getId());
            result +=  ps.executeUpdate();
        }
        return result;
    }
    /**
     * Create a new Email record from an EmailBean
     * 
     * @param emailBean
     * @return result of persistence operation
     * @throws SQLException 
     */  
    @Override
    public int createEmail(EmailBean emailBean) throws SQLException {
        String insertQuery = "INSERT INTO email(from_address, subject, text_message, "
                + "html_message, priority, folder_id, send_time, receive_time) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        int result = 0;
        if(emailBean == null) {
            throw new IllegalArgumentException("EmailDAOImpl: Null input");
        }
        
        LOG.info("Creating email record.");
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
            EmailAddressDAO addressDAO = new EmailAddressDAOImpl(handler);
            addressDAO.createEmailAddress(emailBean.getFrom());
            ps.setInt(1, emailBean.getFrom().getId());
            ps.setString(2, emailBean.getSubject());
            ps.setString(3, emailBean.getTextMessage());
            ps.setString(4, emailBean.getHtmlMessage());
            ps.setInt(5, emailBean.getPriority());
            ps.setInt(6, emailBean.getFolderId());
            ps.setTimestamp(7, Timestamp.valueOf(emailBean.getSendDateTime()));
            if(emailBean.getReceiveDateTime() != null) {
                 ps.setTimestamp(8, Timestamp.valueOf(emailBean.getReceiveDateTime()));
            } else {
                 ps.setTimestamp(8, null);
            }
            
            result = ps.executeUpdate();
            
            try (ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                emailBean.setId(recordNum);
            }
            
            addEmailBccAddressBridging(emailBean.getBcc(), emailBean.getId(), c);
            addEmailCcAddressBridging(emailBean.getCc(), emailBean.getId(), c);
            addEmailToAddressBridging(emailBean.getTo(), emailBean.getId(), c);
            addEmailAttachmentBridging(emailBean.getAttachments(), emailBean.getId(), c);
            addEmailAttachmentBridging(emailBean.getEmbeddedAttachments(), emailBean.getId(), c);
        }
        return result; 
    }
    /**
     * Delete an email record with a specified id
     * 
     * @param id
     * @return result of persistence operation
     * @throws SQLException 
     */        
    @Override
    public int deleteEmail(int id) throws SQLException {
        String deleteQuery = "DELETE FROM email WHERE email_id = ?";
        int result = 0;
        
        LOG.info("Deleting email record: " + id);
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(deleteQuery);
            
            ps.setInt(1, id);
            result = ps.executeUpdate();
        }
        return result; 
    }
    /**
     * Update an email record with an EmailBean
     * 
     * @param emailBean
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int updateEmail(EmailBean emailBean) throws SQLException {
        String insertQuery = "UPDATE email SET "
                + "from_address = ?, subject = ?, text_message = ?, html_message = ?, "
                + "folder_id = ? "
                + "WHERE email_id = ?";
        int result = 0;
        
        if(emailBean == null) {
           throw new IllegalArgumentException("EmailDAOImpl: Null input");
        }
        
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(insertQuery);
            ps.setInt(1, emailBean.getFrom().getId());
            ps.setString(2, emailBean.getSubject());
            ps.setString(3, emailBean.getTextMessage());
            ps.setString(4, emailBean.getHtmlMessage());
            ps.setInt(5, emailBean.getFolderId());
            ps.setInt(6, emailBean.getId());
            
            result = ps.executeUpdate();
        }
        return result;
    }
    
    @Override
    public int changeFolder(EmailBean emailBean, int folderId) throws SQLException {
        String insertQuery = "UPDATE email SET "
                + "folder_id = ? "
                + "WHERE email_id = ?";
        int result = 0;
        
        if(emailBean == null) {
           throw new IllegalArgumentException("EmailDAOImpl: Null input");
        }
        
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(insertQuery);
            ps.setInt(1, folderId);
            ps.setInt(2, emailBean.getId());
            
            result = ps.executeUpdate();
        }
        return result;
    }
}

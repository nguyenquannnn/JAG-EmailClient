/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.folder.persistence;

import com.quannguyen.jagemailclient.folder.data.FolderBean;
import com.quannguyen.jagemailclient.db.controller.DbConnectionHandler;
import com.quannguyen.jagemailclient.email.persistence.EmailDAOImpl;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that implements the FolderDAO interface, implementing the CRUD operation 
 * regarding the folder table.  
 * @author Quan-Nguyen
 */
public class FolderDAOImpl implements FolderDAO {
    private final DbConnectionHandler handler;
    private static final Logger LOG = LoggerFactory.getLogger(FolderDAOImpl.class);
    /**
     * Get a handler to the connection of Database
     * 
     * @param handler 
     */
    public FolderDAOImpl(DbConnectionHandler handler) {
        super();
        this.handler = handler;
    }
    /**
     * Retrieve all folder within the database.
     * 
     * @return List of all folder within the database
     * @throws SQLException 
     */
    @Override
    public List<FolderBean> getAllFolder() throws SQLException {
        String query = "SELECT * FROM folder";
        List<FolderBean> res = new ArrayList<>();
        
        LOG.info("Fetching all folders.");
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    res.add(createFolderBean(rs));
                }
            }
        }
        return res;
    }
    /**
     * Get folder with specified id
     * 
     * @param id
     * @return FolderBean a Folder 
     * @throws SQLException 
     */    
    @Override
    public FolderBean getFolder(int id) throws SQLException {
       String query = "SELECT * FROM folder WHERE folder_id  = ?";
       FolderBean res = null;
        
       LOG.info("Fetching folder.");
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(query);
            ps.setInt(1, id);
            
            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    res = createFolderBean(rs);
                }
            }
        }
        return res;
    }
    /**
     * Create a new record from a FolderBean in the database
     * 
     * @param bean Folder to create
     * @return result of persistence operation
     * @throws SQLException 
     */    
    private FolderBean createFolderBean(ResultSet rs) throws SQLException {
        return new FolderBean(rs.getInt("FOLDER_ID"), rs.getString("FOLDER_NAME"), rs.getInt("FOLDER_PARENT"));
    }
    /**
     * Create a new record from a FolderBean in the database
     * 
     * @param bean Folder to create
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int createFolder(FolderBean bean) throws SQLException {
        String insertQuery = "INSERT INTO folder(folder_name) VALUES (?)";
        int result = 0;
        
        if(bean == null) {
            throw new IllegalArgumentException("FolderDAO: null input");
        }
        
        LOG.info("Creating folder");
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
            
            ps.setString(1, bean.getFolderName());
            result = ps.executeUpdate();
            
            try (ResultSet rs = ps.getGeneratedKeys();) {
                int recordNum = -1;
                if (rs.next()) {
                    recordNum = rs.getInt(1);
                }
                bean.setId(recordNum);
            } 
        }
        return result; 
    }
    /**
     * Delete a folder with the specified id
     * 
     * @param id
     * @return result of persistence operation
     * @throws SQLException 
     */
    @Override
    public int deleteFolder(int id) throws SQLException {
        String deleteQuery = "DELETE FROM folder WHERE folder_id = ?";
        int result = 0;
        
        LOG.info("Deleting folder record.");
        try (Connection c = handler.getConnection()) {
            PreparedStatement ps = c.prepareStatement(deleteQuery);
            
            ps.setInt(1, id);
            result = ps.executeUpdate();
        }
        return result; 
    }
}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.folder.persistence;

import com.quannguyen.jagemailclient.folder.data.FolderBean;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface that abstracts the CRUD operation regarding the folder table.
 * 
 * @author Quan-Nguyen
 */
public interface FolderDAO {
    /**
     * Retrieve all folder within the database.
     * 
     * @return List of all folder within the database
     * @throws SQLException 
     */
    List<FolderBean> getAllFolder() throws SQLException;
    /**
     * Get folder with specified id
     * 
     * @param id
     * @return FolderBean a Folder 
     * @throws SQLException 
     */
    FolderBean getFolder(int id) throws SQLException;
    /**
     * Create a new record from a FolderBean in the database
     * 
     * @param bean Folder to create
     * @return result of persistence operation
     * @throws SQLException 
     */
    int createFolder(FolderBean bean) throws SQLException;
    /**
     * Delete a folder with the specified id
     * 
     * @param id
     * @return result of persistence operation
     * @throws SQLException 
     */
    int deleteFolder(int id) throws SQLException;
}

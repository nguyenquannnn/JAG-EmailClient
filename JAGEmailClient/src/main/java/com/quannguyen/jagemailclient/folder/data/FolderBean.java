/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.folder.data;

import java.io.Serializable;
import java.util.Objects;
import javafx.beans.property.*;

/**
 * JavaBean class that represent a Folder unit to store emails and other folder.
 * @author Quan-Nguyen
 */
public class FolderBean implements Serializable{
    private IntegerProperty id;
    private StringProperty folderName;
    private IntegerProperty folderParent;
    /**
     * Initialize folder with specified folder name and id of 0 and no parent folder
     * @param folderName 
     */
    public FolderBean(String folderName) {
        this(0, folderName, 0);
    }
    /**
     * Initialize folder with specified folder name and a parent folder, with id of 0
     * 
     * @param folderName
     * @param folderParent 
     */
    public FolderBean(String folderName, int folderParent) {
        this(0, folderName, folderParent);
    }
    /**
     * 3 params constructor.
     * 
     * @param id
     * @param folderName
     * @param folderParent 
     */
    public FolderBean(int id, String folderName, int folderParent) {
        this.id = new SimpleIntegerProperty();
        this.folderName = new SimpleStringProperty();
        this.folderParent = new SimpleIntegerProperty();
        this.id.set(id);
        this.folderName.set(folderName);
        this.folderParent.set(folderParent);
    }

    public String getFolderName() {
        return this.folderName.get();
    }

    public void setFolderName(String folderName) {
        this.folderName.set(folderName);
    }

    public int getId() {
        return this.id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public int getFolderParent() {
        return folderParent.get();
    }

    public void setFolderParent(int folderParent) {
        this.folderParent.set(folderParent);
    }
    
    public IntegerProperty folderParent() {
        return this.folderParent;
    }
    
    public StringProperty folderName() {
        return this.folderName;
    }
    
    public IntegerProperty id() {
        return this.id;
    }
    /**
     * Calculate the hash of this FolderBean
     * 
     * @return int Hash value 
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.folderName.get());
        hash = 89 * hash + this.folderParent.get();
        return hash;
    }
    /**
     * Check to see if another object is the same FolderBean.
     * Return true if they both have same name and same parent folder id.
     * 
     * @param obj
     * @return Return true if they both have same name and same parent folder id.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FolderBean other = (FolderBean) obj;
        if (this.folderParent.get() != other.folderParent.get()) {
            return false;
        }
        if (!Objects.equals(this.folderName.get(), other.folderName.get())) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Folder: " + this.getFolderName() + "(" + this.getId() +")";
    }
}

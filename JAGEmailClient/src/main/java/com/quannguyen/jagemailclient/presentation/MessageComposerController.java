/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.presentation;

import com.quannguyen.jagemailclient.email.controller.EmailController;
import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import com.quannguyen.jagemailclient.email.data.EmailAttachmentBean;
import com.quannguyen.jagemailclient.email.data.EmailBean;
import com.quannguyen.jagemailclient.email.data.EmailType;
import com.quannguyen.jagemailclient.email.data.EmailUtil;
import com.quannguyen.jagemailclient.email.persistence.EmailDAO;
import com.quannguyen.jagemailclient.folder.data.FolderBean;
import com.quannguyen.jagemailclient.presentation.thread.SendEmailThread;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author 1632647
 */
public class MessageComposerController implements Initializable {

    private static final Logger LOG = LoggerFactory.getLogger(MessageComposerController.class);
    private EmailDAO emailDAO;
    private EmailController emailController;
    private FolderBean sentFolder;
    private EmailAddressBean from;
    private EmailBean emailBean;
    private List<EmailAttachmentBean> attachments;
    private List<EmailAttachmentBean> embeddedAttachments;
    @FXML
    private Button buttonSend;
    @FXML
    private TextField textfieldTo;
    @FXML
    private TextField textfieldCc;
    @FXML
    private TextField textfieldBcc;
    @FXML
    private TextField textfieldSubject;
    @FXML
    private Button buttonAttachment;
    @FXML
    private HTMLEditor editor;
    @FXML
    private TextArea textareaMessage;
    @FXML
    private Button buttonEmbeddedAttachment;
    @FXML
    private HBox buttonbarAttachments;
    @FXML
    private Tab t;

    public MessageComposerController() {
        super();
        attachments = new ArrayList<EmailAttachmentBean>();
        embeddedAttachments = new ArrayList<EmailAttachmentBean>();
        sentFolder = new FolderBean(2, "Sent", 0);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        buttonSend.setOnAction(this::buttonSendHandler);
        buttonAttachment.setOnAction(this::buttonAttachmentHandler);
        buttonEmbeddedAttachment.setOnAction(this::buttonEmbeddedAttachmentHandler);
    }

    /**
     *
     * @param e
     */
    public void buttonAttachmentHandler(ActionEvent e) {
        Stage popup = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose attachment files!");

        File selectedFile = fileChooser.showOpenDialog(popup);
        if (selectedFile != null) {
            Button attachment = createAttachment(selectedFile);

            try {
                EmailAttachmentBean emailAttachment = new EmailAttachmentBean(selectedFile);
                attachments.add(emailAttachment);
                buttonbarAttachments.getChildren().add(attachment);
            } catch (IOException ex) {
                LOG.error(null, ex);
                errorAlert("Error loading attachment!");
                Platform.exit();
            }
        }
    }

    public void buttonEmbeddedAttachmentHandler(ActionEvent e) {
        Stage popup = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose attachment files!");

        File selectedFile = fileChooser.showOpenDialog(popup);
        if (selectedFile != null) {
            try {
                EmailAttachmentBean emailAttachment = new EmailAttachmentBean(selectedFile, true);
                embeddedAttachments.add(emailAttachment);

                String body = editor.getHtmlText()
                        + String.format("<img src='%s' alt='%s'>", selectedFile.toURI().toString(),
                                selectedFile.getName());

                editor.setHtmlText(body);
            } catch (IOException ex) {
                LOG.error(null, ex);
                errorAlert("Error loading attachment!");
                Platform.exit();
            }
        }
    }

    private List<EmailAddressBean> parseEmailAddress(String value) {
        if (value != null && !value.isEmpty()) {
            List<EmailAddressBean> res = new ArrayList<>();
            String[] addresses = value.split(",");
            for (int i = 0; i < addresses.length; i++) {
                addresses[i] = addresses[i].trim();
                try {
                    if (!addresses[i].isEmpty() && EmailUtil.checkEmailAddress(addresses[i])) {
                        res.add(new EmailAddressBean(addresses[i], ""));
                    }
                } catch (IllegalArgumentException iae) {
                    LOG.error(null, iae);
                    errorAlert("Invalid email address: " + addresses[i]);
                    Platform.exit();
                }
            }
            return res;
        } else {
            return new ArrayList<>();
        }
    }

    /**
     *
     * @param e
     */
    public void buttonSendHandler(ActionEvent e) {
        if (emailBean == null) {
            emailBean = new EmailBean();
        }
        emailBean.setFrom(from);
        emailBean.setTo(parseEmailAddress(textfieldTo.getText()));
        emailBean.setBcc(parseEmailAddress(textfieldBcc.getText()));
        emailBean.setCc(parseEmailAddress(textfieldCc.getText()));

        emailBean.setSubject(textfieldSubject.getText());
        emailBean.setTextMessage(textareaMessage.getText());
        emailBean.setHtmlMessage(editor.getHtmlText());

        emailBean.setAttachments(attachments);
        emailBean.setEmbeddedAttachments(embeddedAttachments);

        emailBean.setFolderId(sentFolder.getId());
        
        SendEmailThread emailThread = new SendEmailThread(emailDAO, emailController, emailBean);
        emailThread.start();
        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        LOG.info("buttonSendHandler event.");
        stage.close();
    }

    public void setEmailDAO(EmailDAO emailDAO) {
        this.emailDAO = emailDAO;
    }

    public void setEmailController(EmailController controller) {
        this.emailController = controller;
    }

    public void setFrom(EmailAddressBean from) {
        this.from = from;
    }

    private void errorAlert(final String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("Error!");
        dialog.setHeaderText("Error!");
        dialog.setContentText(msg);
        dialog.show();
    }

    public void responseToEmail(EmailBean bean, EmailType type) {
        if (bean.getFrom() != null && !bean.getFrom().getEmailAddress().isEmpty()) {
            textfieldTo.textProperty().set(bean.getFrom().getEmailAddress());
        }
        setEmailView(bean, type);
    }

    public void responseToAllEmail(EmailBean bean, EmailType type) {
        String temp = bean.getFrom().getEmailAddress() + ", ";
        if (bean.getFrom() != null && !bean.getFrom().getEmailAddress().isEmpty()) {
            textfieldTo.textProperty().set(temp);
        }
        if (bean.getTo() != null && bean.getTo().size() > 0) {
            List<EmailAddressBean> emailAddressBeans = bean.getTo();

            for (EmailAddressBean e : emailAddressBeans) {
                if (!e.getEmailAddress().equalsIgnoreCase(from.getEmailAddress())) {
                    temp += e.getEmailAddress() + ", ";
                }
            }
            textfieldTo.textProperty().set(temp);
        }

        temp = "";
        if (bean.getCc() != null && bean.getCc().size() > 0) {
            List<EmailAddressBean> emailAddressBeans = bean.getCc();

            for (EmailAddressBean e : emailAddressBeans) {
                if (!e.getEmailAddress().equalsIgnoreCase(from.getEmailAddress())) {
                    temp += e.getEmailAddress() + ", ";
                }
            }
            textfieldCc.textProperty().set(temp);
        }
        setEmailView(bean, type);
    }

    public void forwardEmail(EmailBean bean, EmailType type) {
        setEmailView(bean, type);
    }

    private void setEmailView(EmailBean bean, EmailType type) {
        if (bean.getSubject() != null) {
            //bean.setSubject(type.getCode() + bean.getSubject());
            textfieldSubject.textProperty().set(type.getCode() + bean.getSubject());
        }

        if (bean.getHtmlMessage() != null && !bean.getHtmlMessage().isEmpty()) {
            editor.setHtmlText(bean.getHtmlMessage());
        }

        if (bean.getTextMessage() != null && !bean.getTextMessage().isEmpty()) {
            textareaMessage.setText(bean.getTextMessage());
        }

        if (bean.getAttachments() != null && bean.getAttachments().size() > 0) {
            for (EmailAttachmentBean a : bean.getAttachments()) {
                buttonbarAttachments.getChildren().add(createAttachment(a));
            }
        }
    }

    private Button createAttachment(EmailAttachmentBean bean) {
        Button attachment = new Button();
        attachment.getStylesheets().add(getClass().getResource("/styles/Styles.css").toString());
        attachment.getStyleClass().add("attachment");
        attachment.setText(bean.getAttachmentName());
        attachment.setId(bean.getAttachmentName());
        attachment.setOnAction(value -> {
            HBox group = (HBox) ((Button) value.getTarget()).getParent();
            group.getChildren().remove(attachment);
        });
        return attachment;
    }
    
    private Button createAttachment(File file) {
        Button attachment = new Button();
        attachment.getStylesheets().add(getClass().getResource("/styles/Styles.css").toString());
        attachment.getStyleClass().add("attachment");
        attachment.setText(file.getName());
        attachment.setId(file.getName());
        attachment.setOnAction(value -> {
            HBox group = (HBox) ((Button) value.getTarget()).getParent();
            group.getChildren().remove(attachment);
        });
        return attachment;
    }
}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.presentation;

import com.quannguyen.jagemailclient.email.controller.EmailController;
import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import com.quannguyen.jagemailclient.email.data.EmailAttachmentBean;
import com.quannguyen.jagemailclient.email.data.EmailBean;
import com.quannguyen.jagemailclient.email.data.EmailType;
import com.quannguyen.jagemailclient.email.persistence.EmailDAO;
import com.quannguyen.jagemailclient.folder.persistence.FolderDAO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author 1632647
 */
public class MessageDisplayController implements Initializable {

    private static final Logger LOG = LoggerFactory.getLogger(MessageDisplayController.class);
    private EmailBean email;

    private EmailDAO emailDAO;
    private EmailController emailController;
    private ConfigurationFXController configController;
    @FXML
    private Label labelFromUsername;
    @FXML
    private Label labelFromEmailAddress;
    @FXML
    private Label labelEmailSendDate;
    @FXML
    private Button buttonReplyTo;
    @FXML
    private Button buttonReplyAll;
    @FXML
    private Button buttonForward;
    @FXML
    private Button buttonDelete;
    @FXML
    private Label labelEmailSubject;
    @FXML
    private Label labelEmailToAddress;
    @FXML
    private Label labelEmailCcAddress;
    @FXML
    private WebView webviewContent;
    @FXML
    private HBox buttonbarAttachment;
    @FXML
    private TextArea textareaMessage;

    public MessageDisplayController(EmailBean bean) {
        super();
        this.email = bean;
    }

    public MessageDisplayController() {
        super();
        this.email = createMockEmail();
    }

    private EmailBean createMockEmail() {
        EmailAddressBean from = new EmailAddressBean("send.1632647@gmail.com", "Send Test");
        EmailAddressBean to = new EmailAddressBean("receive.1632647@gmail.com", "Receive Test");
        EmailAddressBean cc = new EmailAddressBean("other.1632647@gmail.com", "Other TestS");
        EmailBean bean = new EmailBean();
        bean.setFrom(from);
        bean.setTo(Arrays.asList(new EmailAddressBean[]{to}));
        bean.getCc().add(cc);
        bean.setSubject("Hello this is Main!");
        bean.setHtmlMessage("<META http-equiv=Content-Type content=\"text/html; "
                + "charset=utf-8\"><body><h1>Hey!</h1><img src='cid:embed2.jpg'>"
                + "<h2>Hay!</h2></body></html>");
        return bean;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    private void bindProperties() {
        labelFromUsername.textProperty().bind(email.getFrom().personnalName());
        labelFromEmailAddress.textProperty().bind(email.getFrom().emailAddress());

        labelEmailSubject.textProperty().bind(email.subject());
        String toAddresses = email.getTo().stream()
                .map(EmailAddressBean::getEmailAddress)
                .collect(Collectors.joining(", "));
        labelEmailToAddress.textProperty().set(toAddresses);

        String ccAddresses = email.getCc().stream()
                .map(EmailAddressBean::getEmailAddress)
                .collect(Collectors.joining(", "));
        labelEmailCcAddress.textProperty().set(ccAddresses);

        labelEmailSendDate.textProperty().set(email.getSendDateTime().toString());

        buttonReplyTo.setOnAction(this::buttonReplyHandler);
        buttonReplyAll.setOnAction(this::buttonReplyAllHandler);
        buttonForward.setOnAction(this::buttonForwardHandler);
        buttonDelete.setOnAction(this::buttonDeleteHandler);
        WebEngine webEngine = webviewContent.getEngine();
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(webviewContent);
        webEngine.loadContent(email.getHtmlMessage());

        textareaMessage.textProperty().set(email.getTextMessage());
    }

    public void setEmailBean(EmailBean bean) {
        this.email = bean;
    }

    public void setEmailView(EmailBean bean) {
        setEmailBean(bean);
        bindProperties();

        if (bean.getAttachments() != null && bean.getAttachments().size() > 0) {
            for (EmailAttachmentBean a : bean.getAttachments()) {
                buttonbarAttachment.getChildren().add(createAttachment(a));
            }
        }
    }

    public void buttonReplyHandler(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/MessageComposer.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();
            MessageComposerController composerController = loader.getController();
            composerController.setEmailController(emailController);
            composerController.setEmailDAO(emailDAO);
            composerController.setFrom(configController.getController().getConfigBean().getUser());
            composerController.responseToEmail(email, EmailType.REPLY);

            Scene scene = new Scene(rootLayout);
            Stage popup = new Stage();
            popup.setScene(scene);
            popup.setTitle("Reply to an email.");
            popup.show();
            Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            primaryStage.hide();
        } catch (IOException ioe) {
            LOG.error(null, ioe);
            errorAlert("initComposeLayout()");
        }
    }

    public void buttonReplyAllHandler(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/MessageComposer.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();
            MessageComposerController composerController = loader.getController();
            composerController.setEmailController(emailController);

            composerController.setEmailDAO(emailDAO);
            composerController.setFrom(configController.getController().getConfigBean().getUser());
            composerController.responseToAllEmail(email, EmailType.REPLY);

            Scene scene = new Scene(rootLayout);
            Stage popup = new Stage();
            popup.setScene(scene);
            popup.setTitle("Reply to all of an email.");
            popup.show();
            Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            primaryStage.hide();
        } catch (IOException ioe) {
            LOG.error(null, ioe);
            errorAlert("initComposeLayout()");
        }

    }

    public void buttonForwardHandler(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/MessageComposer.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();
            MessageComposerController composerController = loader.getController();
            composerController.setEmailController(emailController);
            composerController.setEmailDAO(emailDAO);
            composerController.setFrom(configController.getController().getConfigBean().getUser());
            composerController.forwardEmail(email, EmailType.FORWARD);

            Scene scene = new Scene(rootLayout);
            Stage popup = new Stage();
            popup.setScene(scene);
            popup.setTitle("Forward an email.");
            popup.show();
            Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            primaryStage.hide();
        } catch (IOException ioe) {
            LOG.error(null, ioe);
            errorAlert("initComposeLayout()");
        }

    }

    public void buttonDeleteHandler(ActionEvent event) {
        try {
            //call to DAO
            emailDAO.deleteEmail(email.getId());
//            displayEmailTable();
        } catch (SQLException ex) {
            LOG.error(null, ex);
            errorAlert("Error deleting email!");
        }
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Email deleted!");
        dialog.setHeaderText("The selected email has been deleted!");
        dialog.setContentText("");
        dialog.show();
        Stage primaryStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        primaryStage.hide();
    }

    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("Error!");
        dialog.setHeaderText("Error!");
        dialog.setContentText(msg);
        dialog.show();
    }

    public void setEmailDAO(final EmailDAO emailDAO) {
        this.emailDAO = emailDAO;
    }

    public void setEmailController(final EmailController emailController) {
        this.emailController = emailController;
    }

    public void setConfigController(final ConfigurationFXController configController) {
        this.configController = configController;
    }

    private Button createAttachment(EmailAttachmentBean bean) {
        Button attachment = new Button();
        attachment.getStylesheets().add(getClass().getResource("/styles/Styles.css").toString());
        attachment.getStyleClass().add("attachment");
        attachment.setText(bean.getAttachmentName());
        attachment.setId(bean.getAttachmentName());
        attachment.setOnAction(value -> {
            saveAttachment(bean,
                    (Stage) ((Node) value.getSource()).getScene().getWindow());
        });
        return attachment;
    }

    private Button createAttachment(File file) {
        Button attachment = new Button();
        attachment.getStylesheets().add(getClass().getResource("/styles/Styles.css").toString());
        attachment.getStyleClass().add("attachment");
        attachment.setText(file.getName());
        attachment.setId(file.getName());
        attachment.setOnAction(value -> {
            HBox group = (HBox) ((Button) value.getTarget()).getParent();
            group.getChildren().remove(attachment);
        });
        return attachment;
    }

    private void saveAttachment(EmailAttachmentBean bean, Stage stage) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        File selectedDirectory = directoryChooser.showDialog(stage);
        if (selectedDirectory != null) {
            try (FileOutputStream fos = new FileOutputStream(selectedDirectory.getAbsolutePath() 
                    + "/" + bean.getAttachmentName())) {
                fos.write(bean.getData());
            } catch (IOException ex) {
                LOG.error(null, ex);
                errorAlert("Error saving attachment!");
            }
        }
    }
}

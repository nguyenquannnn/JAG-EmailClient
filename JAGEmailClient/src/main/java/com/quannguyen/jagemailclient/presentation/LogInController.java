/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.presentation;

import com.quannguyen.jagemailclient.db.controller.DbConnectionHandler;
import com.quannguyen.jagemailclient.db.controller.DbController;
import com.quannguyen.jagemailclient.db.controller.DbControllerImpl;
import com.quannguyen.jagemailclient.email.controller.EmailController;
import com.quannguyen.jagemailclient.email.controller.EmailControllerImpl;
import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import com.quannguyen.jagemailclient.email.persistence.EmailDAOImpl;
import com.quannguyen.jagemailclient.folder.persistence.FolderDAOImpl;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author 1632647
 */
public class LogInController implements Initializable {
    private static final Logger LOG = LoggerFactory.getLogger(LogInController.class);
    private EmailController emailController;
    private DbController dbControler;
    private MainController mainController; 
    
    @FXML
    private TextField textEmail;
    @FXML
    private PasswordField textPassword;
    @FXML
    private Button buttonSubmit;
    private Scene mainScene;
    
    public LogInController() {
        super();
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initLogInStage();
    }

    private void initLogInStage() {
        this.buttonSubmit.setOnAction((event) -> {
            String emailAddress = textEmail.getText();
            String password = textPassword.getText();
            try {
                this.emailController = new EmailControllerImpl(new EmailAddressBean(emailAddress, null),
                        password, new EmailAddressBean(emailAddress, null), password, null);
                
                  changeToMainScene(event, emailAddress, password);
            } catch (IllegalArgumentException iae) {
                LOG.error("LogInController initLogInStage(): Invalid authentication.", iae);
                errorAlert("Invalid authentication!");
            }

        });
        LOG.debug("Finishes: initLogInStage()");
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("Error!");
        dialog.setHeaderText("Error!");
        dialog.setContentText(msg);
        dialog.showAndWait();
    }
    
    public EmailController getEmailController() {
        return this.emailController;
    }
    
    private void initMainLayout(String emailAddress, String password) {
        try {           
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class
                    .getResource("/fxml/Main.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();
            this.mainController = loader.getController();
            mainController.setAccountInfo(emailAddress, password);
            mainController.createConfigController();
            mainController.createDbConnection();
            DbConnectionHandler handler = mainController.getDbConnectionHandler();
            dbControler = new DbControllerImpl(handler);
            dbControler.createDatabase();
            mainController.setFolderDAO(new FolderDAOImpl(handler));
            mainController.setEmailDAO(new EmailDAOImpl(handler));
            mainController.setEmailController(emailController);
            mainController.loadContent();
            this.mainScene = new Scene(rootLayout);

        } catch (IOException | SQLException | RuntimeException ex) {
            LOG.error(null, ex);
            errorAlert("Error logging in the system!");
            Platform.exit();
        }
    }
        
    public void changeToMainScene(ActionEvent actionEvent,String emailAddres, String password) {
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        initMainLayout(emailAddres, password);
        primaryStage.setScene(mainScene);
    }
}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.presentation.thread;

import com.quannguyen.jagemailclient.email.controller.EmailController;
import com.quannguyen.jagemailclient.email.data.EmailBean;
import com.quannguyen.jagemailclient.email.persistence.EmailDAO;
import com.quannguyen.jagemailclient.folder.data.FolderBean;
import com.quannguyen.jagemailclient.presentation.MainController;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Alert;

/**
 *
 * @author Quan-Nguyen
 */
public class RefreshingInboxThread extends Thread {

    private EmailDAO emailDAO;
    private EmailController emailController;
    private MainController mainController;

    public RefreshingInboxThread(EmailDAO emailDAO, EmailController emailController, MainController mainController) {
        this.emailDAO = emailDAO;
        this.emailController = emailController;
        this.mainController = mainController;
    }

    @Override
    public void run() {
        try {
            List<EmailBean> unreadNewEmail = emailController.receiveEmail(); 
            unreadNewEmail.stream()
                    .forEach((email) -> {
                        try {
                            emailDAO.createEmail(email);
                        } catch (SQLException ex) {
                            Platform.runLater(new AlertFail("Error refreshing!"));
                        }
                    }
                    );
            final List<EmailBean> emails = emailDAO.getEmailsFromFolder(new FolderBean(1, "Inbox", 0));;
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    mainController.displayEmailTable(emails);
                    
                }
            });
            Platform.runLater(new AlertSuccess("Refreshed!"));
        }  catch (SQLException ex) {
            Platform.runLater(new AlertFail("Error loading emails!"));
        }
    }

    private static class AlertSuccess extends Thread {

        private String msg;

        public AlertSuccess(String message) {
            this.msg = message;
        }

        @Override
        public void run() {
            successAlert(msg);
        }

        private void successAlert(final String msg) {
            Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
            dialog.setTitle("Success!");
            dialog.setHeaderText("Success!");
            dialog.setContentText(msg);
            dialog.show();
        }
    }

    private static class AlertFail extends Thread {

        private String msg;

        public AlertFail(String message) {
            this.msg = message;
        }

        @Override
        public void run() {
            errorAlert(msg);
        }

        private void errorAlert(final String msg) {
            Alert dialog = new Alert(Alert.AlertType.ERROR);
            dialog.setTitle("Error!");
            dialog.setHeaderText("Error!");
            dialog.setContentText(msg);
            dialog.showAndWait();
        }
    }
}

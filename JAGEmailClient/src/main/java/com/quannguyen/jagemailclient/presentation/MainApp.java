package com.quannguyen.jagemailclient.presentation;

import java.io.IOException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class MainApp extends Application {

    private final static Logger LOG = LoggerFactory.getLogger(MainApp.class);
    private Stage stage;
    private Scene logInScene;
    private LogInController logIn;


    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        initLogInLayout();
        showLogInScene();
    }

    private void initLogInLayout() {
        try {
            this.logIn = new LogInController();
            FXMLLoader loader = new FXMLLoader();

            loader.setLocation(MainApp.class
                    .getResource("/fxml/LogIn.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();
            this.logInScene = new Scene(rootLayout);
        } catch (IOException ex) {
            LOG.error(null, ex);
            errorAlert("initRootLayout()");
            Platform.exit();
        }
    }
    
    private void showLogInScene() {
        logInScene.getStylesheets().add("/styles/Styles.css");
        stage.setTitle("Email System");
        stage.setScene(logInScene);
        stage.show();
    }
    
    
    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("Error!");
        dialog.setHeaderText("Error!");
        dialog.setContentText(msg);
        dialog.show();
    }
    
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}

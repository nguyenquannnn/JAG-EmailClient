/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.presentation;

import com.quannguyen.jagemailclient.configuration.controller.ConfigurationController;
import com.quannguyen.jagemailclient.configuration.data.ConfigurationBean;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * FXML Controller class
 *
 * @author 1632647
 */
public class ConfigurationFXController implements Initializable {

    private ConfigurationController controller;
    ConfigurationBean configBean;
    private static final Logger LOG = LoggerFactory.getLogger(MessageComposerController.class);
    @FXML
    private Button buttonReset;
    @FXML
    private Button buttonSave;
    @FXML
    private TextField textfieldUsername;
    @FXML
    private TextField textfieldEmailAddress;
    @FXML
    private TextField textfieldPassword;
    @FXML
    private TextField textfieldIMAPServer;
    @FXML
    private TextField textfieldSMTPServer;
    @FXML
    private TextField textfieldIMAPPort;
    @FXML
    private TextField textfieldSMTPPort;
    @FXML
    private TextField textfieldDbName;
    @FXML
    private TextField textfieldDbPort;
    @FXML
    private TextField textfieldDbUsername;
    @FXML
    private TextField textfieldDbPassword;
    @FXML
    private TextField textfieldDbUrl;
    @FXML
    private Button buttonDelete;

    public ConfigurationFXController() {
        super();
    }

    public ConfigurationFXController(String emailAddress, String password) {
        super();
        setAccountInfo(emailAddress, password);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.buttonReset.setOnAction(this::buttonResetHandler);
        this.buttonSave.setOnAction(this::buttonSaveHandler);
        this.buttonDelete.setOnAction(this::buttonDeleteHandler);
    }

    public void buttonSaveHandler(ActionEvent e) {
        try {
            controller.saveConfiguration();
            Platform.exit();
            System.exit(0);
        } catch (IOException ex) {
            LOG.error(null, ex);
            errorAlert("buttonSaveHandler()");
        }
    }

    public void buttonDeleteHandler(ActionEvent e) {
        try {
            controller.deleteConfiguration();
            Platform.exit();
            System.exit(0);
        } catch (IOException ex) {
            LOG.error(null, ex);
            errorAlert("buttonSaveHandler()");
        }

    }

    public void buttonResetHandler(ActionEvent e) {
        try {
            //Phase4
            controller.reloadConfigBean(controller.getConfigBean().getUser().getEmailAddress(),
                    controller.getConfigBean().getPassword());
//            this.configBean = this.controller.getConfigBean();
            bindProperties();
        } catch (IOException ex) {
            LOG.error(null, ex);
            errorAlert("buttonSaveHandler()");
        }
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("Error!");
        dialog.setHeaderText("Error!");
        dialog.setContentText(msg);
        dialog.show();
    }

    public void setAccountInfo(String emailAddress, String password) {
        try {
            this.controller = new ConfigurationController(emailAddress, password);
//            configBean = this.controller.getConfigBean();
        } catch (IOException ioe) {
            LOG.error(null, ioe);
            errorAlert("ConfigurationFXController()");
            Platform.exit();
        }
    }

    public void setConfigBean(ConfigurationBean configBean) {
        this.configBean = configBean;
    }

    public ConfigurationController getController() {
        return controller;
    }

    public void bindProperties() {
        this.configBean = controller.getConfigBean();
        textfieldUsername.textProperty().bindBidirectional(configBean.getUser().personnalName());
        textfieldEmailAddress.textProperty().bindBidirectional(configBean.getUser().emailAddress());
        textfieldPassword.textProperty().bind(configBean.password());
        textfieldIMAPServer.textProperty().bindBidirectional(configBean.imapServer());
        textfieldIMAPPort.textProperty().bindBidirectional(configBean.imapPort(),
                new NumberStringConverter());

        textfieldSMTPServer.textProperty().bindBidirectional(configBean.smtpServer());
        textfieldSMTPPort.textProperty().bindBidirectional(configBean.smtpPort(),
                new NumberStringConverter());

        textfieldDbName.textProperty().bind(configBean.dbName());
        textfieldDbPort.textProperty().bindBidirectional(configBean.dbPort(), new NumberStringConverter());
        textfieldDbUsername.textProperty().bind(configBean.dbUsername());
        textfieldDbPassword.textProperty().bind(configBean.dbPassword());
        textfieldDbUrl.textProperty().bind(configBean.dbUrl());
    }
}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.presentation;

import com.quannguyen.jagemailclient.db.controller.DbConnectionHandler;
import com.quannguyen.jagemailclient.db.controller.DbConnectionHandlerImpl;
import com.quannguyen.jagemailclient.email.controller.EmailController;
import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import com.quannguyen.jagemailclient.email.data.EmailBean;
import com.quannguyen.jagemailclient.email.data.EmailType;
import com.quannguyen.jagemailclient.email.persistence.EmailDAO;
import com.quannguyen.jagemailclient.folder.data.FolderBean;
import com.quannguyen.jagemailclient.folder.persistence.FolderDAO;
import com.quannguyen.jagemailclient.presentation.thread.RefreshingInboxThread;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FXML Controller class
 *
 * @author Quan-Nguyen
 */
public class MainController {

    private static final Logger LOG = LoggerFactory.getLogger(LogInController.class);
    private DbConnectionHandler dbConnection;
    private EmailDAO emailDAO;
    private FolderDAO folderDAO;
    private ConfigurationFXController configController;
    private EmailController emailController;

    private List<EmailBean> emails;
    private List<FolderBean> folders;
    private FolderBean currentFolder;
    private EmailBean currentEmail;

    private String emailAddress;
    private String password;
    @FXML
    private Label labelUsername;
    @FXML
    private Button buttonCompose;
    @FXML
    private Label labelEmailAddress;
    @FXML
    private Button buttonAddFolder;
    @FXML
    private Button buttonDeleteFolder;
    @FXML
    private ListView<FolderBean> listviewFolder;
    @FXML
    private Button buttonQuit;
    @FXML
    private Button buttonConfig;
    @FXML
    private TableView<EmailBean> tableEmails;
    @FXML
    private TableColumn<EmailBean, Number> columnPriority;
    @FXML
    private TableColumn<EmailBean, String> columnFrom;
    @FXML
    private TableColumn<EmailBean, String> columnSubject;
    @FXML
    private TableColumn<EmailBean, String> columnDate;
    @FXML
    private Button buttonReply;
    @FXML
    private Button buttonReplyAll;
    @FXML
    private Button buttonForward;
    @FXML
    private Button buttonDelete;
    @FXML
    private Button buttonAbout;
    @FXML
    private Button buttonRefresh;
    @FXML
    private Button buttonMove;

    public MainController() {
        super();
    }

    public MainController(String emailAddress, String password) {
        super();
        this.emailAddress = emailAddress;
        this.password = password;
    }

    /**
     * Initializes the controller class.
     */
    public void initialize() {
        initMainLayout();
    }

    private void initMainLayout() {
        listviewFolder.setCellFactory(cell -> new ListCell<FolderBean>() {
            @Override
            protected void updateItem(FolderBean item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty) {
                    setText(item.getFolderName());
                    File image = null;
                    switch (item.getFolderName().toUpperCase()) {
                        case "INBOX":
                            image = new File("./src/main/resources/images/folderInbox.png");
                            break;
                        case "SENT":
                            image = new File("./src/main/resources/images/folderSent.png");
                            break;
                        default:
                            image = new File("./src/main/resources/images/folder.png");
                            break;
                    }
                    setGraphic(new ImageView(image.toURI().toString()));
                } else {
                    setText(null);
                    setGraphic(null);
                }

            }
        });
        listviewFolder.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal) -> {
            currentFolder = newVal;
            emails = loadEmails(currentFolder);
            displayEmailTable(emails);
        });
        tableEmails.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            currentEmail = newValue;
        });
        bindColumns();

        buttonCompose.setOnAction(this::buttonComposeHandler);
        buttonConfig.setOnAction(this::buttonConfigHandler);
        buttonQuit.setOnAction(this::buttonQuitHandler);
        buttonAddFolder.setOnAction(this::buttonAddFolderHandler);
        buttonReply.setOnAction(this::buttonReplyHandler);
        buttonReplyAll.setOnAction(this::buttonReplyAllHandler);
        buttonForward.setOnAction(this::buttonForwardHandler);
        buttonDelete.setOnAction(this::buttonDeleteHandler);
        buttonDeleteFolder.setOnAction(this::buttonDeleteFolderHandler);
        buttonRefresh.setOnAction(this::buttonRefreshHandler);
        buttonAbout.setOnAction(this::buttonAboutHandler);
        buttonMove.setOnAction(this::buttonMoveHandler);
    }

    private List<EmailBean> loadEmails(FolderBean folder) {
        try {
            return emailDAO.getEmailsFromFolder(folder);
        } catch (SQLException sqle) {
            LOG.error(null, sqle);
            errorAlert("loadEmails()");
            return null;
        }
    }

    private List<FolderBean> loadFolders() {
        try {
            return folderDAO.getAllFolder();
        } catch (SQLException ex) {
            LOG.error(null, ex);
            errorAlert("displayTree()");
            return null;
        }
    }

    private void bindColumns() {
        columnPriority.setCellValueFactory(cellData -> cellData.getValue().priority());
        columnFrom.setCellValueFactory(cellData -> cellData.getValue().getFrom().emailAddress());
        columnSubject.setCellValueFactory(cellData -> cellData.getValue().subject());
        columnDate.setCellValueFactory(cellData -> cellData.getValue().sendDateTime().asString());
    }

    public void displayEmailTable(List<EmailBean> list) {
        ObservableList<EmailBean> emails = FXCollections.observableArrayList(list);

        tableEmails.setRowFactory(tv -> {
            TableRow<EmailBean> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    EmailBean rowData = row.getItem();
                    initMessageDislayLayout(rowData);
                }
            });
            return row;
        });
        tableEmails.setItems(emails);
    }

    private void initMessageDislayLayout(EmailBean bean) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/MessageDisplay.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();
            MessageDisplayController displayController = loader.getController();
            displayController.setConfigController(configController);
            displayController.setEmailController(emailController);
            displayController.setEmailDAO(emailDAO);
            displayController.setEmailView(bean);

            Scene scene = new Scene(rootLayout);
            Stage popup = new Stage();
            popup.setScene(scene);
            popup.setTitle(bean.getSubject());
            popup.showAndWait();
        } catch (IOException ioe) {
            LOG.error(null, ioe);
            errorAlert("initComposeLayout()");
            Platform.exit();
        }
    }

    private EmailBean createMockEmail() {
        EmailAddressBean from = new EmailAddressBean("send.1632647@gmail.com", "Send Test");
        EmailAddressBean to = new EmailAddressBean("receive.1632647@gmail.com", "Receive Test");
        EmailAddressBean cc = new EmailAddressBean("other.1632647@gmail.com", "Other TestS");
        EmailBean bean = new EmailBean();
        bean.setFrom(from);
        bean.setTo(Arrays.asList(new EmailAddressBean[]{to}));
        bean.getCc().add(cc);
        bean.setSubject("Hello this is Main!");
        return bean;
    }

    private void displayTree(List<FolderBean> folderBeans) {
        //Phase 4: 
        ObservableList<FolderBean> folders = FXCollections.observableArrayList(folderBeans);
        this.listviewFolder.setItems(folders);
//        if (folders != null) {
//            this.listviewFolder.setItems(folders);
//            this.listviewFolder.getSelectionModel().select(0);
//        }
    }

    public void buttonConfigHandler(ActionEvent event) {
        initConfigLayout();
    }

    public void buttonComposeHandler(ActionEvent event) {
        initComposeLayout();
    }

    public void buttonReplyHandler(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/MessageComposer.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();
            MessageComposerController composerController = loader.getController();
            composerController.setEmailController(emailController);
            composerController.setEmailDAO(emailDAO);
            composerController.setFrom(configController.getController().getConfigBean().getUser());
            composerController.responseToEmail(currentEmail, EmailType.REPLY);

            Scene scene = new Scene(rootLayout);
            Stage popup = new Stage();
            popup.setScene(scene);
            popup.setTitle("Reply to an email.");
            popup.showAndWait();
        } catch (IOException ioe) {
            LOG.error(null, ioe);
            errorAlert("initComposeLayout()");
            Platform.exit();
        }

    }

    public void buttonReplyAllHandler(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/MessageComposer.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();
            MessageComposerController composerController = loader.getController();
            composerController.setEmailController(emailController);
            composerController.setEmailDAO(emailDAO);
            composerController.setFrom(configController.getController().getConfigBean().getUser());
            composerController.responseToAllEmail(currentEmail, EmailType.REPLY);

            Scene scene = new Scene(rootLayout);
            Stage popup = new Stage();
            popup.setScene(scene);
            popup.setTitle("Reply to all of an email.");
            popup.showAndWait();
        } catch (IOException ioe) {
            LOG.error(null, ioe);
            errorAlert("initComposeLayout()");
            Platform.exit();
        }

    }

    public void buttonForwardHandler(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/MessageComposer.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();
            MessageComposerController composerController = loader.getController();
            composerController.setEmailController(emailController);
            composerController.setEmailDAO(emailDAO);
            composerController.setFrom(configController.getController().getConfigBean().getUser());
            composerController.forwardEmail(currentEmail, EmailType.FORWARD);

            Scene scene = new Scene(rootLayout);
            Stage popup = new Stage();
            popup.setScene(scene);
            popup.setTitle("Forward an email.");
            popup.showAndWait();
        } catch (IOException ioe) {
            LOG.error(null, ioe);
            errorAlert("initComposeLayout()");
            Platform.exit();
        }

    }

    public void buttonDeleteHandler(ActionEvent event) {
        try {
            //call to DAO
            emailDAO.deleteEmail(currentEmail.getId());
            emails.remove(currentEmail);
            displayEmailTable(emails);
        } catch (SQLException ex) {
            LOG.error(null, ex);
            errorAlert("Error deleting email!");
        }
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Email deleted!");
        dialog.setHeaderText("The selected email has been deleted!");
        dialog.setContentText("");
        dialog.show();
    }

    public void buttonMoveHandler(ActionEvent e) {
        Map<String, FolderBean> choices = new HashMap<>();
        for (FolderBean folder : this.folders) {
            choices.put(folder.getFolderName(), folder);
        }
        Set<String> keySet = choices.keySet();
        ChoiceDialog<String> dialog = new ChoiceDialog<>(currentFolder.getFolderName(), keySet);
        dialog.setTitle("Move email.");
        dialog.setHeaderText("Moving the selected email!");
        dialog.setContentText("Choose the destination folder!");
        Optional<String> result = dialog.showAndWait();

        result.ifPresent(choice -> {
            try {
                emailDAO.changeFolder(currentEmail, choices.get(choice).getId());

                if (currentFolder.getId() != choices.get(choice).getId()) {
                    emails.remove(currentEmail);
                    displayEmailTable(emails);
                }
            } catch (SQLException ex) {
                LOG.error(null, ex);
                errorAlert("Error moving email!");
            }
        });
    }

    private void buttonDeleteFolderHandler(ActionEvent event) {
        if (this.currentFolder == null) {
            errorAlert("Please select a folder!");
        } else {
            switch (this.currentFolder.getFolderName().toUpperCase()) {
                case "INBOX":
                case "SENT":
                    errorAlert("Error! Cannot delete a default folder.");
                    break;
                default: {
                    try {
                        folderDAO.deleteFolder(currentFolder.getId());
                        this.folders = loadFolders();
                        displayTree(folders);
                        successAlert("Successfully deleted folder!");
                    } catch (SQLException ex) {
                        LOG.error(null, ex);
                        errorAlert("Error while deleting folder!");
                    }
                }
            }
        }
    }

    public void buttonAddFolderHandler(ActionEvent event) {
        Dialog dialog = new TextInputDialog();
        dialog.setTitle("Add new Folder");
        dialog.setHeaderText("Enter the new folder's name:");

        Optional<String> result = dialog.showAndWait();
        String entered = "";

        try {
            if (result.isPresent()) {
                entered = result.get().trim();
            }
            if (entered.isEmpty()) {
                throw new IllegalArgumentException("buttonAddFolderHandler(): Invalid folder name");
            }
            FolderBean bean = new FolderBean(entered);
            folderDAO.createFolder(bean);
            this.folders = loadFolders();
            displayTree(folders);
        } catch (SQLException ex) {
            LOG.error(null, ex);
            errorAlert("Error creating folder!");
        } catch (IllegalArgumentException iae) {
            LOG.error(null, iae);
            errorAlert("Invalid folder name!");
        }
        dialog.close();
    }

    public void buttonRefreshHandler(ActionEvent event) {
//        List<EmailBean> unreadNewEmail = emailController.receiveEmail();
//        unreadNewEmail.stream().forEach((email) -> {
//            try {
//                emailDAO.createEmail(email);
//            } catch (SQLException ex) {
//                LOG.error(null, ex);
//                errorAlert("Error refreshing!");
//            }
//        });
//        try {
//            this.emails = emailDAO.getEmailsFromFolder(currentFolder);
//        } catch (SQLException ex) {
//            LOG.error(null, ex);
//            errorAlert("Error loading emails!");
//        }
//        displayEmailTable(emails);
//        successAlert("Refreshed!");
        RefreshingInboxThread thread = new RefreshingInboxThread(emailDAO, emailController, this);
        Platform.runLater(thread);
    }

    public void buttonAboutHandler(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/About.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();

            Scene scene = new Scene(rootLayout);
            Stage popup = new Stage();
            popup.setScene(scene);
            popup.setTitle("About.");
            popup.showAndWait();
        } catch (IOException ioe) {
            LOG.error(null, ioe);
            errorAlert("buttonAboutHandler()");
            Platform.exit();
        }
    }

    private void initConfigLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/Configuration.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();
            configController = loader.getController();
            configController.setAccountInfo(emailAddress, password);
            configController.bindProperties();

            Scene scene = new Scene(rootLayout);
            Stage popup = new Stage();
            popup.setScene(scene);
            popup.setTitle("Configuration.");
            popup.showAndWait();
        } catch (IOException ioe) {
            LOG.error(null, ioe);
            errorAlert("initConfigLayout()");
            Platform.exit();
        }
    }

    private void initComposeLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/MessageComposer.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();
            MessageComposerController composeController = loader.getController();
            composeController.setEmailController(emailController);
            composeController.setEmailDAO(emailDAO);
            composeController.setFrom(configController.getController().getConfigBean().getUser());

            Scene scene = new Scene(rootLayout);
            Stage popup = new Stage();
            popup.setScene(scene);
            popup.setTitle("Compose an email.");
            popup.showAndWait();

            if (currentFolder.getId() == 2) {
                emails = emailDAO.getEmailsFromFolder(currentFolder);
                displayEmailTable(emails);
            }
        } catch (IOException ioe) {
            LOG.error(null, ioe);
            errorAlert("initComposeLayout()");
            Platform.exit();
        } catch (SQLException e) {
            LOG.error(null, e);
            errorAlert("Error loading emails!");
            Platform.exit();
        }
    }

    public void buttonQuitHandler(ActionEvent event) {
        Platform.exit();
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(final String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("Error!");
        dialog.setHeaderText("Error!");
        dialog.setContentText(msg);
        dialog.showAndWait();
    }

    private void successAlert(final String msg) {
        Alert dialog = new Alert(Alert.AlertType.CONFIRMATION);
        dialog.setTitle("Success!");
        dialog.setHeaderText("Success!");
        dialog.setContentText(msg);
        dialog.show();
    }

    public void setAccountInfo(final String emailAddress, final String password) {
        this.emailAddress = emailAddress;
        this.password = password;
    }

    public void createConfigController() {
        this.configController = new ConfigurationFXController(this.emailAddress, this.password);
        this.configController.setConfigBean(this.configController.getController().getConfigBean());
    }

    public void createDbConnection() {
        String dbUrl = configController.getController().getConfigBean().getDbUrl();
        String dbUsername = configController.getController().getConfigBean().getDbUsername();
        String dbPassword = configController.getController().getConfigBean().getDbPassword();
        this.dbConnection = new DbConnectionHandlerImpl(dbUrl, dbUsername, dbPassword);
    }

    public DbConnectionHandler getDbConnectionHandler() {
        return this.dbConnection;
    }

    public void setFolderDAO(final FolderDAO folderDAO) {
        this.folderDAO = folderDAO;
    }

    public void setEmailDAO(final EmailDAO emailDAO) {
        this.emailDAO = emailDAO;
    }

    public void setEmailController(final EmailController emailController) {
        this.emailController = emailController;
    }

    public void loadContent() {
        this.folders = loadFolders();
        //Inbox
        this.currentFolder = folders.get(0);
        displayTree(folders);
        listviewFolder.getSelectionModel().select(currentFolder);
        if (this.currentFolder != null) {
            this.emails = loadEmails(currentFolder);
            displayEmailTable(emails);
        }
        buttonDeleteFolder.disableProperty().bind(listviewFolder.getSelectionModel().selectedItemProperty().isNull());

        buttonReply.disableProperty().bind(tableEmails.getSelectionModel().selectedItemProperty().isNull());
        buttonReplyAll.disableProperty().bind(tableEmails.getSelectionModel().selectedItemProperty().isNull());
        buttonForward.disableProperty().bind(tableEmails.getSelectionModel().selectedItemProperty().isNull());
        buttonDelete.disableProperty().bind(tableEmails.getSelectionModel().selectedItemProperty().isNull());
        buttonMove.disableProperty().bind(tableEmails.getSelectionModel().selectedItemProperty().isNull());

        StringProperty username = configController.getController().getConfigBean().getUser().personnalName();
        if (username != null && username.get() != null && !username.get().isEmpty()) {
            labelUsername.textProperty().bind(username);
        } else {
            labelUsername.textProperty().set("Welcome, user!");
        }
        labelEmailAddress.textProperty().bind(configController.getController().getConfigBean().getUser().emailAddress());
    }
}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.configuration.controller;

import com.quannguyen.jagemailclient.configuration.data.ConfigurationBean;
import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Quan-Nguyen
 */
public class ConfigurationController {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationController.class);
    private Properties prop;
    private ConfigurationBean configBean;
    private Path defaultPath;

    public ConfigurationController() throws IOException {
        this("nguyenquan233@gmail.com", "hello");
    }

    public ConfigurationController(String emailAddress, String password) throws IOException {
        this.defaultPath = get("./", "user.ini");
        reloadConfigBean(emailAddress, password);
    }

    public void updateConfiguration(ConfigurationBean configBean) {
        prop.setProperty("Username", configBean.getUser().getPersonnalName());
        prop.setProperty("EmailAddress", configBean.getUser().getEmailAddress());
        prop.setProperty("Password", configBean.getPassword());
        prop.setProperty("IMAPServer", configBean.getImapServer());
        prop.setProperty("IMAPPort", configBean.getImapPort() + "");
        prop.setProperty("SMTPServer", configBean.getSmtpServer());
        prop.setProperty("SMTPPort", configBean.getSmtpPort() + "");
        prop.setProperty("DbName", configBean.getDbName());
        prop.setProperty("DbUsername", configBean.getDbUsername());
        prop.setProperty("DbPassword", configBean.getDbPassword());
        prop.setProperty("DbUrl", configBean.getDbUrl());
    }

    private ConfigurationBean createConfigBean() {
        ConfigurationBean temp = new ConfigurationBean();
        if (this.prop.containsKey("Username") && this.prop.containsKey("EmailAddress")) {
            temp.setUser(new EmailAddressBean(prop.getProperty("EmailAddress", null),
                    prop.getProperty("Username", null)));
        }
        if (this.prop.containsKey("Password")) {
            temp.setPassword(this.prop.getProperty("Password"));
        }
        if (this.prop.containsKey("IMAPServer")) {
            temp.setImapServer(this.prop.getProperty("IMAPServer"));
        }
        if (this.prop.containsKey("IMAPPort")) {
            temp.setImapPort(Integer.parseInt(this.prop.getProperty("IMAPPort")));
        }
        if (this.prop.containsKey("SMTPServer")) {
            temp.setSmtpServer(this.prop.getProperty("SMTPServer"));
        }
        if (this.prop.containsKey("SMTPPort")) {
            temp.setSmtpPort(Integer.parseInt(this.prop.getProperty("SMTPPort")));
        }
        if (this.prop.containsKey("DbName")) {
            temp.setDbName(this.prop.getProperty("DbName"));
        }
        if (this.prop.containsKey("DbUsername")) {
            temp.setDbUsername(this.prop.getProperty("DbUsername"));
        }
        if (this.prop.containsKey("DbPassword")) {
            temp.setDbPassword(this.prop.getProperty("DbPassword"));
        }
        if (this.prop.containsKey("DbUrl")) {
            temp.setDbUrl(this.prop.getProperty("DbUrl"));
        }
        return temp;
    }

    public void createConfiguration(ConfigurationBean configBean) {
        if (prop == null) {
            this.prop = new Properties();
        }
        updateConfiguration(configBean);
    }

    public void saveConfiguration() throws IOException {
        if (defaultPath != null && prop != null) {
            try (OutputStream os = new FileOutputStream(this.defaultPath.toFile())) {
                updateConfiguration(configBean);
                prop.store(os, "Email Systems");
                updateConfigBean();
                LOG.info("saveConfiguration(): Stored config.");
            }
        } else {
            throw new IllegalArgumentException("Configuration Controller saveConfiguration(): null fields.");
        }
    }

    public boolean loadConfiguration() throws IOException {
        if (defaultPath != null) {
            if (Files.exists(defaultPath)) {
                try (InputStream propFileStream = new FileInputStream(this.defaultPath.toFile());) {
                    this.prop = new Properties();
                    prop.load(propFileStream);
                    LOG.info("loadConfiguration(): loaded config.");
                    return true;
                }
            }
        }
        return false;
    }

    public ConfigurationBean getConfigBean() {
        return configBean;
    }

    public void reloadConfigBean(String emailAddress, String password) throws IOException {
        if (loadConfiguration()) {
            this.configBean = createConfigBean();
        } else {
            this.configBean = new ConfigurationBean();
            configBean.setUser(new EmailAddressBean(emailAddress, ""));
            configBean.setPassword(password);
            createConfiguration(this.configBean);
        }
    }

    public void updateConfigBean() throws IOException {
        if (loadConfiguration()) {
            ConfigurationBean temp = this.configBean;
            if (this.prop.containsKey("Username") && this.prop.containsKey("EmailAddress")) {
                temp.setUser(new EmailAddressBean(prop.getProperty("EmailAddress", null),
                        prop.getProperty("Username", null)));
            }
            if (this.prop.containsKey("Password")) {
                temp.setPassword(this.prop.getProperty("Password"));
            }
            if (this.prop.containsKey("IMAPServer")) {
                temp.setImapServer(this.prop.getProperty("IMAPServer"));
            }
            if (this.prop.containsKey("IMAPPort")) {
                temp.setImapPort(Integer.parseInt(this.prop.getProperty("IMAPPort")));
            }
            if (this.prop.containsKey("SMTPServer")) {
                temp.setSmtpServer(this.prop.getProperty("SMTPServer"));
            }
            if (this.prop.containsKey("SMTPPort")) {
                temp.setSmtpPort(Integer.parseInt(this.prop.getProperty("SMTPPort")));
            }
            if (this.prop.containsKey("DbName")) {
                temp.setDbName(this.prop.getProperty("DbName"));
            }
            if (this.prop.containsKey("DbUsername")) {
                temp.setDbUsername(this.prop.getProperty("DbUsername"));
            }
            if (this.prop.containsKey("DbPassword")) {
                temp.setDbPassword(this.prop.getProperty("DbPassword"));
            }
            if (this.prop.containsKey("DbUrl")) {
                temp.setDbUrl(this.prop.getProperty("DbUrl"));
            }
        }
    }

    public void deleteConfiguration() throws IOException {
        if (defaultPath != null) {
            if (Files.exists(defaultPath)) {
                Files.delete(defaultPath);

            }
        }
    }
}

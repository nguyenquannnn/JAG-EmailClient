/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.configuration.data;

import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import java.io.Serializable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Quan-Nguyen
 */
public class ConfigurationBean implements Serializable {
        private ObjectProperty<EmailAddressBean> user;
        private StringProperty password;
        private StringProperty imapServer;
        private StringProperty smtpServer;
        private IntegerProperty smtpPort;
        private IntegerProperty imapPort;
        private StringProperty dbUrl;
        private StringProperty dbName;
        private IntegerProperty dbPort;
        private StringProperty dbUsername;
        private StringProperty dbPassword;
        
        public ConfigurationBean() {
            user = new SimpleObjectProperty<>();
            password = new SimpleStringProperty();
            imapServer = new SimpleStringProperty();
            smtpServer = new SimpleStringProperty();
            smtpPort = new SimpleIntegerProperty();
            imapPort = new SimpleIntegerProperty();
            dbUrl = new SimpleStringProperty();
            dbPort = new SimpleIntegerProperty();
            dbName = new SimpleStringProperty();
            dbUsername = new SimpleStringProperty();
            dbPassword = new SimpleStringProperty();
            
            imapServer.set(ConfigurationConstant.IMAP_SERVER_URL);
            imapPort.set(ConfigurationConstant.IMAP_SERVER_PORT);
            smtpServer.set(ConfigurationConstant.SMTP_SERVER_URL);
            smtpPort.set(ConfigurationConstant.SMTP_SERVER_PORT);
            dbName.set(ConfigurationConstant.DB_NAME);
            dbPort.set(ConfigurationConstant.DB_PORT);
            dbUrl.set(ConfigurationConstant.DB_URL);
            dbUsername.set(ConfigurationConstant.DB_USERNAME);
            dbPassword.setValue(ConfigurationConstant.DB_PASSWORD);
        }

    public ObjectProperty<EmailAddressBean> user() {
        return user;
    }

    public StringProperty password() {
        return password;
    }

    public StringProperty imapServer() {
        return imapServer;
    }

    public StringProperty smtpServer() {
        return smtpServer;
    }

    public IntegerProperty smtpPort() {
        return smtpPort;
    }

    public IntegerProperty imapPort() {
        return imapPort;
    }

    public StringProperty dbUrl() {
        return dbUrl;
    }

    public StringProperty dbName() {
        return dbName;
    }

    public IntegerProperty dbPort() {
        return dbPort;
    }

    public StringProperty dbUsername() {
        return dbUsername;
    }

    public StringProperty dbPassword() {
        return dbPassword;
    }

    public EmailAddressBean getUser() {
        return user.get();
    }

    public void setUser(EmailAddressBean user) {
        this.user.set(user);
    }

    public String getPassword() {
        return password.get();
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public String getImapServer() {
        return imapServer.get();
    }

    public void setImapServer(String imapServer) {
        this.imapServer.set(imapServer);
    }

    public String getSmtpServer() {
        return smtpServer.get();
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer.set(smtpServer);
    }

    public Integer getSmtpPort() {
        return smtpPort.get();
    }

    public void setSmtpPort(Integer smtpPort) {
        this.smtpPort.set(smtpPort);
    }

    public Integer getImapPort() {
        return imapPort.get();
    }

    public void setImapPort(Integer imapPort) {
        this.imapPort.set(imapPort);
    }

    public String getDbUrl() {
        return dbUrl.get();
    }

    public void setDbUrl(String dbUrl) {
        this.dbUrl.set(dbUrl);
    }

    public String getDbName() {
        return dbName.get();
    }

    public void setDbName(String dbName) {
        this.dbName.set(dbName);
    }

    public Integer getDbPort() {
        return dbPort.get();
    }

    public void setDbPort(Integer dbPort) {
        this.dbPort.set(dbPort);
    }

    public String getDbUsername() {
        return dbUsername.get();
    }

    public void setDbUsername(String dbUsername) {
        this.dbUsername.set(dbUsername);
    }

    public String getDbPassword() {
        return dbPassword.get();
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword.set(dbPassword);
    }
}

/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.db.controller;

import com.quannguyen.jagemailclient.email.data.EmailBean;
import com.quannguyen.jagemailclient.email.persistence.EmailAddressDAO;
import com.quannguyen.jagemailclient.email.persistence.EmailAddressDAOImpl;
import com.quannguyen.jagemailclient.email.persistence.EmailAttachmentDAO;
import com.quannguyen.jagemailclient.email.persistence.EmailAttachmentDAOImpl;
import com.quannguyen.jagemailclient.email.persistence.EmailDAO;
import com.quannguyen.jagemailclient.email.persistence.EmailDAOImpl;
import com.quannguyen.jagemailclient.folder.data.FolderBean;
import com.quannguyen.jagemailclient.folder.persistence.FolderDAOImpl;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Class that implement DBController interface, which abstract all business logic 
 * that interacts with Database layer.
 * 
 * @author Anh Quan Nguyen
 */
public class DbControllerImpl implements DbController {
    private final DbConnectionHandler handler;
    private final EmailAddressDAO addressDAO;
    private final EmailAttachmentDAO attachmentDAO;
    private final EmailDAO emailDAO;
    private final FolderDAOImpl folderDAO;
    
    /**
     * Initializes the DAO.
     * 
     * @param handler a Connection handler for database 
     */
    public DbControllerImpl(DbConnectionHandler handler) {
        this.handler = handler;
        this.addressDAO = new EmailAddressDAOImpl(handler);
        this.attachmentDAO = new EmailAttachmentDAOImpl(handler);
        this.emailDAO = new EmailDAOImpl(handler);
        this.folderDAO = new FolderDAOImpl(handler);
    }
    
    /**
     * Store a list of Emails to database
     * 
     * @param emailBeans Emails to store;
     * @throws SQLException 
     */
    @Override
    public void storeEmails(List<EmailBean> emailBeans) throws SQLException {
       for(EmailBean email : emailBeans) {
           emailDAO.createEmail(email);
       }
    }
    
    /**
     * Retrieve a list of emails within a specified folder.
     * Empty list if there is no email.
     * 
     * @param folder Folder name
     * @return List of emails that reside with specified folder
     * @throws SQLException 
     */
    @Override
    public List<EmailBean> retrieveAllEmailFromFolder(String folder) throws SQLException {
        FolderBean folderBean = folderDAO.getAllFolder()
                .stream()
                .filter(f -> f.getFolderName().equalsIgnoreCase(folder))
                .findFirst().get();
        return emailDAO.getEmailsFromFolder(folderBean);
    }
    
    /**
     * Retrieve all folders within the database 
     * 
     * @return List of folders
     * @throws SQLException 
     */
    @Override
    public List<FolderBean> retrieveAllFolders() throws SQLException {
       return folderDAO.getAllFolder();
    }
    
    /**
     * Create a new folder record within the database
     * 
     * @param bean Folder to be created
     * @throws SQLException 
     */
    @Override
    public void addFolder(FolderBean bean) throws SQLException {
        folderDAO.createFolder(bean);
    }

    /**
     * Delete a specified folder record
     * 
     * @param folder Name of folder to be deleted
     * @throws SQLException 
     */
    @Override
    public void deleteFolder(String folder) throws SQLException {
        FolderBean folderBean = folderDAO.getAllFolder()
                .stream()
                .filter(f -> f.getFolderName().equalsIgnoreCase(folder))
                .findFirst().get(); 
       folderDAO.deleteFolder(folderBean.getId());
    }
    
    /**
     * Update an existing email record
     * 
     * @param bean Email to be updated
     * @throws SQLException 
     */
    @Override
    public void updateEmail(EmailBean bean) throws SQLException {
        emailDAO.updateEmail(bean);
    }    

    @Override
    public void createDatabase() throws SQLException {    
        final String seedDataScript = loadAsString("./src/main/resources/scripts/EmailSystemSchemaFirstTime.sql");
        try (Connection connection2 = handler.getConnection()) {
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection2.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
        
    }
    
     /**
     * The following methods support the seedDatabase method
     * @author: Ken Fogel
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = new FileInputStream(new File(path));
                Scanner scanner = new Scanner(inputStream);) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
}

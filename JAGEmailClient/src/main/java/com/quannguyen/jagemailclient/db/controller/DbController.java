/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.db.controller;

import com.quannguyen.jagemailclient.email.data.EmailBean;
import com.quannguyen.jagemailclient.folder.data.FolderBean;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface for the controller that handle all interactions with the database.
 * 
 * @author Anh Quan Nguyen
 */
public interface DbController {
    
    void createDatabase() throws SQLException;
    /**
     * Store a list of Emails to database
     * 
     * @param emailBeans Emails to store;
     * @throws SQLException 
     */
    void storeEmails(List<EmailBean> emailBeans) throws SQLException;
    /**
     * Retrieve a list of emails within a specified folder.
     * Empty list if there is no email.
     * 
     * @param folder Folder name
     * @return List of emails that reside with specified folder
     * @throws SQLException 
     */
    List<EmailBean> retrieveAllEmailFromFolder(String folder) throws SQLException;
    /**
     * Retrieve all folders within the database 
     * 
     * @return List of folders
     * @throws SQLException 
     */
    List<FolderBean> retrieveAllFolders() throws SQLException;
    /**
     * Create a new folder record within the database
     * 
     * @param bean Folder to be created
     * @throws SQLException 
     */
    void addFolder(FolderBean bean) throws SQLException;
    /**
     * Delete a specified folder record
     * 
     * @param folder Name of folder to be deleted
     * @throws SQLException 
     */
    void deleteFolder(String folder) throws SQLException;
    /**
     * Update an existing email record
     * 
     * @param bean Email to be updated
     * @throws SQLException 
     */
    void updateEmail(EmailBean bean) throws SQLException;
}

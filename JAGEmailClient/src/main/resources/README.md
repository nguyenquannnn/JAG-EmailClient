<p># JAG-EmailClient</p>
<h3>Author: Anh Quan Nguyen</h3>
<h3>StudentId: 1632647</h3>
<section>
    <strong>PHASE 4 (Final ver 4.0) Instruction:</strong>
    <p>Set up the program first time before the first run:</p>
    <ol>
        <li>Ensure that mySQL is correctly installed</li>
        <li>Start the mySQL service</li>
        <li><ol>Do the folowing to ensure the testes run successful:
                <li>Establish a connection to a database as a root user. 
                    On Dawson machine, the root user of the localhost database is of the username root with the password dawson</li>
                <li>Run the script in Other Sources as the root user: 
                    <strong>src/main/resources/scripts/EmailSystemDbCreate.sql</strong></li>
        </ol></li>
        <li>Establish a connection to a database as a root user. On Dawson machine, the root user of the localhost database is of the username root with the password dawson</li>
        <li>Run the script in Other Sources as the root user (ensure proper main database set up):
            <strong>src/main/resources/scripts/EmailSystemDbFirstTimeLogin.sql</strong></li>
        <li>
                <ol>Start the program by one of the following way: 
                    <li>Press the Green button to run in Netbean (configured to run the default Maven goals)</li>
                    <li>Right click on the project -> Hover run maven -> Choose run_gui</li>
                </ol>
        </li>
    </ol>
    <li><ol>Wait until the Login screen to appears and login with the following credentials:
        <li>Username: send.1632647@gmail.com</li>    
        <li>Password: d5=9u~xo</li>
    </ol>
    </li>
</section>
<section>
<strong>Instruction to the GUI:</strong>
<div>
    <h4>Configuration</h4>
    <p>When you login the program, the config file will be auto generated with the default values.
        This config file is called: <strong>/user.ini</strong> and reside in the root folder.
    </p>
    <p>Example config file:
            #Email Systems<br>
            #Tue Nov 06 14:09:12 EST 2018<br>
            IMAPPort=993<br>
            SMTPServer=smtp.gmail.com<br>
            Username=Send Test<br>
            SMTPPort=465<br>
            IMAPServer=imap.gmail.com<br>
            Password=d5\=9u~xo<br>
            DbName=email_system<br>
            DbUrl=jdbc\:mysql\://localhost\:3306/email_system?autoReconnect\=true&useSSL\=false&allowPublicKeyRetrieval\=true<br>
            DbUsername=TheUser<br>
            DbPassword=tester<br>
            EmailAddress=send.1632647@gmail.com<br> 
    </p>
    <p>To ensure the program run smoothly, the user is restricted to edit only the username, port, server addresses.</p>
    <p>From the main page, click on the gear icon to open Configuration module. The user then can edit the editables fields.
        <ol>
            <li>Click on reset to reset to the previously saved config.</li>
            <li>Click on save and quit to save the config and exit the program. When reload, the new config will be in effect.</li>
            <li>Click on delete and quit to delete the config and exit the program. The user.ini config file will be deleted.</li>
        </ol>
    </p>
</div>
</section>
<section>
        <strong>Known bugs:</strong>
        <ol>These are some of the known bugs I have encountered during the creation of this program
            <li>For Displaying the email with the MessageDisplay module, the GUI the same attachment twice, however in the Database, the data stored is still valid.</li>
            <li>The WebView's javafx control does not support displaying embbedded images, therefor, emails received from gmail will not be able to display the embedded images,
                 and instead will place a rectangle with a cross inside to represent it.</li>
        </ol>
</section>
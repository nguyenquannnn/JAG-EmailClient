USE email_system_test;

DROP TABLE IF EXISTS folder;
DROP TABLE IF EXISTS attachment;
DROP TABLE IF EXISTS address;
DROP TABLE IF EXISTS email;
DROP TABLE IF EXISTS email_to_address;
DROP TABLE IF EXISTS email_cc_address;
DROP TABLE IF EXISTS email_bcc_address;
DROP TABLE IF EXISTS email_attachment;

CREATE TABLE IF NOT EXISTS folder(
    folder_id INT AUTO_INCREMENT PRIMARY KEY,
    folder_name VARCHAR(260) NOT NULL,
    folder_parent INT REFERENCES folder(folder_id)
);

CREATE TABLE IF NOT EXISTS attachment(
	attachment_id INT AUTO_INCREMENT PRIMARY KEY,
	data MEDIUMBLOB,
    attachment_name VARCHAR(260),
    is_embedded BOOLEAN
);

CREATE TABLE IF NOT EXISTS address(
	address_id INT AUTO_INCREMENT ,
    email_address VARCHAR(254) UNIQUE NOT NULL,
    contact_name VARCHAR(254),
    PRIMARY KEY(address_id)
);

CREATE TABLE IF NOT EXISTS email(
    email_id INT AUTO_INCREMENT PRIMARY KEY,
    from_address INT REFERENCES address(address_id) ON DELETE CASCADE,
    subject VARCHAR(998),
    text_message TEXT,
    html_message TEXT,
    priority INT,
    folder_id INT REFERENCES folder(folder_id) ON DELETE CASCADE,
    send_time TIMESTAMP,
    receive_time TIMESTAMP
);

CREATE TABLE IF NOT EXISTS email_to_address(
    email_id INT AUTO_INCREMENT REFERENCES email(email_id) ON DELETE CASCADE,
    to_email_id INT REFERENCES address(address_id) ON DELETE CASCADE,
    PRIMARY KEY(email_id, to_email_id)
);

CREATE TABLE IF NOT EXISTS email_cc_address(
    email_id INT REFERENCES email(email_id) ON DELETE CASCADE,
    cc_email_id INT REFERENCES address(address_id) ON DELETE CASCADE,
    PRIMARY KEY(email_id, cc_email_id)
);

CREATE TABLE IF NOT EXISTS email_bcc_address(
    email_id INT REFERENCES email(email_id) ON DELETE CASCADE,
    bcc_email_id INT REFERENCES address(address_id) ON DELETE CASCADE,
    PRIMARY KEY(email_id, bcc_email_id)
);

CREATE TABLE IF NOT EXISTS email_attachment (
    email_id INT REFERENCES email(email_id) ON DELETE CASCADE,
    attachment_id INT REFERENCES attachment(attachment_id),
    PRIMARY KEY(email_id, attachment_id)
); 

INSERT INTO folder(folder_name) VALUES ('Inbox');
INSERT INTO folder(folder_name) VALUES ('Sent');
CREATE DATABASE email_system;
USE email_system;

CREATE USER TheUser@'localhost' IDENTIFIED BY 'tester';

GRANT ALL ON email_system.* TO TheUser@'localhost';

FLUSH PRIVILEGES;

USE email_system;
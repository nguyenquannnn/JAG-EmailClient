DROP DATABASE IF EXISTS email_system_test;

CREATE DATABASE email_system_test;
USE email_system_test;

DROP USER IF EXISTS TheUser@'localhost';
CREATE USER TheUser@'localhost' IDENTIFIED BY 'tester';

GRANT ALL ON email_system_test.* TO TheUser@'localhost';

FLUSH PRIVILEGES;




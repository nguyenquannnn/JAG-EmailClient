--FOLDER
INSERT INTO folder(folder_name) VALUES ('Inbox');
INSERT INTO folder(folder_name) VALUES ('Sent');
INSERT INTO folder(folder_name) VALUES ('Work');
INSERT INTO folder(folder_name, folder_parent) VALUES ('Important','3');
INSERT INTO folder(folder_name, folder_parent) VALUES ('More Important','4');
--ADDRESS
insert into address (email_address, contact_name) values ('kdunn0@wisc.edu', 'Kimberlyn Dunn');
insert into address (email_address, contact_name) values ('kgutsell1@youtube.com', 'Kaila Gutsell');
insert into address (email_address, contact_name) values ('rlucien2@hao123.com', 'Rutter Lucien');
insert into address (email_address, contact_name) values ('pcolombier3@ifeng.com', 'Philipa Colombier');
insert into address (email_address, contact_name) values ('co4@eventbrite.com', 'Chris O Quirk');
insert into address (email_address, contact_name) values ('tcornhill5@army.mil', 'Trever cornhill');
insert into address (email_address, contact_name) values ('ageertz6@unicef.org', 'Amos Geertz');
insert into address (email_address, contact_name) values ('ihayhurst7@theatlantic.com', 'Inigo Hayhurst');
insert into address (email_address, contact_name) values ('wspurryer8@123-reg.co.uk', 'Willamina Spurryer');
insert into address (email_address, contact_name) values ('aperett9@furl.net', 'Agustin Perett');
--ATTACHMENT
insert into attachment (data, attachment_name, is_embedded) values (0xEBE2AB0, 'amet_sem.gif', false);
insert into attachment (data, attachment_name, is_embedded) values (0xFB29818, 'eros.mpeg', true);
insert into attachment (data, attachment_name, is_embedded) values (0x9319B99, 'turpis_donec_posuere.pdf', false);
insert into attachment (data, attachment_name, is_embedded) values (0x15B6C0C, 'est_lacinia_nisi.gif', true);
insert into attachment (data, attachment_name, is_embedded) values (0x791448F, 'ante.xls', false);
insert into attachment (data, attachment_name, is_embedded) values (0x6679E62, 'posuere_cubilia_curae.png', false);
insert into attachment (data, attachment_name, is_embedded) values (0x9B59F67, 'mi_pede_malesuada.doc', true);
insert into attachment (data, attachment_name, is_embedded) values (0xDCC5FB3, 'in_consequat_ut.ppt', false);
insert into attachment (data, attachment_name, is_embedded) values (0xE60822A, 'ipsum_praesent_blandit.mp3', true);
insert into attachment (data, attachment_name, is_embedded) values (0xD164CC3, 'nunc.png', false);
--EMAIL
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (1, 'toolset', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '<h1>Hello!</h1>', 1, '2018-09-10 21:19:07', '2018-03-05 16:11:00');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (2, 'application', 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.', '<h1>Hello!</h1>', 2, '2017-11-24 22:36:00', '2018-03-30 11:37:31');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (3, 'access', 'Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.', '<h1>Hello!</h1>', 1, '2018-07-01 02:21:11', '2018-04-23 03:23:53');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (4, 'Cross-group', 'In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.', '<h1>Hello!</h1>', 3, '2018-07-19 21:08:41', '2018-08-10 10:36:19');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (5, 'executive', 'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.', '', 1, '2018-02-12 19:23:20', '2018-02-01 01:41:36');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (6, 'alliance', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis.', '<h1>Hello!</h1>', 1, '2018-09-16 18:06:14', '2017-11-09 20:16:15');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (7, 'middleware', 'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', '<h1>Hello!</h1>', 1, '2017-11-20 13:03:35', '2018-08-10 08:21:55');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (8, 'transitional', 'Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.', '<h1>Hello!</h1>', 1, '2018-03-27 14:36:52', '2018-02-02 18:35:04');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (9, 'knowledge user', 'Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.', '<h1>Hello!</h1>', 1, '2018-09-18 23:03:24', '2018-01-29 11:39:45');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (1, 'client-driven', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', '<h1>Hello!</h1>', 1, '2018-06-16 20:05:22', '2018-02-12 07:24:40');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (2, 'function', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.', '<h1>Hello!</h1>', 2, '2018-02-15 13:19:42', '2018-08-03 05:57:40');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (3, 'Inverse', 'Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '', 3, '2018-01-14 11:07:27', '2018-04-03 06:19:07');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (3, 'bi-directional', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum.', '<h1>Hello!</h1>', 3, '2018-04-05 09:40:38', '2018-05-04 20:26:22');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (4, 'info-mediaries', 'Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.', '<h1>Hello!</h1>', 1, '2017-10-30 09:26:24', '2018-08-25 00:59:31');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (5, 'service-desk', 'Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', '<h1>Hello!</h1>', 1, '2018-06-16 03:58:33', '2018-07-09 23:26:49');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (6, 'clear-thinking', 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.', '<h1>Hello!</h1>', 4, '2018-02-06 10:21:33', '2017-11-03 22:23:27');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (7, 'knowledge base', 'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.', '<h1>Hello!</h1>', 4, '2018-08-27 01:16:58', '2018-04-02 12:58:13');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (8, 'fresh-thinking', 'Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '<h1>Hello!</h1>', 3, '2018-05-18 14:38:55', '2018-09-07 11:41:50');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (9, 'Organic', 'Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue.', '<h1>Hello!</h1>', 1, '2018-05-22 00:52:54', '2018-01-18 07:55:43');
insert into email (from_address, subject, text_message, html_message, folder_id, send_time, receive_time) values (2, 'coherent', 'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '<h1>Hello!</h1>', 2, '2018-03-18 02:39:22', '2018-02-13 23:53:53');
--EMAIL_TO_ADDRESS
insert into email_to_address(email_id, to_email_id) values (1, 3);
insert into email_to_address(email_id, to_email_id) values (1, 8);
insert into email_to_address(email_id, to_email_id) values (2, 2);
insert into email_to_address(email_id, to_email_id) values (3, 4);
insert into email_to_address(email_id, to_email_id) values (5, 1);
insert into email_to_address(email_id, to_email_id) values (5, 2);
insert into email_to_address(email_id, to_email_id) values (6, 6);
insert into email_to_address(email_id, to_email_id) values (7, 7);
insert into email_to_address(email_id, to_email_id) values (8, 8);
insert into email_to_address(email_id, to_email_id) values (9, 9);
insert into email_to_address(email_id, to_email_id) values (10, 10);
insert into email_to_address(email_id, to_email_id) values (11, 10);
insert into email_to_address(email_id, to_email_id) values (12, 1);
insert into email_to_address(email_id, to_email_id) values (13, 2);
insert into email_to_address(email_id, to_email_id) values (14, 3);
insert into email_to_address(email_id, to_email_id) values (15, 4);
insert into email_to_address(email_id, to_email_id) values (16, 5);
insert into email_to_address(email_id, to_email_id) values (17, 6);
insert into email_to_address(email_id, to_email_id) values (18, 6);
insert into email_to_address(email_id, to_email_id) values (19, 6);
insert into email_to_address(email_id, to_email_id) values (20, 6);
--EMAIL_BCC_ADDRESS
insert into email_bcc_address(email_id, bcc_email_id) values (1, 4);
insert into email_bcc_address(email_id, bcc_email_id) values (1, 7); 
insert into email_bcc_address(email_id, bcc_email_id) values (1, 2);
insert into email_bcc_address(email_id, bcc_email_id) values (7, 6);
insert into email_bcc_address(email_id, bcc_email_id) values (7, 5);
insert into email_bcc_address(email_id, bcc_email_id) values (15, 3);
insert into email_bcc_address(email_id, bcc_email_id) values (20, 3);
insert into email_bcc_address(email_id, bcc_email_id) values (21, 8);
--EMAIL_CC_ADDRESS
insert into email_cc_address(email_id, cc_email_id) values (1, 5);
insert into email_cc_address(email_id, cc_email_id) values (1, 6);
insert into email_cc_address(email_id, cc_email_id) values (2, 4);
insert into email_cc_address(email_id, cc_email_id) values (2, 9);
insert into email_cc_address(email_id, cc_email_id) values (3, 6);
insert into email_cc_address(email_id, cc_email_id) values (10, 5);
insert into email_cc_address(email_id, cc_email_id) values (15, 1);
insert into email_cc_address(email_id, cc_email_id) values (13, 2);
insert into email_cc_address(email_id, cc_email_id) values (20, 6);
insert into email_cc_address(email_id, cc_email_id) values (10, 8);
  
--EMAIL_ATTACHMENT
insert into email_attachment(email_id, attachment_id) values (1, 1);
insert into email_attachment(email_id, attachment_id) values (1, 2);
insert into email_attachment(email_id, attachment_id) values (1, 3);
insert into email_attachment(email_id, attachment_id) values (1, 4);
insert into email_attachment(email_id, attachment_id) values (2, 5);
insert into email_attachment(email_id, attachment_id) values (3, 6);
insert into email_attachment(email_id, attachment_id) values (4, 7);
insert into email_attachment(email_id, attachment_id) values (15, 8);
insert into email_attachment(email_id, attachment_id) values (19, 9);
insert into email_attachment(email_id, attachment_id) values (11, 10);    





/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.data;

import jodd.mail.EmailAddress;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Quan-Nguyen
 */
//@Ignore
public class EmailAddressBeanTest {
    
    public EmailAddressBeanTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    @Test
    public void constructor2Params_Valid_ObjectCreated() {
        String emailAddress = "nguyenquan233@gmail.com";
        String personName = "Anh Quan Nguyen";
        
        EmailAddressBean bean = new EmailAddressBean(emailAddress, personName);
        
        Assert.assertNotNull(bean);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void constructor2Params_NullValue_Exception() {
        String emailAddress = null;
        String personName = "Anh Quan Nguyen";
        
        EmailAddressBean bean = new EmailAddressBean(emailAddress, personName);
        
        Assert.assertNotNull(bean);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void constructor2Params_InvalidEmail_ExceptionThrown() {
        String emailAddress = "testtest";
        String personName = "Anh Quan Nguyen";
        
        EmailAddressBean bean = new EmailAddressBean(emailAddress, personName);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void setEmailAddress_InvalidEmail_ExceptionThrown() {
        String emailAddress = "2345";
        String personName = "Anh Quan Nguyen";
        
        EmailAddressBean bean = new EmailAddressBean(emailAddress, personName);
    }
    
    @Test
    public void setEmailAddress_ValidEmail_NoException() {
        String emailAddress = "testtest@gmail.com";
        String personName = "Anh Quan Nguyen";
        
        EmailAddressBean bean = new EmailAddressBean(emailAddress, personName);
        bean.setEmailAddress("hello@gmail.com");
        
        Assert.assertEquals(bean.getEmailAddress(), bean.getEmailAddress());
    }
    
    @Test
    public void toJoddEmailAddress_Valid_NoException() {
        String emailAddress = "nguyenquan233@gmail.com";
        String personName = "Anh Quan Nguyen";
        
        EmailAddressBean bean = new EmailAddressBean(emailAddress, personName);
        EmailAddress res = bean.toJoddEmailAddress();
        
        Assert.assertEquals(res.getEmail(), emailAddress);
        Assert.assertEquals(res.getPersonalName(), personName);
    }
    
    @Test
    public void equals_True_NoException() {
        String emailAddress = "nguyenquan233@gmail.com";
        String personName = "Anh Quan Nguyen";
        String emailAddress2 = "nguyenquan233@gmail.com";
        String personName2 = "Anh Quan Nguyen";
        
        EmailAddressBean bean = new EmailAddressBean(emailAddress, personName);
        EmailAddressBean bean2 = new EmailAddressBean(emailAddress2, personName2);
        
        Assert.assertEquals(bean, bean2);
    }
    
    @Test
    public void equals_False_NoException() {
        String emailAddress = "nguyenquan233@gmail.com";
        String personName = "Anh Quan Nguyen";
        String emailAddress2 = "ngnquan233@gmail.com";
        String personName2 = "Anh Nguyen";
        
        EmailAddressBean bean = new EmailAddressBean(emailAddress, personName);
        EmailAddressBean bean2 = new EmailAddressBean(emailAddress2, personName2);
        
        Assert.assertNotSame(bean2, bean);
    }
    
    
}

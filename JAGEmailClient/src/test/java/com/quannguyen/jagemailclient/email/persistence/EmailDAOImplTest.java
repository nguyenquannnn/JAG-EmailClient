package com.quannguyen.jagemailclient.email.persistence;

import com.quannguyen.jagemailclient.db.controller.DbConnectionHandler;
import com.quannguyen.jagemailclient.db.controller.DbConnectionHandlerImpl;
import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import com.quannguyen.jagemailclient.email.data.EmailAttachmentBean;
import com.quannguyen.jagemailclient.email.data.EmailBean;
import com.quannguyen.jagemailclient.folder.persistence.FolderDAO;
import com.quannguyen.jagemailclient.folder.persistence.FolderDAOImpl;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Quan-Nguyen
 */
//@Ignore
public class EmailDAOImplTest {
    private static final Logger LOG = LoggerFactory.getLogger(EmailDAOImplTest.class);
    private final EmailDAO emailDAO;
    private final DbConnectionHandler handler;
    
    public EmailDAOImplTest() {
        String url = "jdbc:mysql://localhost:3306/email_system_test?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
        String user = "TheUser";
        String password = "tester";
        handler = new DbConnectionHandlerImpl(url, user, password);
        this.emailDAO = new EmailDAOImpl(handler);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        LOG.info("Seeding Database");
        final String seedTableCreatingScript = loadAsString("./src/main/resources/scripts/EmailSystemSchema.sql");
        try (Connection connection = handler.getConnection()) {
            for (String statement : splitStatements(new StringReader(
                    seedTableCreatingScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
            
        final String seedDataScript = loadAsString("./src/main/resources/scripts/InsertMockDataEmailSystem.sql");
        try (Connection connection2 = handler.getConnection()) {
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection2.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }
    
    /**
     * The following methods support the seedDatabase method
     * @author: Ken Fogel
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = new FileInputStream(new File(path));
                Scanner scanner = new Scanner(inputStream);) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
    
    @After
    public void tearDown() {
    }
        
    @Test(timeout = 1000)
    public void getEmailsFromFolder_Valid_NoExcepion() throws SQLException{
        EmailAttachmentDAOImpl attachmentDAO = new EmailAttachmentDAOImpl(handler);
        EmailAddressDAOImpl addressDAO = new EmailAddressDAOImpl(handler); 
        List<EmailBean> expects = new ArrayList<>();
        EmailBean bean1 = new EmailBean();
        bean1.setFrom(addressDAO.getEmailAddressFrom(16));
        bean1.setSubject("clear-thinking");
        bean1.setTo(addressDAO.getEmailAddressToFromEmail(16));
        bean1.setCc(addressDAO.getEmailAddressCcFromEmail(16));
        bean1.setBcc(addressDAO.getEmailAddressBccFromEmail(16));
        bean1.setTextMessage("Fusce posuere felis sed lacus. Morbi sem mauris, laoreet "
                + "ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel "
                + "sem. Sed sagittis. Nam congue, risus semper porta volutpat, "
                + "quam pede lobortis ligula, sit amet eleifend pede libero quis "
                + "orci.");
        bean1.setHtmlMessage("<h1>Hello!</h1>");
        bean1.setFolderId(4);
        bean1.setSendDateTime(LocalDateTime.of(2018,02,06,10,21,33));
        bean1.setReceiveDateTime(LocalDateTime.of(2017,11,03,22,23,27));
        bean1.setAttachments(attachmentDAO.getEmailAttachmentsOfEmail(16));
        bean1.setEmbeddedAttachments(attachmentDAO.getEmbeddedEmailAttachmentsOfEmail(16));
        expects.add(bean1);
        
        EmailBean bean2 = new EmailBean();
        bean2.setFrom(addressDAO.getEmailAddressFrom(17));
        bean2.setSubject("knowledge base");
        bean2.setTo(addressDAO.getEmailAddressToFromEmail(17));
        bean2.setCc(addressDAO.getEmailAddressCcFromEmail(17));
        bean2.setBcc(addressDAO.getEmailAddressBccFromEmail(17));
        bean2.setTextMessage("Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.");
        bean2.setHtmlMessage("<h1>Hello!</h1>");
        bean2.setFolderId(4);
        bean2.setSendDateTime(LocalDateTime.of(2018,8,27,01,16,58));
        bean2.setReceiveDateTime(LocalDateTime.of(2018,04,02,12,58,13));
        bean2.setAttachments(attachmentDAO.getEmailAttachmentsOfEmail(17));
        bean2.setEmbeddedAttachments(attachmentDAO.getEmbeddedEmailAttachmentsOfEmail(17));
        expects.add(bean2);
        
        FolderDAO folderDAO = new FolderDAOImpl(handler);
        List<EmailBean> actual = emailDAO.getEmailsFromFolder(folderDAO.getFolder(4));
        
        Assert.assertArrayEquals(expects.toArray(), expects.toArray());
    }
    
    @Test(timeout = 1000)
    public void getEmailsFromFolder_NoEmails_NoExcepion() throws SQLException {
        List<EmailBean> expects = new ArrayList<>();
          
        FolderDAO folderDAO = new FolderDAOImpl(handler);
        List<EmailBean> actual = emailDAO.getEmailsFromFolder(folderDAO.getFolder(5));
        
        Assert.assertArrayEquals(expects.toArray(), actual.toArray());
    }
    
    @Test(timeout = 1000)
    public void getEmail_Valid_NoException() throws SQLException {
        EmailAttachmentDAOImpl attachmentDAO = new EmailAttachmentDAOImpl(handler);
        EmailAddressDAOImpl addressDAO = new EmailAddressDAOImpl(handler); 
        EmailBean expect = new EmailBean();
        expect.setFrom(addressDAO.getEmailAddress("kdunn0@wisc.edu"));
        expect.setSubject("client-driven");
        expect.setTextMessage("Fusce posuere felis sed lacus. Morbi sem mauris,"
                + " laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus "
                + "dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, "
                + "quam pede lobortis ligula, sit amet eleifend pede libero quis orci. "
                + "Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse "
                + "potenti. Cras in purus eu magna vulputate luctus. Cum sociis "
                + "natoque penatibus et magnis dis parturient montes, nascetur "
                + "ridiculus mus.");
        expect.setHtmlMessage("<h1>Hello!</h1>");
        expect.setFolderId(1);
        expect.setSendDateTime(LocalDateTime.of(2018,06,16,20,05,22));
        expect.setReceiveDateTime(LocalDateTime.of(2018,02,12,07,24,40));
        expect.setAttachments(attachmentDAO.getEmailAttachmentsOfEmail(10));
        expect.setEmbeddedAttachments(attachmentDAO.getEmbeddedEmailAttachmentsOfEmail(10));
        expect.setTo(addressDAO.getEmailAddressToFromEmail(10));
        expect.setCc(addressDAO.getEmailAddressCcFromEmail(10));
        expect.setBcc(addressDAO.getEmailAddressBccFromEmail(10));
        
        EmailBean actual = emailDAO.getEmail(10);
        
        Assert.assertEquals(expect, actual);
    }
    
    @Test(timeout = 2000)
    public void createEmail_FullEmailBean_NoException() throws SQLException {
        EmailBean bean = new EmailBean();
        File attachement2 = new File("./src/main/resources/attachments/embed2.jpg");
        File attachement = new File("./src/main/resources/attachments/test2.jpg");
        try {
            List temp = Arrays.asList(new EmailAttachmentBean[] {new EmailAttachmentBean(attachement)});
            bean.setAttachments(temp);
            
            List temp2 = Arrays.asList(new EmailAttachmentBean[] {new EmailAttachmentBean(attachement2, true)});
            bean.setEmbeddedAttachments(temp2);
        } catch (IOException ioe) {
            System.err.print(ioe);
        }
        bean.setFrom(new EmailAddressBean("send.1635333@gmail.com", "Quan Nguyen"));
        bean.setSubject("Hello this is Main!");
        bean.setTextMessage("Hello Everyone from Main");
        bean.setHtmlMessage("<META http-equiv=Content-Type content=\"text/html; " +
            "charset=utf-8\"><body><h1>Hey!</h1><img src='cid:embed2.jpg'>" +
            "<h2>Hay!</h2></body></html>");
        bean.setTo(Arrays.asList(new EmailAddressBean[] {new EmailAddressBean("receive.1632647@gmail.com", "Receive Test")}));
        bean.setCc(Arrays.asList(new EmailAddressBean[] {new EmailAddressBean("other.1632647@gmail.com", "Other Test")}));
        bean.setSendDateTime(LocalDateTime.now());
        bean.setReceiveDateTime(LocalDateTime.now());
        bean.setFolderId(1);
        
        int result = emailDAO.createEmail(bean);
        EmailBean actual = emailDAO.getEmail(bean.getId());
        
        Assert.assertEquals(1, result);
        Assert.assertEquals(bean, bean);
    }
    
    @Test(timeout = 1000)
    public void deleteEmail_Valid_NoException() throws SQLException {
        int result = emailDAO.deleteEmail(10);
        
        Assert.assertEquals(1, result);
    }
    
    @Test(timeout = 1000)
    public void updateEmail_Valid_NoException() throws SQLException {
        EmailBean bean1 = new EmailBean();
        
        EmailAddressDAO addressDAO = new EmailAddressDAOImpl(handler);
        EmailAttachmentDAO attachmentDAO = new EmailAttachmentDAOImpl(handler);    
        
        bean1.setId(16);
        bean1.setFrom(addressDAO.getEmailAddressFrom(16));
        bean1.setSubject("Empty");
        bean1.setTo(addressDAO.getEmailAddressToFromEmail(16));
        bean1.setCc(addressDAO.getEmailAddressCcFromEmail(16));
        bean1.setBcc(addressDAO.getEmailAddressBccFromEmail(16));
        bean1.setTextMessage("Updated");
        bean1.setHtmlMessage("<h1>Updated!</h1>");
        bean1.setFolderId(4);
        bean1.setSendDateTime(LocalDateTime.of(2018,02,06,10,21,33));
        bean1.setReceiveDateTime(LocalDateTime.of(2017,11,03,22,23,27));
        bean1.setAttachments(attachmentDAO.getEmailAttachmentsOfEmail(16));
        bean1.setEmbeddedAttachments(attachmentDAO.getEmbeddedEmailAttachmentsOfEmail(16));
        
        int result = emailDAO.updateEmail(bean1);
        EmailBean actual = emailDAO.getEmail(16);
        
        Assert.assertEquals(1, result);
        Assert.assertEquals(bean1, actual);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.controller;

import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import com.quannguyen.jagemailclient.email.data.EmailBean;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author MA
 */
@Ignore
public class EmailReceiverTest {
    EmailAddressBean from;
    String passwordFrom;
    EmailReceiver emailReceiver;
    EmailAddressBean cc;
    EmailAddressBean to;
    String passwordTo;
    EmailBean emailBean;
    EmailSender sender;
    
    public EmailReceiverTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        from = new EmailAddressBean("send.1632647@gmail.com", "Send Test");
        passwordFrom = "d5=9u~xo";
        to = new EmailAddressBean("receive.1632647@gmail.com", "Receive Test");
        passwordTo = "p,7\\j?~(";
        cc = new EmailAddressBean("other.1632647@gmail.com", "Other TestS");
        sender = new EmailSender(from, passwordFrom);
    }
    
    @After
    public void tearDown() {
    }
    
    @Test
    public void constuctor2Param_Valid_ObjectCreated() {
        emailReceiver = new EmailReceiver(to, passwordTo);
        
        Assert.assertNotNull(emailReceiver);
    }
    
    @Test 
    public void receiveEmail_ValidEmail_NoException() throws InterruptedException {
        emailReceiver = new EmailReceiver(to, passwordTo);
        
        emailBean = new EmailBean();
        emailBean.setSubject("Hello from ReceiverTest!");
        emailBean.setTextMessage("Hello Everyone from Test");
        emailBean.setHtmlMessage("<h1>Hello from Test</h1>");
        emailBean.setFrom(from);
        emailBean.setTo(Arrays.asList(new EmailAddressBean[] {to}));
        emailBean.setCc(Arrays.asList(new EmailAddressBean[] {cc}));
       
        sender.sendEmail(emailBean);
        Thread.sleep(2000);
        
        List<EmailBean> receivedEmails = emailReceiver.receiveEmail();
        Assert.assertNotNull(receivedEmails);
        Assert.assertNotNull(receivedEmails.stream().filter(r -> r.equals(emailBean)).count() == receivedEmails.size());
    }
    
    @Test
    public void receiveEmail_NoNewEmail_NoException() throws InterruptedException {
        emailReceiver = new EmailReceiver(to, passwordTo);
        
        List<EmailBean> receivedEmails = emailReceiver.receiveEmail();
        Assert.assertNotNull(receivedEmails);
        Assert.assertTrue(receivedEmails.size() == 0);
    }
    
        @Test 
    public void receiveEmail_TwoNewdEmail_NoException() throws InterruptedException {
        emailReceiver = new EmailReceiver(to, passwordTo);
        
        emailBean = new EmailBean();
        emailBean.setSubject("Hello from ReceiverTest!");
        emailBean.setTextMessage("Hello Everyone from Test");
        emailBean.setHtmlMessage("<h1>Hello from Test</h1>");
        emailBean.setFrom(from);
        emailBean.setTo(Arrays.asList(new EmailAddressBean[] {to}));
        emailBean.setCc(Arrays.asList(new EmailAddressBean[] {cc}));
       
        sender.sendEmail(emailBean);
        sender.sendEmail(emailBean);
        Thread.sleep(2000);
        
        List<EmailBean> receivedEmails = emailReceiver.receiveEmail();
        Assert.assertNotNull(receivedEmails);
        Assert.assertTrue(receivedEmails.size() == 2);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.controller;

import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import com.quannguyen.jagemailclient.email.data.EmailAttachmentBean;
import com.quannguyen.jagemailclient.email.data.EmailBean;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Anh Quan Nguyen
 */
//@Ignore
public class SendAndReceiveTest {
    EmailAddressBean from;
    String passwordFrom;
    EmailReceiver receiver;
    EmailAddressBean to;
    String passwordTo;
    EmailAddressBean cc;
    String passwordCc;
    EmailAddressBean bcc;
    String passwordBcc;
    EmailReceiver ccReceiver;
    EmailReceiver bccReceiver;

    EmailSender sender;
    
    public SendAndReceiveTest() {
        from = new EmailAddressBean("send.1632647@gmail.com", "Send Test");
        passwordFrom = "d5=9u~xo";
        to = new EmailAddressBean("receive.1632647@gmail.com", "Receive Test");
        passwordTo = "p,7\\j?~(";
        sender = new EmailSender(from, passwordFrom);
        receiver = new EmailReceiver(new EmailAddressBean("receive.1632647@gmail.com", "Receive Test"), "p,7\\j?~(");
        cc = new EmailAddressBean("other.1633656@gmail.com", "Nick");
        passwordCc = "schoolemail123";
        bcc = new EmailAddressBean("other.1632647@gmail.com", "Quan");
        passwordBcc = "k59|$'_@";
        
        receiver.receiveEmail();
        ccReceiver = new EmailReceiver(cc, passwordCc);
        bccReceiver = new EmailReceiver(bcc, passwordBcc);
       
        ccReceiver.receiveEmail();
        bccReceiver.receiveEmail();
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        from = new EmailAddressBean("send.1632647@gmail.com", "Send Test");
        passwordFrom = "d5=9u~xo";
        to = new EmailAddressBean("receive.1632647@gmail.com", "Receive Test");
        passwordTo = "p,7\\j?~(";
        sender = new EmailSender(from, passwordFrom);
        receiver = new EmailReceiver(new EmailAddressBean("receive.1632647@gmail.com", "Receive Test"), "p,7\\j?~(");
        cc = new EmailAddressBean("other.1633656@gmail.com", "Nick");
        passwordCc = "schoolemail123";
        bcc = new EmailAddressBean("other.1632647@gmail.com", "Quan");
        passwordBcc = "k59|$'_@";
        
        receiver.receiveEmail();
        ccReceiver.receiveEmail();
        bccReceiver.receiveEmail();
    }
    
    @After
    public void tearDown() {
    }

   @Test
   public void sendAndReceive_ValidEmailWithAllFields_NoException() throws InterruptedException {
        EmailBean bean = new EmailBean();
        File attachement2 = new File("./src/main/resources/attachments/embed2.jpg");
        File attachement = new File("./src/main/resources/attachments/test2.jpg");
        try {
            List temp = Arrays.asList(new EmailAttachmentBean[] {new EmailAttachmentBean(attachement)});
            bean.setAttachments(temp);
            
            List temp2 = Arrays.asList(new EmailAttachmentBean[] {new EmailAttachmentBean(attachement2, true)});
            bean.setEmbeddedAttachments(temp2);
        } catch (IOException ioe) {
            System.err.print(ioe);
        }
        bean.setFrom(new EmailAddressBean("send.1632647@gmail.com", "Send Test"));
        bean.setSubject("Hello this is Main!");
        bean.setTextMessage("Hello Everyone from Main");
        bean.setHtmlMessage("<META http-equiv=Content-Type content=\"text/html; " +
            "charset=utf-8\"><body><h1>Hey!</h1><img src='cid:embed2.jpg'>" +
            "<h2>Hay!</h2></body></html>");
        bean.setTo(Arrays.asList(new EmailAddressBean[] {new EmailAddressBean("receive.1632647@gmail.com", "Receive Test")}));
        bean.setCc(Arrays.asList(new EmailAddressBean[] {new EmailAddressBean("other.1632647@gmail.com", "Other Test")}));
        
        sender.sendEmail(bean);
        Thread.sleep(5000);
        List<EmailBean> res = receiver.receiveEmail();
        
        Assert.assertEquals(bean, res.get(0));
   }
   
   @Test(expected = NullPointerException.class)
   public void sendAndReceive_EmptyEmail_NullPointerException() throws InterruptedException {
       EmailBean bean = new EmailBean();
       
       sender.sendEmail(bean);
       Thread.sleep(2000);
       List<EmailBean> res = receiver.receiveEmail();  
   }
   
   //Minimal email need at least from and either to,cc or bcc
   @Test
   public void sendAndReceive_MinimalEmail_NoException() throws InterruptedException {
       EmailBean bean = new EmailBean();
       bean.setFrom(from);
       bean.setTo(Arrays.asList(new EmailAddressBean[] { to }));
       sender.sendEmail(bean);
       Thread.sleep(2000);
       List<EmailBean> res = receiver.receiveEmail();
       
       Assert.assertEquals(bean, res.get(0));
   }
   
   @Test
   public void sendAndReceive_TextAndHtmlMessage_NoException() throws InterruptedException {
       EmailBean bean = new EmailBean();
       bean.setFrom(from);
       bean.setTo(Arrays.asList(new EmailAddressBean[] { to }));
       bean.setSubject("Hello this is Main!");
       bean.setTextMessage("Hello Everyone from Main");
       bean.setHtmlMessage("<META http-equiv=Content-Type content=\"text/html; " +
            "charset=utf-8\"><body><h1>Hey!</h1><h2>Hay!</h2></body></html>");
       
       sender.sendEmail(bean);
       Thread.sleep(2000);
       List<EmailBean> res = receiver.receiveEmail();
       Assert.assertEquals(res.get(0), bean);
   }
   
   @Test
   public void sendAndReceive_HtmlTextAndAttachment_NoException() throws InterruptedException {
       EmailBean bean = new EmailBean();
       bean.setFrom(from);
       bean.setTo(Arrays.asList(new EmailAddressBean[] { to }));
       bean.setSubject("Hello this is Main!");
       bean.setHtmlMessage("<META http-equiv=Content-Type content=\"text/html; " +
            "charset=utf-8\"><body><h1>Hey!</h1><h2>Hay!</h2></body></html>");
       EmailAttachmentBean attachmentBean = new EmailAttachmentBean();
       attachmentBean.setAttachmentName("Test");
       attachmentBean.setData(new byte[] {1,2,3,4,5});
       bean.addAttachments(attachmentBean);
       
       sender.sendEmail(bean);
       Thread.sleep(2000);
       List<EmailBean> res = receiver.receiveEmail();
       Assert.assertEquals(res.get(0), bean);
   }
   
   @Test
   public void sendAndReceive_AttachmentsNoMessage_NoException() throws InterruptedException {
       EmailBean bean = new EmailBean();
       bean.setFrom(from);
       bean.setTo(Arrays.asList(new EmailAddressBean[] { to }));
       bean.setSubject("Hello this is Main!");
       EmailAttachmentBean attachmentBean = new EmailAttachmentBean();
       attachmentBean.setAttachmentName("Test");
       attachmentBean.setData(new byte[] {1,2,3,4,5});
       bean.addAttachments(attachmentBean);
       
       EmailAttachmentBean attachmentBean2 = new EmailAttachmentBean();
       attachmentBean2.setAttachmentName("Test2");
       attachmentBean2.setData(new byte[] {1,2,3,4,5});
       attachmentBean2.setIsEmbedded(true);
       bean.addEmbeddedAttachments(attachmentBean2);
       
       sender.sendEmail(bean);
       Thread.sleep(2000);
       List<EmailBean> res = receiver.receiveEmail();
       Assert.assertEquals(res.get(0), bean);
   }
   
   @Test
   public void sendAndReceive_CcBcc_NoException() throws InterruptedException {
       EmailBean bean = new EmailBean();
       bean.setFrom(from);
       bean.setTo(Arrays.asList(new EmailAddressBean[] { to }));
       bean.getCc().add(cc);
       bean.getBcc().add(bcc);
       bean.setSubject("Hello this is Main!");
       EmailAttachmentBean attachmentBean = new EmailAttachmentBean();
       attachmentBean.setAttachmentName("Test");
       attachmentBean.setData(new byte[] {1,2,3,4,5});
       bean.addAttachments(attachmentBean);
               
       sender.sendEmail(bean);
       EmailReceiver ccReceiver = new EmailReceiver(cc, passwordCc);
       EmailReceiver bccReceiver = new EmailReceiver(bcc, passwordBcc);
       Thread.sleep(2000);
       List<EmailBean> res1 = ccReceiver.receiveEmail();
       List<EmailBean> res2 = bccReceiver.receiveEmail();
       
       Assert.assertEquals(res1.get(0), bean);
       Assert.assertEquals(res2.get(0), bean);
   }
}

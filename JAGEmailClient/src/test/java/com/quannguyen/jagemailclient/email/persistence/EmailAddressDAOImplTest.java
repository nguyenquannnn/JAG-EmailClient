/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.email.persistence;

import com.quannguyen.jagemailclient.db.controller.DbConnectionHandler;
import com.quannguyen.jagemailclient.db.controller.DbConnectionHandlerImpl;
import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import com.quannguyen.jagemailclient.folder.persistence.FolderDAOImpl;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1632647
 */
//@Ignore
public class EmailAddressDAOImplTest {
    private static final Logger LOG = LoggerFactory.getLogger(EmailAddressDAOImpl.class);
    private EmailAddressDAO addressDAO;
    private DbConnectionHandler handler;
    
    public EmailAddressDAOImplTest() {
        String url = "jdbc:mysql://localhost:3306/email_system_test?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
        String user = "TheUser";
        String password = "tester";
        handler = new DbConnectionHandlerImpl(url, user, password);
        this.addressDAO = new EmailAddressDAOImpl(handler);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        LOG.info("Seeding Database");
        final String seedTableCreatingScript = loadAsString("./src/main/resources/scripts/EmailSystemSchema.sql");
        try (Connection connection = handler.getConnection()) {
            for (String statement : splitStatements(new StringReader(
                    seedTableCreatingScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
            
        final String seedDataScript = loadAsString("./src/main/resources/scripts/InsertMockDataEmailSystem.sql");
        try (Connection connection2 = handler.getConnection()) {
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection2.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }
    
    /**
     * The following methods support the seedDatabase method
     * @author: Ken Fogel
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = new FileInputStream(new File(path));
                Scanner scanner = new Scanner(inputStream);) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
    
    @After
    public void tearDown() {
    }
    
    @Test(timeout = 10000)
    public void getEmailAddress_Valid_NoException() throws SQLException{
        EmailAddressBean expected = new EmailAddressBean("kdunn0@wisc.edu", "Kimberlyn Dunn");
        
        EmailAddressBean actual = addressDAO.getEmailAddress("kdunn0@wisc.edu");
        
        Assert.assertEquals(expected, actual);
    }
    
    @Test(timeout = 10000)
    public void getEmailAddress_NullEmailAddress_NoException() throws SQLException {       
        EmailAddressBean actual = addressDAO.getEmailAddress(null);
        
        Assert.assertEquals(null, actual);
    }
    
    @Test(timeout = 10000)
    public void getAllEmailAddress_Valid_NoException() throws SQLException {
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("kdunn0@wisc.edu", "Kimberlyn Dunn"));
        expected.add(new EmailAddressBean("kgutsell1@youtube.com", "Kaila Gutsell"));
        expected.add(new EmailAddressBean("rlucien2@hao123.com", "Rutter Lucien"));
        expected.add(new EmailAddressBean("pcolombier3@ifeng.com", "Philipa Colombier"));
        expected.add(new EmailAddressBean("co4@eventbrite.com", "Chris O Quirk"));
        expected.add(new EmailAddressBean("tcornhill5@army.mil", "Trever cornhill"));
        expected.add(new EmailAddressBean("ageertz6@unicef.org", "Amos Geertz"));
        expected.add(new EmailAddressBean("ihayhurst7@theatlantic.com", "Inigo Hayhurst"));
        expected.add(new EmailAddressBean("wspurryer8@123-reg.co.uk", "Willamina Spurryer"));
        expected.add(new EmailAddressBean("aperett9@furl.net", "Agustin Perett"));
        
        List<EmailAddressBean> actual = addressDAO.getAllEmailAddress();
        
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void getEmailAddressCcFromEmail_NoCc_NoException() throws SQLException {
        List<EmailAddressBean> expected = new ArrayList<>();
        
        List<EmailAddressBean> actual = addressDAO.getEmailAddressCcFromEmail(4);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void getEmailAddressCcFromEmail_OneCc_NoException() throws SQLException {
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("tcornhill5@army.mil", "Trever cornhill"));
        
        List<EmailAddressBean> actual = addressDAO.getEmailAddressCcFromEmail(3);
        
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void getEmailAddressCcFromEmail_MultipleCc_NoException() throws SQLException {
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("co4@eventbrite.com", "Chris O Quirk"));
        expected.add(new EmailAddressBean("tcornhill5@army.mil", "Trever cornhill"));
        
        List<EmailAddressBean> actual = addressDAO.getEmailAddressCcFromEmail(1);
        
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void getEmailAddressBccFromEmail_NoBcc_NoException() throws SQLException {
        List<EmailAddressBean> expected = new ArrayList<>();
        
        List<EmailAddressBean> actual = addressDAO.getEmailAddressBccFromEmail(4);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
        
    @Test(timeout = 10000)
    public void getEmailAddressBccFromEmail_OneBcc_NoException() throws SQLException {
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("rlucien2@hao123.com", "Rutter Lucien"));
        
        List<EmailAddressBean> actual = addressDAO.getEmailAddressBccFromEmail(15);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void getEmailAddressBccFromEmail_MultipleBcc_NoException() throws SQLException {
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("co4@eventbrite.com", "Chris O Quirk"));
        expected.add(new EmailAddressBean("tcornhill5@army.mil", "Trever cornhill"));
        
        List<EmailAddressBean> actual = addressDAO.getEmailAddressBccFromEmail(7);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void getEmailAddressToFromEmail_NoTo_NoException() throws SQLException {
        List<EmailAddressBean> expected = new ArrayList<>();
        
        List<EmailAddressBean> actual = addressDAO.getEmailAddressToFromEmail(-1);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
        
    @Test(timeout = 10000)
    public void getEmailAddressToFromEmail_OneTo_NoException() throws SQLException {
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("ageertz6@unicef.org", "Amos Geertz"));
        
        List<EmailAddressBean> actual = addressDAO.getEmailAddressToFromEmail(7);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void getEmailAddressToFromEmail_MultipleTo_NoException() throws SQLException {
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("rlucien2@hao123.com", "Rutter Lucien"));
        expected.add(new EmailAddressBean("ihayhurst7@theatlantic.com", "Inigo Hayhurst"));
        
        List<EmailAddressBean> actual = addressDAO.getEmailAddressToFromEmail(1);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void getEmailAddressFrom_Valid_NoException() throws SQLException {
        EmailAddressBean expected = new EmailAddressBean("kdunn0@wisc.edu", "Kimberlyn Dunn");
        
        EmailAddressBean actual = addressDAO.getEmailAddressFrom(1);
        
        Assert.assertEquals(expected, actual);
    }
    
    @Test(timeout = 10000)
    public void createEmailAddress_Valid_NoException() throws SQLException {
        EmailAddressBean bean = new EmailAddressBean("nguyenquan233@gmail.com", "Quan Nguyen");
        
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("kdunn0@wisc.edu", "Kimberlyn Dunn"));
        expected.add(new EmailAddressBean("kgutsell1@youtube.com", "Kaila Gutsell"));
        expected.add(new EmailAddressBean("rlucien2@hao123.com", "Rutter Lucien"));
        expected.add(new EmailAddressBean("pcolombier3@ifeng.com", "Philipa Colombier"));
        expected.add(new EmailAddressBean("co4@eventbrite.com", "Chris O Quirk"));
        expected.add(new EmailAddressBean("tcornhill5@army.mil", "Trever cornhill"));
        expected.add(new EmailAddressBean("ageertz6@unicef.org", "Amos Geertz"));
        expected.add(new EmailAddressBean("ihayhurst7@theatlantic.com", "Inigo Hayhurst"));
        expected.add(new EmailAddressBean("wspurryer8@123-reg.co.uk", "Willamina Spurryer"));
        expected.add(new EmailAddressBean("aperett9@furl.net", "Agustin Perett"));
        expected.add(bean);
        
        int result = addressDAO.createEmailAddress(bean);
        List<EmailAddressBean> actual = addressDAO.getAllEmailAddress();
        
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void deleteEmailAddress_Valid_NoException() throws SQLException {        
        EmailAddressBean toDelete = new EmailAddressBean(10, "aperett9@furl.net", "Agustin Perett");
        
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("kdunn0@wisc.edu", "Kimberlyn Dunn"));
        expected.add(new EmailAddressBean("kgutsell1@youtube.com", "Kaila Gutsell"));
        expected.add(new EmailAddressBean("rlucien2@hao123.com", "Rutter Lucien"));
        expected.add(new EmailAddressBean("pcolombier3@ifeng.com", "Philipa Colombier"));
        expected.add(new EmailAddressBean("co4@eventbrite.com", "Chris O Quirk"));
        expected.add(new EmailAddressBean("tcornhill5@army.mil", "Trever cornhill"));
        expected.add(new EmailAddressBean("ageertz6@unicef.org", "Amos Geertz"));
        expected.add(new EmailAddressBean("ihayhurst7@theatlantic.com", "Inigo Hayhurst"));
        expected.add(new EmailAddressBean("wspurryer8@123-reg.co.uk", "Willamina Spurryer"));
        
        int result = addressDAO.deleteEmailAddress(toDelete);
        List<EmailAddressBean> actual = addressDAO.getAllEmailAddress();
        
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void addEmailAddressCcForEmail_NewAddress_RecordCreatedAndAdded() throws SQLException {
        EmailAddressBean bean = new EmailAddressBean("nguyenquan233@gmail.com", "Quan Nguyen");
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("co4@eventbrite.com", "Chris O Quirk"));
        expected.add(new EmailAddressBean("tcornhill5@army.mil", "Trever cornhill"));
        expected.add(bean);
        
        int result = addressDAO.addEmailAddressCcForEmail(1, bean);
        List<EmailAddressBean> actual = addressDAO.getEmailAddressCcFromEmail(1);
        
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void addEmailAddressCcForEmail_OldAddress_RecordAdded() throws SQLException {
        EmailAddressBean bean = new EmailAddressBean("aperett9@furl.net", "Agustin Perett");
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("co4@eventbrite.com", "Chris O Quirk"));
        expected.add(new EmailAddressBean("tcornhill5@army.mil", "Trever cornhill"));
        expected.add(bean);
        
        int result = addressDAO.addEmailAddressCcForEmail(1, bean);
        List<EmailAddressBean> actual = addressDAO.getEmailAddressCcFromEmail(1);
        
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void addEmailAddressBccForEmail_NewAddress_RecordCreatedAndAdded() throws SQLException {
        EmailAddressBean bean = new EmailAddressBean("nguyenquan233@gmail.com", "Quan Nguyen");
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("co4@eventbrite.com", "Chris O Quirk"));
        expected.add(new EmailAddressBean("tcornhill5@army.mil", "Trever cornhill"));
        expected.add(bean);
        
        int result = addressDAO.addEmailAddressBccForEmail(7, bean);
        List<EmailAddressBean> actual = addressDAO.getEmailAddressBccFromEmail(7);
        
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void addEmailAddressBccForEmail_OldAddress_RecordAdded() throws SQLException {
        EmailAddressBean bean = new EmailAddressBean("aperett9@furl.net", "Agustin Perett");
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("co4@eventbrite.com", "Chris O Quirk"));
        expected.add(new EmailAddressBean("tcornhill5@army.mil", "Trever cornhill"));
        expected.add(bean);
        
        int result = addressDAO.addEmailAddressBccForEmail(7, bean);
        List<EmailAddressBean> actual = addressDAO.getEmailAddressBccFromEmail(7);
        
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void addEmailAddressToForEmail_NewAddress_RecordCreatedAndAdded() throws SQLException {
        EmailAddressBean bean = new EmailAddressBean("nguyenquan233@gmail.com", "Quan Nguyen");
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("rlucien2@hao123.com", "Rutter Lucien"));
        expected.add(new EmailAddressBean("ihayhurst7@theatlantic.com", "Inigo Hayhurst"));
        expected.add(bean);
        
        int result = addressDAO.addEmailAddressToForEmail(1, bean);
        List<EmailAddressBean> actual = addressDAO.getEmailAddressToFromEmail(1);
        
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void addEmailAddressToForEmail_OldAddress_RecordAdded() throws SQLException {
        EmailAddressBean bean = new EmailAddressBean("aperett9@furl.net", "Agustin Perett");
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("rlucien2@hao123.com", "Rutter Lucien"));
        expected.add(new EmailAddressBean("ihayhurst7@theatlantic.com", "Inigo Hayhurst"));
        expected.add(bean);
        
        int result = addressDAO.addEmailAddressToForEmail(1, bean);
        List<EmailAddressBean> actual = addressDAO.getEmailAddressToFromEmail(1);
        
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void deleteEmailAddressToFromEmail_Valid_NoException() throws SQLException {        
        EmailAddressBean toDelete = new EmailAddressBean("ihayhurst7@theatlantic.com", "Inigo Hayhurst");
        
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("rlucien2@hao123.com", "Rutter Lucien"));
        
        int result = addressDAO.deleteEmailAddressToFromEmail(1, toDelete);
        List<EmailAddressBean> actual = addressDAO.getEmailAddressToFromEmail(1);
        
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void deleteEmailAddressCcFromEmail_Valid_NoException() throws SQLException {        
        EmailAddressBean toDelete = new EmailAddressBean("tcornhill5@army.mil", "Trever cornhill");
        
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("co4@eventbrite.com", "Chris O Quirk"));
        
        int result = addressDAO.deleteEmailAddressCcFromEmail(1, toDelete);
        List<EmailAddressBean> actual = addressDAO.getEmailAddressCcFromEmail(1);
        
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test(timeout = 10000)
    public void deleteEmailAddressBccFromEmail_Valid_NoException() throws SQLException {        
        EmailAddressBean toDelete = new EmailAddressBean("tcornhill5@army.mil", "Trever cornhill");
        
        List<EmailAddressBean> expected = new ArrayList<>();
        expected.add(new EmailAddressBean("co4@eventbrite.com", "Chris O Quirk"));
   
        int result = addressDAO.deleteEmailAddressBccFromEmail(7, toDelete);
        List<EmailAddressBean> actual = addressDAO.getEmailAddressBccFromEmail(7);
        
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
}

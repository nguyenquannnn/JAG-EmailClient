/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.data;

import com.quannguyen.jagemailclient.email.controller.EmailController;
import com.quannguyen.jagemailclient.email.controller.EmailControllerImpl;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import jodd.mail.Email;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author 1632647
 */
//@Ignore
public class EmailBeanTest {
    EmailAddressBean from;
    String passwordFrom;
    EmailAddressBean to;
    String passwordTo;
    EmailBean emailBean;
    EmailController controller;
    
    public EmailBeanTest() {
        
    }
        
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        from = new EmailAddressBean("send.1632647@gmail.com", "Send Test");
        passwordFrom = "d5=9u~xo";
        to = new EmailAddressBean("receive.1632647@gmail.com", "Receive Test");
        passwordTo = "p,7\\j?~(";
        controller = new EmailControllerImpl(from, passwordFrom,new EmailAddressBean("receive.1632647@gmail.com", "Receive Test"), "p,7\\j?~(", null);

    }
    
    @After
    public void tearDown() {
    }


    @Test
    public void toJoddEmail_ValidEmail_NoException() throws InterruptedException {
        EmailBean bean = new EmailBean();
        File attachement2 = new File("./src/main/resources/attachments/embed2.jpg");
        File attachement = new File("./src/main/resources/attachments/test2.jpg");
        try {
            List temp = Arrays.asList(new EmailAttachmentBean[] {new EmailAttachmentBean(attachement)});
            bean.setAttachments(temp);
            
            List temp2 = Arrays.asList(new EmailAttachmentBean[] {new EmailAttachmentBean(attachement2, true)});
            bean.setEmbeddedAttachments(temp2);
        } catch (IOException ioe) {
            System.err.print(ioe);
        }
        bean.setFrom(new EmailAddressBean("send.1632647@gmail.com", "Quan Nguyen"));
        bean.setSubject("Hello this is Main!");
        bean.setTextMessage("Hello Everyone from Main");
        bean.setHtmlMessage("<META http-equiv=Content-Type content=\"text/html; " +
            "charset=utf-8\"><body><h1>Hey!</h1><img src='cid:embed2.jpg'>" +
            "<h2>Hay!</h2></body></html>");
        bean.setTo(Arrays.asList(new EmailAddressBean[] {new EmailAddressBean("receive.1632647@gmail.com", "Receive Test")}));
        bean.setCc(Arrays.asList(new EmailAddressBean[] {new EmailAddressBean("other.1632647@gmail.com", "Other Test")}));
        
        //Email res = bean.toJoddEmail();
        //sendEmail use toJoddEmail
        controller.sendEmail(bean);
        Thread.sleep(5000);
        List<EmailBean> received= controller.receiveEmail();
        
        Assert.assertEquals(received.get(0), bean);
    }
    
    @Test
    public void equals_NullObject_ReturnFalse() {
        EmailBean bean = new EmailBean();
        
        Assert.assertFalse(bean.equals(null));
    }
    
    @Test
    public void equals_SameBean_ReturnTrue() {
        EmailBean bean = new EmailBean();
        
        Assert.assertTrue(bean.equals(bean));
    }
    
    @Test
    public void equals_EqualsBean_ReturnTrue() {
        EmailBean bean = new EmailBean();
        bean.setFrom(from);
        
        EmailBean bean2 = new EmailBean();
        bean2.setFrom(from);
        
        Assert.assertEquals(bean, bean2);
    }
    
    @Test
    public void equals_DifferentBean_ReturnFalse() {
        EmailBean bean1 = new EmailBean();
        bean1.setFrom(from);
        
        EmailBean bean2 = new EmailBean();
        bean2.setFrom(to);
        
        Assert.assertNotEquals(bean1, bean2);
    }
    
    @Test
    public void equals_DifferentTypeObject_ReturnFalse() {
        EmailBean bean1 = new EmailBean();
        bean1.setFrom(from);
        
        Email e = new Email();
        
        Assert.assertNotEquals(bean1, e);
    }
    
}

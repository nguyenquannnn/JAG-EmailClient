/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.controller;

import com.quannguyen.jagemailclient.email.data.EmailAddressBean;
import com.quannguyen.jagemailclient.email.data.EmailAttachmentBean;
import com.quannguyen.jagemailclient.email.data.EmailBean;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author 1632647
 */
@Ignore
public class EmailSenderTest {
    private EmailAddressBean emailAddress;
    private String password;
    private EmailAddressBean emailAddressTo;
    private EmailBean emailBean;
    
    public EmailSenderTest() {
    }
    
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        emailAddress = new EmailAddressBean("send.1632647@gmail.com", "Send Test");
        password = "d5=9u~xo";
        
        emailAddressTo = new EmailAddressBean("receive.1632647@gmail.com", "Receive Test");

    }
    
    @After
    public void tearDown() {
    }


    @Test
    public void constructor2Params_Valid_ObjectCreated() {        
        EmailSender sender = new EmailSender(emailAddress, password);
        
        Assert.assertNotNull(sender);
    }
    
    @Test
    public void sendEmail_Valid_NoException() {
        emailBean = new EmailBean();
        emailBean.setHtmlMessage("<h1>Hello World!<h1>");
        emailBean.setTextMessage("Hello World!");
        emailBean.setSubject("Hi!");
        emailBean.setFrom(emailAddress);
        emailBean.setTo(Arrays.asList(new EmailAddressBean[] {emailAddressTo}));
        EmailSender sender = new EmailSender(emailAddress, password);
        
        sender.sendEmail(emailBean);
    }

    //Minimal bean has at least a from and either to/cc/bcc
    @Test
    public void sendEmail_MimnimalBean_NoException() {
        emailBean = new EmailBean();        
        emailBean.setFrom(emailAddress);
        emailBean.setCc(Arrays.asList(new EmailAddressBean[] {emailAddressTo}));
        EmailSender sender = new EmailSender(emailAddress, password);
        
        sender.sendEmail(emailBean);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void sendEmail_InvalidTo_NoException() {
        emailBean = new EmailBean();        
        emailBean.setFrom(emailAddress);
        emailBean.getTo().add(new EmailAddressBean("testtest", "Mr.Test"));
        EmailSender sender = new EmailSender(emailAddress, password);
        
        sender.sendEmail(emailBean);
    }
    
    @Test
    public void sendEmail_NullSubject_NoException() {
        emailBean = new EmailBean();        
        emailBean.setFrom(emailAddress);
        emailBean.setTo(Arrays.asList(new EmailAddressBean[] {emailAddressTo}));
        emailBean.setSubject(null);
        EmailSender sender = new EmailSender(emailAddress, password);
        
        sender.sendEmail(emailBean);
    }
    
    @Test
    public void sendEmail_EmbeddedAttachments_NoException() {
        emailBean = new EmailBean();        
        emailBean.setFrom(emailAddress);
        emailBean.setTo(Arrays.asList(new EmailAddressBean[] {emailAddressTo}));
        emailBean.setSubject(null);
        EmailSender sender = new EmailSender(emailAddress, password);
        
        EmailAttachmentBean attachment = new EmailAttachmentBean();
        attachment.setAttachmentName("Hello");
        attachment.setData(new byte[] {1,2,3,4,5});
        attachment.setIsEmbedded(true);
        
        emailBean.addEmbeddedAttachments(attachment);
        sender.sendEmail(emailBean);
    }
    
    @Test
    public void sendEmail_NoEmbeddedAttachmentsButWithReferrenceHtmlText_NoException() {
        emailBean = new EmailBean();        
        emailBean.setFrom(emailAddress);
        emailBean.setTo(Arrays.asList(new EmailAddressBean[] {emailAddressTo}));
        emailBean.setSubject(null);
        emailBean.setHtmlMessage("<img src='cid:Hello'>");
        EmailSender sender = new EmailSender(emailAddress, password);
        
        EmailAttachmentBean attachment = new EmailAttachmentBean();
        attachment.setAttachmentName("Hello");
        attachment.setData(new byte[] {1,2,3,4,5});
        attachment.setIsEmbedded(false);
        
        emailBean.addAttachments(attachment);
        sender.sendEmail(emailBean);
    }
}   

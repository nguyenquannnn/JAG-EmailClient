/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.data;

import java.io.File;
import java.io.IOException;
import jodd.mail.EmailAttachment;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;

/**
 *
 * @author Quan-Nguyen
 */
//@Ignore
public class EmailAttachmentBeanTest {
    
    public EmailAttachmentBeanTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    @Test(expected = IOException.class)
    public void constructorWithFile_InValid_ExceptionThrown() throws IOException {
        File file = new File("hello.java");
          
        EmailAttachmentBean bean = new EmailAttachmentBean(file);        
    }
    
    @Test
    public void constructorWithFile_Valid_NoException() throws IOException {
        File file = new File("./src/main/resources/attachments/embed2.jpg");
          
        EmailAttachmentBean bean = new EmailAttachmentBean(file);
        Assert.assertNotNull(bean);
    }
    
    @Test
    public void constructor2Param_Valid_ObjectCreated() throws IOException {
        File file = new File("./src/main/resources/attachments/embed2.jpg");
          
        EmailAttachmentBean bean = new EmailAttachmentBean(file, true);
        Assert.assertNotNull(bean);
    }
    
    @Test
    public void constructorEmailAttachment_Valid_ObjectCreated() { 
        EmailAttachmentBean bean = new EmailAttachmentBean( EmailAttachment.with()
                .content(new File("./src/main/resources/attachments/embed2.jpg"))
                .buildByteArrayDataSource());
        Assert.assertNotNull(bean);
    }
    
    @Test
    public void toJoddEmailAttachment_Valid_NoException() throws IOException {
        File file = new File("./src/main/resources/attachments/embed2.jpg");
          
        EmailAttachmentBean bean = new EmailAttachmentBean(file);
        EmailAttachment res = bean.toJoddEmailAttachment();
        
        Assert.assertArrayEquals(res.toByteArray(), bean.getData());
        Assert.assertEquals(res.getName(), bean.getAttachmentName());
    }
    
    @Test
    public void equals_EqualsObject_NoException() throws IOException {
        File file = new File("./src/main/resources/attachments/embed2.jpg");
        File file2 = new File("./src/main/resources/attachments/embed2.jpg");
        
        EmailAttachmentBean bean = new EmailAttachmentBean(file);
        EmailAttachmentBean bean2 = new EmailAttachmentBean(file2);
        
        Assert.assertEquals(bean, bean2);
    }
    
    @Test
    public void equals_UnEqualsObject_NoException() throws IOException {
        File file = new File("./src/main/resources/attachments/embed2.jpg");
        File file2 = new File("./src/main/resources/attachments/test2.jpg");
        
        EmailAttachmentBean bean = new EmailAttachmentBean(file);
        EmailAttachmentBean bean2 = new EmailAttachmentBean(file2);
        
        Assert.assertNotEquals(bean, bean2);
    }
}

package com.quannguyen.jagemailclient.email.persistence;

import com.quannguyen.jagemailclient.db.controller.DbConnectionHandler;
import com.quannguyen.jagemailclient.db.controller.DbConnectionHandlerImpl;
import com.quannguyen.jagemailclient.email.data.EmailAttachmentBean;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Quan-Nguyen
 */
//@Ignore
public class EmailAttachmentDAOImplTest {
    private static final Logger LOG = LoggerFactory.getLogger(EmailAttachmentDAOImplTest.class);
    private final EmailAttachmentDAO attachmentDAO;
    private final DbConnectionHandler handler;
    
    public EmailAttachmentDAOImplTest() {
        String url = "jdbc:mysql://localhost:3306/email_system_test?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
        String user = "TheUser";
        String password = "tester";
        handler = new DbConnectionHandlerImpl(url, user, password);
        this.attachmentDAO = new EmailAttachmentDAOImpl(handler);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        LOG.info("Seeding Database");
        final String seedTableCreatingScript = loadAsString("./src/main/resources/scripts/EmailSystemSchema.sql");
        try (Connection connection = handler.getConnection()) {
            for (String statement : splitStatements(new StringReader(
                    seedTableCreatingScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
            
        final String seedDataScript = loadAsString("./src/main/resources/scripts/InsertMockDataEmailSystem.sql");
        try (Connection connection2 = handler.getConnection()) {
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection2.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }
    
    /**
     * The following methods support the seedDatabase method
     * @author: Ken Fogel
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = new FileInputStream(new File(path));
                Scanner scanner = new Scanner(inputStream);) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
    
   /**
     * Modified to work for all strings
     * Source: https://stackoverflow.com/a/140861
     * @param s Hex String
     * @return byte[]
     * @author: Dave L.
     */
    public static byte[] hexStringToByteArray(String s) {
       if(s.length() % 2 != 0) {
           s = "0" + s;
       }
       int len = s.length();
       byte[] data = new byte[len / 2];
       for (int i = 0; i < len; i += 2) {
           //16 for hexadecimal, check for the numeric value in a base indicated and 
           //store them in a byte. 2 hex character is enconded in abyte. Shift the first byte
           //to the right 4 bit after enconded.
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                             + Character.digit(s.charAt(i+1), 16));
       }
    return data;
}
    
    @After
    public void tearDown() {
    }
    
    @Test(timeout = 1000)
    public void getEmailAttachment_Valid_NoException() throws SQLException {
        EmailAttachmentBean expected = new EmailAttachmentBean(hexStringToByteArray("EBE2AB0"), "amet_sem.gif", false);
        
        EmailAttachmentBean actual = attachmentDAO.getEmailAttachmentBean(1);
        
        Assert.assertEquals(expected, actual);
    }
    
    @Test(timeout = 1000)
    public void getEmailAttachment_InvalidId_NoException() throws SQLException {    
        EmailAttachmentBean actual = attachmentDAO.getEmailAttachmentBean(0);
        
        Assert.assertEquals(null, actual);
    }

    @Test(timeout = 1000)
    public void getEmbeddedEmailAttachmentsOfEmail_NoEmbeddedAttachment_NoException() throws SQLException {
        List<EmailAttachmentBean> expected = new ArrayList<>();
        
        List<EmailAttachmentBean> actual = attachmentDAO.getEmbeddedEmailAttachmentsOfEmail(5);
        
        Assert.assertEquals(expected, actual);
    }
    
    @Test(timeout = 1000)
    public void getEmbeddedEmailAttachmentsOfEmail_OneEmbeddedAttachment_NoException() throws SQLException {
        List<EmailAttachmentBean> expected = new ArrayList<>();
        expected.add(new EmailAttachmentBean(hexStringToByteArray("9B59F67"),"mi_pede_malesuada.doc",true));
        List<EmailAttachmentBean> actual = attachmentDAO.getEmbeddedEmailAttachmentsOfEmail(4);
       
        Assert.assertEquals(expected, actual);
    }
    
    @Test(timeout = 1000)
    public void getEmbeddedEmailAttachmentsOfEmail_MultipleEmbeddedAttachment_NoException() throws SQLException {
        List<EmailAttachmentBean> expected = new ArrayList<>();
        expected.add(new EmailAttachmentBean(hexStringToByteArray("FB29818"),"eros.mpeg",true));
        expected.add(new EmailAttachmentBean(hexStringToByteArray("15B6C0C"),"est_lacinia_nisi.gif",true));
        List<EmailAttachmentBean> actual = attachmentDAO.getEmbeddedEmailAttachmentsOfEmail(1);
       
        Assert.assertEquals(expected, actual);
    }
    
    @Test(timeout = 1000)
    public void getEmailAttachmentsOfEmail_NoAttachment_NoException() throws SQLException {
        List<EmailAttachmentBean> expected = new ArrayList<>();
        
        List<EmailAttachmentBean> actual = attachmentDAO.getEmailAttachmentsOfEmail(5);
        
        Assert.assertEquals(expected, actual);
    }
    
    @Test(timeout = 1000)
    public void getEmailAttachmentsOfEmail_OneAttachment_NoException() throws SQLException {
        List<EmailAttachmentBean> expected = new ArrayList<>();
        expected.add(new EmailAttachmentBean(hexStringToByteArray("6679E62"),"posuere_cubilia_curae.png",false));
        List<EmailAttachmentBean> actual = attachmentDAO.getEmailAttachmentsOfEmail(3);
       
        Assert.assertEquals(expected, actual);
    }
    
    @Test(timeout = 1000)
    public void getEmailAttachmentsOfEmail_MultipleAttachment_NoException() throws SQLException {
        List<EmailAttachmentBean> expected = new ArrayList<>();
        expected.add(new EmailAttachmentBean(hexStringToByteArray("EBE2AB0"),"amet_sem.gif",false));
        expected.add(new EmailAttachmentBean(hexStringToByteArray("9319B99"),"turpis_donec_posuere.pdf",false));
        List<EmailAttachmentBean> actual = attachmentDAO.getEmailAttachmentsOfEmail(1);
       
        Assert.assertEquals(expected, actual);
    }
    
    @Test(timeout = 1000)
    public void createEmailAttachment_Valid_RecordCreated() {
        EmailAttachmentBean bean = new EmailAttachmentBean();
    }
    
    @Test(timeout = 1000)
    public void createEmbeddedEmailAttachment_Valid_RecordCreated() throws SQLException {
        EmailAttachmentBean bean = new EmailAttachmentBean(new byte[] {1,3,5,6,7,8},"hello.jpg",true);
        
        attachmentDAO.createEmailAttachment(bean);
        EmailAttachmentBean actual = attachmentDAO.getEmailAttachmentBean(bean.getId());
        
        Assert.assertEquals(bean, actual);
    }
    
    @Test(timeout = 1000)
    public void deleteEmailAttachment_Valid_RecordDeleted() throws SQLException {
        EmailAttachmentBean bean = new EmailAttachmentBean(new byte[] {1,3,5,6,7,8},"hello.jpg",true);
        
        attachmentDAO.createEmailAttachment(bean);
        int result = attachmentDAO.deleteEmailAttachment(bean.getId());
        
        Assert.assertEquals(1, result);
    }
    
    @Test
    public void addEmailAttachmentForEmail_Valid_NoException() throws SQLException {
        EmailAttachmentBean bean = new EmailAttachmentBean(hexStringToByteArray("EBE2AB0"), "ayo.gif", false);
        
        List<EmailAttachmentBean> expected = new ArrayList<>();
        expected.add(new EmailAttachmentBean(hexStringToByteArray("6679E62"),"posuere_cubilia_curae.png",false));
        expected.add(bean);
        
        int result = attachmentDAO.addEmailAttachmentForEmail(3, bean);
        List<EmailAttachmentBean> actual = attachmentDAO.getEmailAttachmentsOfEmail(3);
       
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
    
    @Test
    public void deleteEmailAttachmentFromEmail_Valid_NoException() throws SQLException { 
        List<EmailAttachmentBean> expected = new ArrayList<>();
        
        int result = attachmentDAO.deleteEmailAttachmentFromEmail(3, 6);
        List<EmailAttachmentBean> actual = attachmentDAO.getEmailAttachmentsOfEmail(3);
       
        Assert.assertEquals(1, result);
        Assert.assertArrayEquals(expected.toArray(), actual.toArray());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quannguyen.jagemailclient.email.data;

import com.quannguyen.jagemailclient.presentation.MainApp;
import com.quannguyen.jagemailclient.email.controller.EmailController;
import com.quannguyen.jagemailclient.email.controller.EmailControllerImpl;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Anh Quan Nguyen
 */
//@Ignore
public class EmailUtilTest {
    
    public EmailUtilTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void isEqualList_NullLists_AreEquals() {
        ArrayList<String> l1 = null;
        ArrayList<String> l2 = null;
        
        Assert.assertTrue(EmailUtil.isEqualsList(l1, l2));
    }
    
    @Test
    public void isEqualList_EmptyList_AreEquals() {
        ArrayList<String> l1 = new ArrayList<>();
        ArrayList<String> l2 = new ArrayList<>();
        
        Assert.assertTrue(EmailUtil.isEqualsList(l1, l2));
    }
    
    @Test
    public void isEqualList_EmptyListNullList_NotEquals() {
        ArrayList<String> l1 = new ArrayList<>();
        ArrayList<String> l2 = null;
        
        Assert.assertFalse(EmailUtil.isEqualsList(l1, l2));
    }
    
    @Test
    public void isEqualList_SameListSameOrder_AreEquals() {
        ArrayList<String> l1 = new ArrayList<>();
        ArrayList<String> l2 = new ArrayList<>();
        l1.add("a");
        l1.add("b");
        l1.add("c");
        l2.add("a");
        l2.add("b");
        l2.add("c");
        Assert.assertTrue(EmailUtil.isEqualsList(l1, l2));
    }
    
    @Test
    public void isEqualList_SameListDifferentOrder_AreEquals() {
        ArrayList<String> l1 = new ArrayList<>();
        ArrayList<String> l2 = new ArrayList<>();
        l1.add("a");
        l1.add("b");
        l1.add("c");
        l2.add("a");
        l2.add("c");
        l2.add("b");
        Assert.assertTrue(EmailUtil.isEqualsList(l1, l2));
    }
    
    @Test
    public void isEqualList_DifferentListDifferentOrder_AreEquals() {
        ArrayList<String> l1 = new ArrayList<>();
        ArrayList<String> l2 = new ArrayList<>();
        l1.add("a");
        l1.add("b");
        l1.add("c");
        l2.add("a");
        l2.add("c");
        Assert.assertFalse(EmailUtil.isEqualsList(l1, l2));
    }
    
    @Test
    public void isSameStringField_NullString_True() {
        String s1 = null;
        String s2 = null;
        
        Assert.assertTrue(EmailUtil.isSameStringField(s1, s2));
    }
    
    @Test
    public void isSameStringField_EmptyString_True() {
        String s1 = "";
        String s2 = "";
        
        Assert.assertTrue(EmailUtil.isSameStringField(s1, s2));
    }
    
    @Test
    public void isSameStringField_NullStringEmptyString_False() {
        String s1 = null;
        String s2 = "";
        
        Assert.assertFalse(EmailUtil.isSameStringField(s1, s2));
    }
    
    @Test
    public void isSameStringField_SameString_True() {
        String s1 = "Hello there";
        String s2 = "Hello there";
        
        Assert.assertTrue(EmailUtil.isSameStringField(s1, s2));
    }
    
    @Test
    public void parseJoddMailToEmailBean_ValidEmailWithAllField_NoException() {
        EmailBean bean = new EmailBean();
        File attachement2 = new File("./src/main/resources/attachments/embed2.jpg");
        File attachement = new File("./src/main/resources/attachments/test2.jpg");
        try {
            List temp = Arrays.asList(new EmailAttachmentBean[] {new EmailAttachmentBean(attachement)});
            bean.setAttachments(temp);
            
            List temp2 = Arrays.asList(new EmailAttachmentBean[] {new EmailAttachmentBean(attachement2, true)});
            bean.setEmbeddedAttachments(temp2);
        } catch (IOException ioe) {
            System.err.print(ioe);
        }
        bean.setFrom(new EmailAddressBean("send.1635333@gmail.com", "Quan Nguyen"));
        bean.setSubject("Hello this is Main!");
        bean.setTextMessage("Hello Everyone from Main");
        bean.setHtmlMessage("<META http-equiv=Content-Type content=\"text/html; " +
            "charset=utf-8\"><body><h1>Hey!</h1><img src='cid:embed2.jpg'>" +
            "<h2>Hay!</h2></body></html>");
        bean.setTo(Arrays.asList(new EmailAddressBean[] {new EmailAddressBean("receive.1632647@gmail.com", "Receive Test")}));
        bean.setCc(Arrays.asList(new EmailAddressBean[] {new EmailAddressBean("other.1632647@gmail.com", "Other Test")}));
        EmailController controller = new EmailControllerImpl(new EmailAddressBean("send.1635333@gmail.com", "Quan Nguyen"),"sendpassword",
        new EmailAddressBean("receive.1632647@gmail.com", "Receive Test"), "p,7\\j?~(", null);
        controller.sendEmail(bean);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //receiver.receiveEmail(); uses parseJoddMailToEmailBean()
        List<EmailBean> res = controller.receiveEmail();
        Assert.assertEquals(res.get(0), bean);
    }
    
        @Test
    public void parseJoddMailToEmailBean_MinimalEmailBean_NoException() {
        EmailBean bean = new EmailBean();
    
        bean.setFrom(new EmailAddressBean("send.1635333@gmail.com", "Quan Nguyen"));
        bean.setTo(Arrays.asList(new EmailAddressBean[] {new EmailAddressBean("receive.1632647@gmail.com", "Receive Test")}));
        bean.setCc(Arrays.asList(new EmailAddressBean[] {new EmailAddressBean("other.1632647@gmail.com", "Other Test")}));
        EmailController controller = new EmailControllerImpl(new EmailAddressBean("send.1635333@gmail.com", "Quan Nguyen"),"sendpassword",
        new EmailAddressBean("receive.1632647@gmail.com", "Receive Test"), "p,7\\j?~(", null);
        controller.sendEmail(bean);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(MainApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //receiver.receiveEmail(); uses parseJoddMailToEmailBean()
        List<EmailBean> res = controller.receiveEmail();
        
        Assert.assertEquals(res.get(0), bean);
    }
}

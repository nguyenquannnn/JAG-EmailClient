/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.
 *
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 *
 * Contributor(s):
 */
package com.quannguyen.jagemailclient.folder.persistence;

import com.quannguyen.jagemailclient.db.controller.DbConnectionHandler;
import com.quannguyen.jagemailclient.db.controller.DbConnectionHandlerImpl;
import com.quannguyen.jagemailclient.folder.data.FolderBean;
import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author 1632647
 */
//@Ignore
public class FolderDAOImplTest {
    private final static Logger LOG = LoggerFactory.getLogger(FolderDAOImplTest.class);
    private FolderDAOImpl folderDAO;
    private DbConnectionHandler handler;
    
    public FolderDAOImplTest() {
        String url = "jdbc:mysql://localhost:3306/email_system_test?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
        String user = "TheUser";
        String password = "tester";
        this.handler = new DbConnectionHandlerImpl(url, user, password);
        this.folderDAO = new FolderDAOImpl(handler);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
        
    @After
    public void tearDown() {
    }
    
    @Before
    public void setUp() {
        LOG.info("Seeding Database");
        final String seedTableCreatingScript = loadAsString("./src/main/resources/scripts/EmailSystemSchema.sql");
        try (Connection connection = handler.getConnection()) {
            for (String statement : splitStatements(new StringReader(
                    seedTableCreatingScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
            
        final String seedDataScript = loadAsString("./src/main/resources/scripts/InsertMockDataEmailSystem.sql");
        try (Connection connection2 = handler.getConnection()) {
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection2.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }
    
    /**
     * The following methods support the seedDatabase method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = new FileInputStream(new File(path));
                Scanner scanner = new Scanner(inputStream);) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
    
    @Test(timeout = 1000)
    public void getAllFolder_Valid_NoException() throws SQLException{
        List<FolderBean> folders = folderDAO.getAllFolder();
        
        List<FolderBean> expected = new ArrayList<>();
        expected.add(new FolderBean("Inbox"));
        expected.add(new FolderBean("Sent"));
        expected.add(new FolderBean("Work"));
        expected.add(new FolderBean("Important", 3));
        expected.add(new FolderBean("More Important", 4));
        Assert.assertEquals(expected.toArray().length, folders.toArray().length - 2);    
    }
    
    @Test(timeout = 1000)
    public void getFolder_Valid_NoException() throws SQLException {
        FolderBean folder = folderDAO.getFolder(1);
        
        FolderBean expected = new FolderBean("Inbox");
        
        Assert.assertEquals(expected, folder);
    }
    
    @Test(timeout = 1000)
    public void createFolder_Valid_RecordCreated() throws SQLException{
        FolderBean bean = new FolderBean(0, "School", 0);
        
        List<FolderBean> folders = folderDAO.getAllFolder();
        List<FolderBean> expected = new ArrayList<>();
        expected.add(new FolderBean("Inbox"));
        expected.add(new FolderBean("Sent"));
        expected.add(new FolderBean("Work"));
        expected.add(new FolderBean("Important", 3));
        expected.add(new FolderBean("More Important", 4));
        
        Assert.assertEquals(expected.toArray().length, folders.toArray().length - 2);        
    }
    
    @Test(timeout = 1000)
    public void deleteFolder_Valid_DeleteRecord() throws SQLException{

        List<FolderBean> expected = new ArrayList<>();
        expected.add(new FolderBean("Inbox"));
        expected.add(new FolderBean("Work"));
        expected.add(new FolderBean("Important", 3));
        expected.add(new FolderBean("More Important", 4));
        
        folderDAO.deleteFolder(2);
        List<FolderBean> folders = folderDAO.getAllFolder();
        
       Assert.assertEquals(expected.toArray().length, folders.toArray().length - 2);    
    }
}
